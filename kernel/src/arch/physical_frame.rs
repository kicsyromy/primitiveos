use common::arch::AddressPhysical;

use crate::constants::FRAME_SIZE;

pub struct PhysicalFrame {
    address: AddressPhysical,
}

impl PhysicalFrame {
    #[inline]
    pub const fn new(address: AddressPhysical) -> Self {
        PhysicalFrame { address }
    }

    #[inline]
    pub fn address(&self) -> AddressPhysical {
        self.address
    }

    #[inline]
    pub fn zero(&mut self) {
        let address: usize = self.address.into();
        unsafe { common::libc_functions::memset(address as *mut u8, 0, FRAME_SIZE) };
    }
}
