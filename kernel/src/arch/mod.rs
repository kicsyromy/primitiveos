pub use common::arch::*;

pub mod frame_allocator;
pub use frame_allocator::FrameAllocator;

mod physical_frame;
use physical_frame::PhysicalFrame;

#[cfg(target_arch = "x86_64")]
pub mod x86_64;
#[cfg(target_arch = "x86_64")]
include!("x86_64/config.rs");

#[cfg(target_arch = "aarch64")]
pub mod aarch64;
#[cfg(target_arch = "aarch64")]
include!("aarch64/config.rs");

pub type AddressVirtual = common::arch::AddressVirtual;
pub type AddressPhysical = common::arch::AddressPhysical;
