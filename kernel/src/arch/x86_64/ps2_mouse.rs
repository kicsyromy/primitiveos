use super::{in_byte, out_byte};

const PS2_CONTROLLER_PORT: u16 = 0x64;
const PS2_MOUSE_PORT: u16 = 0x60;

const COMMAND_ENABLE_AUXILIARY_DEVICE: u8 = 0xA8;

const COMMAND_ENABLE_PACKET_STREAMING: u8 = 0xF4;
const COMMAND_SET_DEFAULTS: u8 = 0xF6;

fn ps2_mouse_wait_write() {
    const TIMEOUT: usize = 10000;
    for _ in 0..TIMEOUT {
        let byte_read = unsafe { in_byte(PS2_CONTROLLER_PORT) };
        if (byte_read & 0b10) == 0 {
            break;
        }
    }
}
fn ps2_mouse_wait_read() {
    const TIMEOUT: usize = 10000;
    for _ in 0..TIMEOUT {
        let byte_read = unsafe { in_byte(PS2_CONTROLLER_PORT) };
        if (byte_read & 0b1) != 0 {
            break;
        }
    }
}

fn ps2_mouse_read() -> u8 {
    unsafe {
        ps2_mouse_wait_read();
        in_byte(PS2_MOUSE_PORT)
    }
}

fn ps2_mouse_write(value: u8) {
    unsafe {
        ps2_mouse_wait_write();
        out_byte(PS2_CONTROLLER_PORT, 0xD4);
        ps2_mouse_wait_write();
        out_byte(PS2_MOUSE_PORT, value);
    }
}

pub fn initialize_ps2_mouse() {
    unsafe {
        out_byte(PS2_CONTROLLER_PORT, COMMAND_ENABLE_AUXILIARY_DEVICE);
        ps2_mouse_wait_write();
        out_byte(PS2_CONTROLLER_PORT, 0x20); //magic that tells the controller that we want to talk to the mouse
    }

    let status = ps2_mouse_read() | 0b10;
    unsafe {
        ps2_mouse_wait_write();
        out_byte(PS2_CONTROLLER_PORT, PS2_MOUSE_PORT as u8);
        ps2_mouse_wait_write();
        out_byte(PS2_MOUSE_PORT, status);
    }

    ps2_mouse_write(COMMAND_SET_DEFAULTS);
    let _ = ps2_mouse_read();

    ps2_mouse_write(COMMAND_ENABLE_PACKET_STREAMING);
    let _ = ps2_mouse_read();
}
