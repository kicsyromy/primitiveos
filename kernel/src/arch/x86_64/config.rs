pub mod cpu {
    use crate::arch::x86_64::*;

    pub fn configure() {
        gdt::init_global_descriptor_table();
        idt::init_interrupt_descriptor_table();
        pic_chip::initialize();
    }
}

pub mod paging {
    use spin::Mutex;

    use crate::{
        arch::{x86_64::paging::PageTableManager, AddressPhysical, AddressVirtual, FrameAllocator},
        constants::*,
    };

    static mut PAGE_TABLE_MANAGER: Option<Mutex<PageTableManager>> = None;

    pub fn configure(frame_allocator: &Mutex<FrameAllocator>) {
        use log::*;

        let mut page_table_manager = unsafe { PageTableManager::new() };

        let physical_address_range_end: usize =
            frame_allocator.lock().physical_address_range().end.into();
        info!(
            "Identity mapping physical memory from 0x{:x} to 0x{:x}",
            0, physical_address_range_end
        );

        // FIXME: The mapping is a bit overreaching, don't map unusable memory
        for i in (0..physical_address_range_end).step_by(PAGE_SIZE) {
            page_table_manager.map_memory(AddressVirtual::from(i), AddressPhysical::from(i));
        }

        // SAFETY: Call is safe since initialization happens at kernel start-up before
        // multi-core/multi-threading is enabled.
        unsafe {
            PAGE_TABLE_MANAGER = Some(Mutex::new(page_table_manager));
        }
    }

    pub fn activate() {
        page_table_manager().lock().activate();
    }

    pub fn map(address_virtual: AddressVirtual, address_physical: AddressPhysical) {
        page_table_manager()
            .lock()
            .map_memory(address_virtual, address_physical);
    }

    fn page_table_manager() -> &'static Mutex<PageTableManager> {
        assert!(unsafe { PAGE_TABLE_MANAGER.is_some() });

        unsafe { PAGE_TABLE_MANAGER.as_ref().unwrap() }
    }
}
