use super::{IDTEntry, IDTEntryOptions};
use crate::arch::x86_64::pic_chip::{PICIRQLine, PICId};

#[allow(dead_code)]
#[derive(Copy, Clone)]
#[repr(usize)]
pub enum InterruptVectorIndex {
    // CPU Exceptions
    ExceptionDivideError = 0x0,
    ExceptionDebug = 0x1,
    ExceptionNonMaskableInterrupt = 0x2,
    ExceptionBreakpoint = 0x3,
    ExceptionOverflow = 0x4,
    ExceptionBoundRangeExceeded = 0x5,
    ExceptionInvalidOpcode = 0x6,
    ExceptionDeviceNotAvailable = 0x7,
    ExceptionDoubleFault = 0x8,
    ExceptionInvalidTss = 0xA,
    ExceptionSegmentNotPresent = 0xB,
    ExceptionStackSegmentFault = 0xC,
    ExceptionGeneralProtectionFault = 0xD,
    ExceptionPageFault = 0xE,
    ExceptionX87FloatingPoint = 0x10,
    ExceptionAlignmentCheck = 0x11,
    ExceptionMachineCheck = 0x12,
    ExceptionSimdFloatingPoint = 0x13,
    ExceptionVirtualization = 0x14,
    ExceptionSecurityException = 0x1E,

    // Hardware Interrupts
    IRQPICTimer = PICId::PIC1 as usize + PICIRQLine::SystemTimer as usize,
    IRQPS2Keyboard = PICId::PIC1 as usize + PICIRQLine::KeyboardController as usize,
    IRQPS2Mouse = PICId::PIC1 as usize + PICIRQLine::MouseController as usize,
}

#[repr(C)]
pub struct StackFrame {
    instruction_pointer: u64,
    code_segment: u64,
    cpu_flags: u64,
    stack_pointer: u64,
    stack_segment: u64,
}

impl core::fmt::Display for StackFrame {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!(
            "\tIP: 0x{:x}\n\tCS: 0x{:x}\n\tCPU_FLAGS: 0b{:0>64b}\n\tSP: 0x{:x}\n\tSS: 0x{:x}",
            self.instruction_pointer,
            self.code_segment,
            self.cpu_flags,
            self.stack_pointer,
            self.stack_segment
        ))
    }
}

#[inline(never)]
pub fn _set_interrupt_handler(
    vector_id: InterruptVectorIndex,
    wrapper: unsafe extern "C" fn() -> !,
) -> &'static mut IDTEntry {
    use crate::arch::x86_64::idt::IDT;

    let idt_entry: &mut IDTEntry = unsafe { &mut IDT[vector_id as usize] };
    idt_entry.set_handler(wrapper);
    idt_entry.selector = crate::arch::x86_64::gdt::KERNEL_CODE_SEGMENT;
    idt_entry.options = IDTEntryOptions::DEFAULT | IDTEntryOptions::PRESENT;

    idt_entry
}
