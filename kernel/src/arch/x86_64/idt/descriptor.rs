#[repr(C, packed)]
pub struct Descriptor {
    pub size: u16,
    pub offset: u64,
}
