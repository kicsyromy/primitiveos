use interrupts::_set_interrupt_handler;

macro_rules! set_interrupt_handler {
    ($idt_vector_index: expr, $handler: ident) => {{
        use paste::paste;
        use crate::arch::x86_64::idt::*;

        let idt_vector_index = $idt_vector_index as IDTVectorIndex;

        paste! {
            #[naked]
            unsafe extern "C" fn [<$handler _wrapper>]() -> ! {
                asm! {
                    "# Save scratch registers\n\
                    push rax\n\
                    push rcx\n\
                    push rdx\n\
                    push rsi\n\
                    push rdi\n\
                    push r8\n\
                    push r9\n\
                    push r10\n\
                    push r11\n\
\
                    mov rdi, rsp       # Set the stack frame pointer as argument to the handler function call\n\
                    add rdi, 9*8       # Calculate exception stack frame pointer\n\
                    call {}\n\
\
                    # Restore scratch registers\n\
                    pop r11\n\
                    pop r10\n\
                    pop r9\n\
                    pop r8\n\
                    pop rdi\n\
                    pop rsi\n\
                    pop rdx\n\
                    pop rcx\n\
                    pop rax\n\
\
                    iretq\n",
                    sym $handler,
                    options(noreturn),
                }
            }
        }

        _set_interrupt_handler(idt_vector_index, paste!{ [<$handler _wrapper>] })
    }};
}

macro_rules! set_interrupt_handler_with_error_code {
    ($idt_vector_index: expr, $handler: ident) => {{
        use paste::paste;
        use crate::arch::x86_64::idt::*;

        let idt_vector_index = $idt_vector_index as IDTVectorIndex;

        paste! {
            #[naked]
            unsafe extern "C" fn [<$handler _wrapper>]() -> ! {
                asm! {
                    "# Save scratch registers\n\
                    push rax\n\
                    push rcx\n\
                    push rdx\n\
                    push rsi\n\
                    push rdi\n\
                    push r8\n\
                    push r9\n\
                    push r10\n\
                    push r11\n\
\
                    mov rdi, [rsp + 9*8] # Pop error code into rdi, first argument sent to the handler function\n\
                    mov rsi, rsp         # Set the stack frame pointer as the second argument to the handler function call\n\
                    add rsi, 10*8\n\
                    sub rsp, 8           # Align the stack pointer\n\
\
                    call {}\n\
\
                    add rsp, 8           # Undo stack pointer alignment\n\
\
                    # Restore scratch registers\n\
                    pop r11\n\
                    pop r10\n\
                    pop r9\n\
                    pop r8\n\
                    pop rdi\n\
                    pop rsi\n\
                    pop rdx\n\
                    pop rcx\n\
                    pop rax\n\
\
                    add rsp, 8           # Pop error code\n\
                    iretq\n",
                    sym $handler,
                    options(noreturn),
                }
            }
        }

        _set_interrupt_handler(idt_vector_index, paste!{ [<$handler _wrapper>] })
    }};
}

mod descriptor;
mod entry;
mod exceptions;
mod hw_interrupts;
mod interrupts;

mod loader {
    use crate::arch::x86_64::idt::IDTDescriptor;

    global_asm!(
        r#"
        .global load_idt
    load_idt:
        lidt [rdi]         # rdi - First parameter passed to function
        ret
    "#
    );

    extern "C" {
        pub fn load_idt(descriptor: &IDTDescriptor);
    }
}

type StackFrame = interrupts::StackFrame;

type IDTEntry = entry::Entry;
type IDTEntryOptions = entry::OptionFlags;
type IDTDescriptor = descriptor::Descriptor;
type IDTVectorIndex = interrupts::InterruptVectorIndex;

const IDT_ENTRY_COUNT: usize = 256;
const IDT_SIZE: usize = core::mem::size_of::<IDTEntry>() * IDT_ENTRY_COUNT;

static mut IDT: [IDTEntry; IDT_ENTRY_COUNT] = [IDTEntry::new(); IDT_ENTRY_COUNT];

pub fn init_interrupt_descriptor_table() {
    use crate::arch::x86_64::idt::entry::ISTIStackIndex;
    use exceptions::handlers::*;
    use hw_interrupts::handlers::*;

    set_interrupt_handler!(IDTVectorIndex::ExceptionDivideError, divide_by_zero_handler);
    set_interrupt_handler!(IDTVectorIndex::ExceptionDebug, debug_handler);
    set_interrupt_handler!(
        IDTVectorIndex::ExceptionNonMaskableInterrupt,
        non_maskable_interrupt_handler
    );
    set_interrupt_handler!(IDTVectorIndex::ExceptionBreakpoint, breakpoint_handler);
    set_interrupt_handler!(IDTVectorIndex::ExceptionOverflow, overflow_handler);
    set_interrupt_handler!(
        IDTVectorIndex::ExceptionBoundRangeExceeded,
        bound_range_exceeded
    );
    set_interrupt_handler!(
        IDTVectorIndex::ExceptionInvalidOpcode,
        invalid_opcode_handler
    );
    set_interrupt_handler_with_error_code!(IDTVectorIndex::ExceptionPageFault, page_fault_handler);
    set_interrupt_handler_with_error_code!(
        IDTVectorIndex::ExceptionDoubleFault,
        double_fault_handler
    )
    .set_stack_index(ISTIStackIndex::Index1);

    set_interrupt_handler_with_error_code!(
        IDTVectorIndex::ExceptionInvalidTss,
        invalid_tss_handler
    );

    // HW Interrupts
    set_interrupt_handler!(IDTVectorIndex::IRQPICTimer, pic_timer_interrupt_handler);
    set_interrupt_handler!(
        IDTVectorIndex::IRQPS2Keyboard,
        ps2_keyboard_interrupt_handler
    );
    set_interrupt_handler!(IDTVectorIndex::IRQPS2Mouse, ps2_mouse_interrupt_handler);

    unsafe {
        let descriptor = IDTDescriptor {
            size: (IDT_SIZE - 1) as u16,
            offset: (&IDT as *const _ as usize as u64),
        };

        crate::arch::x86_64::idt::loader::load_idt(&descriptor);
    }
}
