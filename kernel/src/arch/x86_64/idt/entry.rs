use bitflags::bitflags;

bitflags! {
    pub struct OptionFlags: u16 {
        const DEFAULT = 0b0000111000000000;
        const ISTI_DONT_SWITCH_STACKS = 0b0000000000000000;
        const ISTI_SWITCH_TO_STACK_1 = 0b0000000000000001;
        const ISTI_SWITCH_TO_STACK_2 = 0b0000000000000010;
        const ISTI_SWITCH_TO_STACK_3 = 0b0000000000000011;
        const ISTI_SWITCH_TO_STACK_4 = 0b0000000000000100;
        const ISTI_SWITCH_TO_STACK_5 = 0b0000000000000101;
        const ISTI_SWITCH_TO_STACK_6 = 0b0000000000000110;
        const ISTI_SWITCH_TO_STACK_7 = 0b0000000000000111;
        const INTERRUPT_GATE = 0b0000000000000000;
        const TRAP_GATE = 0b0000000100000000;
        const DESCRIPTOR_PRIVILEDGE_LEVEL_0 = 0b0000000000000000;
        const DESCRIPTOR_PRIVILEDGE_LEVEL_1 = 0b0010000000000000;
        const DESCRIPTOR_PRIVILEDGE_LEVEL_2 = 0b0100000000000000;
        const DESCRIPTOR_PRIVILEDGE_LEVEL_3 = 0b0110000000000000;
        const PRESENT = 0b1000000000000000;
    }
}

#[allow(dead_code)]
#[repr(u8)]
pub enum ISTIStackIndex {
    DontSwitch = OptionFlags::ISTI_DONT_SWITCH_STACKS.bits as u8,
    Index1 = OptionFlags::ISTI_SWITCH_TO_STACK_1.bits as u8,
    Index2 = OptionFlags::ISTI_SWITCH_TO_STACK_2.bits as u8,
    Index3 = OptionFlags::ISTI_SWITCH_TO_STACK_3.bits as u8,
    Index4 = OptionFlags::ISTI_SWITCH_TO_STACK_4.bits as u8,
    Index5 = OptionFlags::ISTI_SWITCH_TO_STACK_5.bits as u8,
    Index6 = OptionFlags::ISTI_SWITCH_TO_STACK_6.bits as u8,
    Index7 = OptionFlags::ISTI_SWITCH_TO_STACK_7.bits as u8,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Entry {
    handler_address_low: u16,
    pub selector: u16,
    pub options: OptionFlags,
    handler_address_mid: u16,
    handler_address_high: u32,
    _reserved: u32,
}

impl Entry {
    pub const fn new() -> Self {
        Self {
            handler_address_low: 0,
            selector: 0,
            options: OptionFlags::DEFAULT,
            handler_address_mid: 0,
            handler_address_high: 0,
            _reserved: 0,
        }
    }

    pub fn set_stack_index(&mut self, index: ISTIStackIndex) {
        let mut options = self.options.bits;
        options &= 0b1111111111111000;
        options |= index as u16;
        self.options = OptionFlags::from_bits(options).unwrap();
    }

    pub fn set_handler(&mut self, handler: unsafe extern "C" fn() -> !) {
        let handler_address: u64 = handler as *const u64 as u64;

        self.handler_address_low = (handler_address & 0x000000000000ffff) as u16;
        self.handler_address_mid = ((handler_address & 0x00000000ffff0000) >> 16) as u16;
        self.handler_address_high = ((handler_address & 0xffffffff00000000) >> 32) as u32;
    }
}
