use bitflags::bitflags;

bitflags! {
    struct PageFaultErrorCode: u64 {
        const PROTECTION_VIOLATION = 1 << 0;
        const CAUSED_BY_WRITE = 1 << 1;
        const USER_MODE = 1 << 2;
        const MALFORMED_TABLE = 1 << 3;
        const INSTRUCTION_FETCH = 1 << 4;
    }
}

#[allow(clippy::empty_loop)]
pub mod handlers {
    use log::*;

    use crate::arch::x86_64::idt::{exceptions::PageFaultErrorCode, StackFrame};

    pub extern "C" fn divide_by_zero_handler(stack_frame: &StackFrame) {
        error!(
            "CPU Exception:\nDivide-by-zero\nStack Frame:\n{}",
            stack_frame
        );

        loop {}
    }

    pub extern "C" fn debug_handler(stack_frame: &StackFrame) {
        error!("CPU Exception:\nDebug\nStack Frame:\n{}", stack_frame);

        loop {}
    }

    pub extern "C" fn non_maskable_interrupt_handler(stack_frame: &StackFrame) {
        error!(
            "CPU Exception:\nNon-maskable Interrupt\nStack Frame:\n{}",
            stack_frame
        );

        loop {}
    }

    pub extern "C" fn breakpoint_handler(stack_frame: &StackFrame) {
        error!("CPU Exception:\nBreakpoint\nStack Frame:\n{}", stack_frame);
    }

    pub extern "C" fn overflow_handler(stack_frame: &StackFrame) {
        error!("CPU Exception:\nOverflow\nStack Frame:\n{}", stack_frame);

        loop {}
    }

    pub extern "C" fn bound_range_exceeded(stack_frame: &StackFrame) {
        error!(
            "CPU Exception:\nBound Range Exceeded\nStack Frame:\n{}",
            stack_frame
        );

        loop {}
    }

    pub extern "C" fn invalid_opcode_handler(stack_frame: &StackFrame) {
        error!(
            "CPU Exception:\nInvalid Opcode\nStack Frame:\n{}",
            stack_frame
        );

        loop {}
    }

    pub extern "C" fn page_fault_handler(error_code: u64, stack_frame: &StackFrame) {
        error!(
            "CPU Exception:\nPage Fault: Reason = {:?}\nStack Frame:\n{}",
            PageFaultErrorCode::from_bits(error_code).unwrap(),
            stack_frame
        );
    }

    pub extern "C" fn double_fault_handler(error_code: u64, stack_frame: &StackFrame) {
        error!(
            "CPU Exception:\nDouble Fault: Reason = {:?}\nStack Frame:\n{}",
            error_code, stack_frame
        );

        loop {}
    }

    pub extern "C" fn invalid_tss_handler(error_code: u64, stack_frame: &StackFrame) {
        error!(
            "CPU Exception:\nInvalid TSS: Error Code = {:?}\nStack Frame:\n{}",
            error_code, stack_frame
        );

        loop {}
    }
}
