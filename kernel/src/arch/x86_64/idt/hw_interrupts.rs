pub mod handlers {
    use crate::arch::x86_64::{
        idt::StackFrame,
        pic_chip::{self, keyboard_scan_code, mouse_data, PICIRQLine},
    };
    use crate::task;

    pub extern "C" fn pic_timer_interrupt_handler(_stack_frame: &StackFrame) {
        task::timer::queue_timer_tick();
        pic_chip::notify_end_of_interrupt(PICIRQLine::SystemTimer);
    }

    pub extern "C" fn ps2_keyboard_interrupt_handler(_stack_frame: &StackFrame) {
        task::keyboard::queue_scan_code(keyboard_scan_code());
        pic_chip::notify_end_of_interrupt(PICIRQLine::KeyboardController);
    }

    pub extern "C" fn ps2_mouse_interrupt_handler(_stack_frame: &StackFrame) {
        task::mouse::queue_mouse_event(mouse_data());
        pic_chip::notify_end_of_interrupt(PICIRQLine::MouseController);
    }
}
