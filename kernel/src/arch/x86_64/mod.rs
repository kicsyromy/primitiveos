pub mod gdt;
pub mod idt;
pub mod paging;
pub mod pic_chip;
mod ps2_mouse;
pub mod tss;

unsafe fn out_byte(port: u16, value: u8) {
    asm! {
    "out dx, al",
    in ("dx") port,
    in ("al") value,
    }
}

unsafe fn in_byte(port: u16) -> u8 {
    let value;

    asm! {
    "in al, dx",
    in ("dx") port,
    out("al") value,
    }

    value
}
