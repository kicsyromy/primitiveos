#[repr(C, packed)]
pub struct GDTDescriptor {
    pub size: u16,
    pub offset: u64,
}
