mod descriptor;
mod entry;

mod loader {
    use crate::arch::x86_64::gdt::GDTDescriptor;

    global_asm!(
        r#"
        .global load_gdt
    load_gdt:
        lgdt (%rdi)         # rdi - First parameter passed to function
        mov $0x30, %ax      # Load TSS segment selector into ax
        ltr %ax             # Load TSS
        mov $0x10, %ax      # Move the kernel data segment selector into ax
        mov %ax, %ds        # Update all the data segment registers with the kernel data segment selector value
        mov %ax, %es
        mov %ax, %fs
        mov %ax, %gs
        mov %ax, %ss
        pop %rdi            # Pop the return address from this function into rdi
        mov $0x08, %rax     # Move the kernel code segment selector into rax
        push %rax           # Push the kernel code segment selector onto the stack, will subsequently be used by the far jump (lretq) as a return address
        push %rdi           # Push the return address to the stack (not clear why)
        lretq               # Do a far jump to the kernel code segment
    "#,
        options(att_syntax)
    );

    extern "C" {
        pub fn load_gdt(descriptor: &GDTDescriptor);
    }
}

type GDTEntry = entry::GDTEntry;
type GDTDescriptor = descriptor::GDTDescriptor;

static mut DEFAULT_GDT: GlobalDescriptorTable = GlobalDescriptorTable::default();

#[allow(dead_code)]
pub const KERNEL_DATA_SEGMENT: u16 = 0x10;
pub const KERNEL_CODE_SEGMENT: u16 = 0x08;

#[repr(C, packed)]
struct GDTInner {
    pub null: GDTEntry,
    // 0x00
    pub kernel_code: GDTEntry,
    // 0x08
    pub kernel_data: GDTEntry,
    // 0x10
    pub user_null: GDTEntry,
    // 0x18
    pub user_code: GDTEntry,
    // 0x20
    pub user_data: GDTEntry,
    // 0x28
    pub tss_low: GDTEntry,
    // 0x30
    pub tss_high: GDTEntry, // 0x38
}

const GDT_SIZE: usize = core::mem::size_of::<GDTInner>();

// The GDT needs to be both packed and aligned, this does not work in rust
// As a workaround the _inner struct is packed, and the outer struct is aligned
// and padded out to fill an entire page (not sure this is actually needed though)
#[repr(C, align(0x1000))]
struct GlobalDescriptorTable {
    pub inner: GDTInner,
    _padding: [u8; 0x1000 - GDT_SIZE],
}

impl GlobalDescriptorTable {
    const fn default() -> Self {
        Self {
            inner: GDTInner {
                null: GDTEntry::new(0, 0, 0, 0x00, 0x00, 0),
                kernel_code: GDTEntry::new(0, 0, 0, 0x9a, 0xa0, 0),
                kernel_data: GDTEntry::new(0, 0, 0, 0x92, 0xa0, 0),
                user_null: GDTEntry::new(0, 0, 0, 0x00, 0x00, 0),
                user_code: GDTEntry::new(0, 0, 0, 0x9a, 0xa0, 0),
                user_data: GDTEntry::new(0, 0, 0, 0x92, 0xa0, 0),
                tss_low: GDTEntry::new(0, 0, 0, 0x89, 0xa0, 0),
                tss_high: GDTEntry::new(0, 0, 0, 0x00, 0x00, 0),
            },
            _padding: [0; 0x1000 - GDT_SIZE],
        }
    }
}

pub fn init_global_descriptor_table() {
    use static_assertions::*;

    use crate::arch::x86_64::tss::TaskStateSegment;
    use crate::constants::PAGE_SIZE;

    const_assert_eq!(
        core::mem::size_of::<GDTEntry>(),
        core::mem::size_of::<u64>()
    );
    const_assert_eq!(
        (core::mem::size_of::<GDTEntry>() * 8),
        core::mem::size_of::<GDTInner>()
    );

    let gdt = unsafe { &mut DEFAULT_GDT };

    let tss_base_address = TaskStateSegment::address();

    gdt.inner.tss_low.base_0_15 = (tss_base_address & 0xffff) as u16;
    gdt.inner.tss_low.base_16_23 = ((tss_base_address >> 16) & 0xff) as u8;
    gdt.inner.tss_low.base_24_31 = ((tss_base_address >> 24) & 0xff) as u8;
    gdt.inner.tss_low.limit_0_15 = core::mem::size_of::<TaskStateSegment>() as u16;
    gdt.inner.tss_high.limit_0_15 = ((tss_base_address >> 32) & 0xffff) as u16;
    gdt.inner.tss_high.base_0_15 = ((tss_base_address >> 48) & 0xffff) as u16;

    // Set up some stack space
    const STACK_SIZE: usize = PAGE_SIZE * 5;
    static mut ISTI_STACK_1: [u8; STACK_SIZE] = [0; STACK_SIZE];
    let stack_start = unsafe { &ISTI_STACK_1 as *const _ as usize };
    let stack_end = stack_start + STACK_SIZE;
    TaskStateSegment::set_interrupt_stack_table_entry(0, stack_end as u64);

    let descriptor = GDTDescriptor {
        size: (GDT_SIZE - 1) as u16,
        offset: (gdt as *const _ as usize as u64),
    };

    unsafe { crate::arch::x86_64::gdt::loader::load_gdt(&descriptor) }
}
