#[repr(C, packed)]
pub struct GDTEntry {
    pub limit_0_15: u16,
    pub base_0_15: u16,
    pub base_16_23: u8,
    pub access_byte: u8,
    pub limit_16_19_flags: u8,
    pub base_24_31: u8,
}

impl GDTEntry {
    pub const fn new(
        limit_0_15: u16,
        base_0_15: u16,
        base_16_23: u8,
        access_byte: u8,
        limit_16_19_flags: u8,
        base_24_31: u8,
    ) -> Self {
        Self {
            limit_0_15,
            base_0_15,
            base_16_23,
            access_byte,
            limit_16_19_flags,
            base_24_31,
        }
    }
}
