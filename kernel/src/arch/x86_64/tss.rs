static mut TSS: TaskStateSegment = TaskStateSegment::empty();

const PST_SIZE: usize = 3;
const IST_SIZE: usize = 7;

#[repr(C, packed)]
pub struct TaskStateSegment {
    _reserved1: u32,
    _privilege_stack_table: [u64; PST_SIZE],
    _reserved2: u64,
    interrupt_stack_table: [u64; IST_SIZE],
    _reserved3: u64,
    _reserved4: u64,
    io_map_base_address: u64,
}

impl TaskStateSegment {
    pub fn address() -> u64 {
        unsafe { &TSS as *const _ as u64 }
    }

    pub fn set_interrupt_stack_table_entry(index: usize, address: u64) {
        assert!(index < IST_SIZE);

        let tss = unsafe { &mut TSS };
        tss.interrupt_stack_table[index] = address;
    }

    const fn empty() -> Self {
        Self {
            _reserved1: 0,
            _privilege_stack_table: [0; 3],
            _reserved2: 0,
            interrupt_stack_table: [0; 7],
            _reserved3: 0,
            _reserved4: 0,
            io_map_base_address: 0,
        }
    }
}
