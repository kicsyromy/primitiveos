use pic8259::ChainedPics;
use spin::Mutex;

use super::{in_byte, out_byte};

const PIC_1_OFFSET: u8 = 0x20;
const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

/// IO base address for master PIC
const PIC1_IO_BASE: u16 = 0x20;
/// IO base address for slave PIC
const PIC2_IO_BASE: u16 = 0xA0;
const PIC1_COMMAND_PORT: u16 = PIC1_IO_BASE;
const PIC1_DATA_PORT: u16 = PIC1_IO_BASE + 1;
const PIC2_COMMAND_PORT: u16 = PIC2_IO_BASE;
const PIC2_DATA_PORT: u16 = PIC2_IO_BASE + 1;
const PIC_COMMAND_END_OF_INTERRUPT: u8 = 0x20;

static PICS: Mutex<ChainedPics> =
    Mutex::new(unsafe { ChainedPics::new(PIC_1_OFFSET, PIC_2_OFFSET) });

#[allow(dead_code)]
#[repr(u8)]
pub enum PICId {
    PIC1 = PIC_1_OFFSET,
    PIC2 = PIC_2_OFFSET,
}

#[allow(dead_code)]
#[repr(u8)]
pub enum PICIRQLine {
    SystemTimer,
    KeyboardController,
    SerialPortCOM2,
    SerialPortCOM1,
    LinePrintTerminal2,
    FloppyController,
    LinePrintTerminal1,
    RTCTimer,
    ACPI,
    MouseController = 12,
    MathCoProcessor,
    ATAChannel1,
    ATAChannel2,
}

#[allow(dead_code)]
unsafe fn disable_irq_line(irq_line: PICIRQLine) {
    let _ = PICS.lock();

    let mut irq_line = irq_line as u8;

    let port;
    if irq_line < 8 {
        port = PIC1_DATA_PORT;
    } else {
        port = PIC2_DATA_PORT;
        irq_line -= 8;
    }
    let value = in_byte(port) | (1 << irq_line);
    out_byte(port, value);
}

unsafe fn enable_irq_line(irq_line: PICIRQLine) {
    let _ = PICS.lock();

    let mut irq_line = irq_line as u8;

    let port;
    if irq_line < 8 {
        port = PIC1_DATA_PORT;
    } else {
        port = PIC2_DATA_PORT;
        irq_line -= 8;
    }

    let value = in_byte(port) & !(1 << irq_line);
    out_byte(port, value);
}

unsafe fn irq_mask_all() {
    let _ = PICS.lock();

    asm! {
        "out {1}, {0:l}
        out {2}, {0:l}",
        in(reg) 0xff,
        const PIC1_DATA_PORT,
        const PIC2_DATA_PORT,
    }
}

pub fn initialize() {
    unsafe {
        irq_mask_all();

        PICS.lock().initialize();
    }
}

pub fn start_pic_timer() {
    unsafe {
        enable_irq_line(PICIRQLine::SystemTimer);
    }
}

pub fn enable_ps2_keyboard() {
    unsafe {
        enable_irq_line(PICIRQLine::KeyboardController);
    }
}

pub fn enable_ps2_mouse() {
    super::ps2_mouse::initialize_ps2_mouse();
    unsafe {
        enable_irq_line(PICIRQLine::SerialPortCOM2);
        enable_irq_line(PICIRQLine::MouseController);
    }
}

pub fn keyboard_scan_code() -> u8 {
    unsafe { in_byte(0x60) }
}

pub fn mouse_data() -> u8 {
    unsafe { in_byte(0x60) }
}

pub fn notify_end_of_interrupt(irq_line: PICIRQLine) {
    let irq_line = irq_line as u8;
    unsafe {
        if irq_line >= 8 {
            out_byte(PIC2_COMMAND_PORT, PIC_COMMAND_END_OF_INTERRUPT);
        }
        out_byte(PIC1_COMMAND_PORT, PIC_COMMAND_END_OF_INTERRUPT);
    };
}
