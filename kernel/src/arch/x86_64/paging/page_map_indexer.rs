use common::arch::AddressVirtual;

use crate::arch::x86_64::paging::PageMapLevel;

#[derive(Copy, Clone)]
pub(crate) struct PageMapIndexer {
    address_bits: [u64; 4],
}

impl PageMapIndexer {
    pub(crate) fn new(virtual_address: AddressVirtual) -> Self {
        let virtual_address: u64 = virtual_address.into();

        let mut this = Self {
            address_bits: [0; 4],
        };

        const BIT_MASK: u64 = 0x1ff;

        let virtual_address = virtual_address >> 12;
        this.address_bits[PageMapLevel::Level1 as usize] = virtual_address & BIT_MASK;

        let virtual_address = virtual_address >> 9;
        this.address_bits[PageMapLevel::Level2 as usize] = virtual_address & BIT_MASK;

        let virtual_address = virtual_address >> 9;
        this.address_bits[PageMapLevel::Level3 as usize] = virtual_address & BIT_MASK;

        let virtual_address = virtual_address >> 9;
        this.address_bits[PageMapLevel::Level4 as usize] = virtual_address & BIT_MASK;

        this
    }

    pub(crate) fn index(&self, level: PageMapLevel) -> usize {
        self.address_bits[level as usize] as usize
    }
}
