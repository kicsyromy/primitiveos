use bitflags::bitflags;

pub use page_table_manager::PageTableManager;

mod page_map_indexer;
mod page_map_iterator;
mod page_table_manager;

// If the bit is set, the page is actually in physical memory at the moment.
const BIT_PRESENT: u64 = 0;

// If the bit is set, the page is read/write. Otherwise when it is not set, the page is read-only.
const BIT_READ_WRITE: u64 = 1;

// Controls access to the page based on privilege level. If the bit is set, then the page may be accessed by all; if the bit is not set, however, only the supervisor can access it.
const BIT_USER_SUPERVISOR: u64 = 2;

// If the bit is set, write-through caching is enabled. If not, then write-back is enabled instead.
const BIT_WRITE_THROUGH: u64 = 3;

// If the bit is set, the page will not be cached. Otherwise, it will be.
const BIT_CACHE_DISABLED: u64 = 4;

// Used to discover whether a page has been read or written to. If it has, then the bit is set, otherwise, it is not.
const BIT_ACCESSED: u64 = 5;

// Stores the page size for that specific entry. If the bit is set, then pages are 4 MiB in size. Otherwise, they are 4 KiB.
const BIT_PAGE_SIZE: u64 = 7;

// Not used by the processor, and are free for the OS to store some of its own accounting information.
const BIT_CUSTOM_0: u64 = 9;
const BIT_CUSTOM_1: u64 = 10;
const BIT_CUSTOM_2: u64 = 11;

// No execute bit, only supported on some systems
const BIT_NO_EXECUTE: u64 = 63;

bitflags! {
    pub struct PageTableEntryFlag: u64 {
        const PRESENT = 1 << BIT_PRESENT;
        const READ_WRITE = 1 << BIT_READ_WRITE;
        const USER_SUPERVISOR = 1 << BIT_USER_SUPERVISOR;
        const WRITE_THROUGH = 1 << BIT_WRITE_THROUGH;
        const CACHE_DISABLED = 1 << BIT_CACHE_DISABLED;
        const ACCESSED = 1 << BIT_ACCESSED;
        const PAGE_SIZE = 1 << BIT_PAGE_SIZE;
        const CUSTOM_0 = 1 << BIT_CUSTOM_0;
        const CUSTOM_1 = 1 << BIT_CUSTOM_1;
        const CUSTOM_2 = 1 << BIT_CUSTOM_2;
        const NO_EXECUTE = 1 << BIT_NO_EXECUTE;
    }
}

#[repr(C)]
#[derive(Copy, Clone, Default)]
pub(crate) struct PageTableEntry {
    data: u64,
}

impl PageTableEntry {
    #[inline]
    pub fn get_flag(&self, flag: PageTableEntryFlag) -> bool {
        let flag = flag.bits;
        (self.data & flag) > 0
    }

    #[inline]
    pub fn set_flag(&mut self, flag: PageTableEntryFlag, value: bool) {
        let flag = flag.bits;
        let value = value as u64;
        self.data ^= (value.overflowing_neg().0 ^ self.data) & flag;
    }

    #[inline]
    pub fn present(&self) -> bool {
        self.get_flag(PageTableEntryFlag::PRESENT)
    }

    #[inline]
    pub fn set_present(&mut self, value: bool) {
        self.set_flag(PageTableEntryFlag::PRESENT, value)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn read_write(&self) -> bool {
        self.get_flag(PageTableEntryFlag::READ_WRITE)
    }

    #[inline]
    pub fn set_read_write(&mut self, value: bool) {
        self.set_flag(PageTableEntryFlag::READ_WRITE, value)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn user_supervisor(&self) -> bool {
        self.get_flag(PageTableEntryFlag::USER_SUPERVISOR)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn write_through(&self) -> bool {
        self.get_flag(PageTableEntryFlag::WRITE_THROUGH)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn cache_disabled(&self) -> bool {
        self.get_flag(PageTableEntryFlag::CACHE_DISABLED)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn accessed(&self) -> bool {
        self.get_flag(PageTableEntryFlag::ACCESSED)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn larger_pages(&self) -> bool {
        self.get_flag(PageTableEntryFlag::PAGE_SIZE)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn custom_0(&self) -> bool {
        self.get_flag(PageTableEntryFlag::CUSTOM_0)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn custom_1(&self) -> bool {
        self.get_flag(PageTableEntryFlag::CUSTOM_1)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn custom_2(&self) -> bool {
        self.get_flag(PageTableEntryFlag::CUSTOM_2)
    }

    #[inline]
    #[allow(dead_code)]
    pub fn no_execute(&self) -> bool {
        self.get_flag(PageTableEntryFlag::NO_EXECUTE)
    }

    #[inline]
    pub fn address(&self) -> u64 {
        (self.data & 0x000ffffffffff000) >> 12
    }

    #[inline]
    pub fn set_address(&mut self, value: u64) {
        let address = value & 0x000000ffffffffff;
        self.data &= 0xfff0000000000fff;
        self.data |= address << 12;
    }
}

#[derive(Copy, Clone, PartialEq)]
#[repr(u8)]
pub(crate) enum PageMapLevel {
    Level1 = 0,
    Level2,
    Level3,
    Level4,
}

impl PageMapLevel {
    pub fn from_u8(value: u8) -> PageMapLevel {
        match value {
            4 => PageMapLevel::Level4,
            3 => PageMapLevel::Level3,
            2 => PageMapLevel::Level2,
            1 => PageMapLevel::Level1,
            _ => {
                panic!("Trying to convert value {} to PageTableLevel", value);
            }
        }
    }
}

#[repr(align(0x1000))]
pub(crate) struct PageTable {
    entries: [PageTableEntry; 512],
}
