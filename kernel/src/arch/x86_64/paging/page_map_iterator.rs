use common::arch::AddressVirtual;

use crate::arch::x86_64::{
    paging::page_map_indexer::PageMapIndexer,
    paging::page_table_manager::PageTableManager,
    paging::{PageMapLevel, PageTable, PageTableEntry},
};

pub(crate) struct PageMapIteratorItem<'a> {
    page_table: &'a mut PageTable,
    indexer: PageMapIndexer,
    level: PageMapLevel,
}

impl<'a> PageMapIteratorItem<'a> {
    pub(crate) fn new(
        page_table: &'a mut PageTable,
        indexer: PageMapIndexer,
        level: PageMapLevel,
    ) -> Self {
        Self {
            page_table,
            indexer,
            level,
        }
    }

    #[allow(dead_code)]
    pub(crate) fn page_table(&mut self) -> &mut PageTable {
        self.page_table
    }

    pub(crate) fn page_table_entry(&mut self) -> &'a mut PageTableEntry {
        unsafe {
            core::mem::transmute(&mut self.page_table.entries[self.indexer.index(self.level)])
        }
    }

    #[allow(dead_code)]
    pub(crate) fn level(&self) -> PageMapLevel {
        self.level
    }
}

pub(crate) struct PageMapIterator<'a> {
    indexer: PageMapIndexer,
    level: u8,
    calculate_next_page_table: bool,
    page_table_address: usize,
    _lifeline: &'a PageTableManager,
}

impl<'a> PageMapIterator<'a> {
    pub(crate) fn new(address: AddressVirtual, manager: &'a mut PageTableManager) -> Self {
        Self {
            indexer: PageMapIndexer::new(address),
            level: 4,
            calculate_next_page_table: false,
            page_table_address: manager.level_4_address().into(),
            _lifeline: manager,
        }
    }
}

impl<'a> Iterator for PageMapIterator<'a> {
    type Item = PageMapIteratorItem<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.calculate_next_page_table {
            let current_page_table: &mut PageTable =
                unsafe { core::mem::transmute(self.page_table_address) };
            let current_level = PageMapLevel::from_u8(self.level);

            let page_table_entry = current_page_table.entries[self.indexer.index(current_level)];
            if page_table_entry.present() {
                self.page_table_address = (page_table_entry.address() << 12) as usize;
                self.level -= 1;
            } else {
                return None;
            }
        } else {
            self.calculate_next_page_table = true;
        }

        if self.level == 0 {
            return None;
        }

        Some(PageMapIteratorItem::new(
            unsafe { core::mem::transmute(self.page_table_address) },
            self.indexer,
            PageMapLevel::from_u8(self.level),
        ))
    }
}
