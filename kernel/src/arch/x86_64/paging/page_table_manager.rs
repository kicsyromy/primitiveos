// This is one complicated motha'. The graph below should shed some light on how the page manager
// is supposed to work, and how the pages are laid out in memory.
//                                                                                ------------------------
//  LEVEL 4:                                                                      | Page Manager Level 4 |
//                                                                                ------------------------
//                                                                     ______________________|______________________
//                                                                     |                     |                     |
//                                                                     |                     |                     |
//                                        --------------------------------   --------------------------------   --------------------------------
//  LEVEL 3:                              | Page Directory Pointer Table |   | Page Directory Pointer Table |   | Page Directory Pointer Table |   ... * 512
//                                        --------------------------------   --------------------------------   --------------------------------
//                                                                     ______________________|______________________
//                                                                     |                     |                     |
//                                                                     |                     |                     |
//                                                   ------------------------     ------------------------    ------------------------
//  LEVEL 2:                                         | Page Directory Table |     | Page Directory Table |    | Page Directory Table | ... * 512
//                                                   ------------------------     ------------------------    ------------------------
//                                                                      ______________________|______________________
//                                                                      |                     |                     |
//                                                                      |                     |                     |
//                                                                --------------        --------------         --------------
//  LEVEL 1:                                                      | Page Table |        | Page Table |         | Page Table | ... * 512
//                                                                --------------        --------------         --------------
//                                                ______________________|______________________
//                                                |                     |                     |
//                                                |                     |                     |
//                                             --------             --------              --------
//  LEVEL 0:                                   | Page |             | Page |              | Page | ... * 512
//                                             --------             --------              --------
use common::arch::{AddressPhysical, AddressVirtual};

use crate::{
    arch::{
        frame_allocator,
        x86_64::paging::{
            page_map_indexer::PageMapIndexer, page_map_iterator::PageMapIterator, PageMapLevel,
            PageTable, PageTableEntry,
        },
    },
    constants::PAGE_SIZE,
};

#[repr(transparent)]
pub struct PageTableManager {
    level_4_address: AddressPhysical,
}

impl PageTableManager {
    /// Safety: Unsafe because the initializer assumes the memory is identity mapped to begin with
    pub unsafe fn new() -> Self {
        let mut level_4_frame;

        {
            let mut alloc = frame_allocator::instance().lock();
            level_4_frame = alloc.request_frame();
        }

        level_4_frame.zero();

        Self {
            level_4_address: level_4_frame.address(),
        }
    }

    pub fn level_4_address(&self) -> AddressPhysical {
        self.level_4_address
    }

    pub fn activate(&self) {
        let level_4_address: u64 = self.level_4_address.into();
        unsafe {
            asm!(
            "mov cr3, {}",
            in(reg) level_4_address,
            );
        }
    }

    fn iter_mut(&mut self, address: AddressVirtual) -> PageMapIterator {
        PageMapIterator::new(address, self)
    }

    // FIXME: This is a tad on the slow side, make it faster while retaining the nice abstraction
    //        Ideas:
    //          - The code is checking if the entry is present twice, perhaps that ca be avoided
    //          - It's indexing the PageTable twice, seems redundant
    pub fn map_memory(
        &mut self,
        address_virtual: AddressVirtual,
        address_physical: AddressPhysical,
    ) {
        let mut global_page_allocator = frame_allocator::instance().lock();

        for mut page_map_it in self.iter_mut(address_virtual) {
            let level = page_map_it.level();
            let table_entry = page_map_it.page_table_entry();

            if level == PageMapLevel::Level1 {
                let address_physical: u64 = address_physical.into();
                table_entry.set_address(address_physical >> 12);
                table_entry.set_present(true);
                table_entry.set_read_write(true);

                break;
            }

            if !table_entry.present() {
                let mut frame_address = global_page_allocator.request_frame();

                frame_address.zero();

                let frame_address: u64 = frame_address.address().into();
                table_entry.set_address(frame_address >> 12);
                table_entry.set_present(true);
                table_entry.set_read_write(true);
            }
        }
    }

    #[allow(dead_code)]
    pub fn map_memory_old(
        &mut self,
        address_virtual: AddressVirtual,
        address_physical: AddressPhysical,
    ) {
        let indexer = PageMapIndexer::new(address_virtual);

        let mut global_page_allocator = frame_allocator::instance().lock();

        // Top Level (Level 4) (Page Manager Level 4) -> Page Directory Pointer Table
        let l4_address: u64 = self.level_4_address.into();
        let page_manager_l4: &mut PageTable = unsafe { core::mem::transmute(l4_address) };
        let page_manager_entry: &mut PageTableEntry =
            &mut page_manager_l4.entries[indexer.index(PageMapLevel::Level4)];
        let page_directory_pointer_table: *mut PageTable;
        if !page_manager_entry.present() {
            page_directory_pointer_table =
                unsafe { core::mem::transmute(global_page_allocator.request_frame()) };
            unsafe {
                common::libc_functions::memset(
                    page_directory_pointer_table as *mut u8,
                    0,
                    PAGE_SIZE,
                )
            };

            page_manager_entry.set_address((page_directory_pointer_table as u64) >> 12);
            page_manager_entry.set_present(true);
            page_manager_entry.set_read_write(true);
        } else {
            page_directory_pointer_table =
                unsafe { core::mem::transmute(page_manager_entry.address() << 12) };
        }

        // Level 3: Page Directory Pointer Table -> Page Directory Table
        let page_directory_pointer_table = unsafe { &mut *page_directory_pointer_table };
        let page_directory_pointer_table_entry =
            &mut page_directory_pointer_table.entries[indexer.index(PageMapLevel::Level3)];
        let page_directory_table: *mut PageTable;
        if !page_directory_pointer_table_entry.present() {
            page_directory_table =
                unsafe { core::mem::transmute(global_page_allocator.request_frame()) };
            unsafe {
                common::libc_functions::memset(page_directory_table as *mut u8, 0, PAGE_SIZE)
            };

            page_directory_pointer_table_entry.set_address((page_directory_table as u64) >> 12);
            page_directory_pointer_table_entry.set_present(true);
            page_directory_pointer_table_entry.set_read_write(true);
        } else {
            page_directory_table =
                unsafe { core::mem::transmute(page_directory_pointer_table_entry.address() << 12) };
        }

        // Level 2: Page Directory Table -> Page Table
        let page_directory_table = unsafe { &mut *page_directory_table };
        let page_directory_table_entry =
            &mut page_directory_table.entries[indexer.index(PageMapLevel::Level2)];
        let page_table: *mut PageTable;
        if !page_directory_table_entry.present() {
            page_table = unsafe { core::mem::transmute(global_page_allocator.request_frame()) };
            unsafe { common::libc_functions::memset(page_table as *mut u8, 0, PAGE_SIZE) };

            page_directory_table_entry.set_address((page_table as u64) >> 12);
            page_directory_table_entry.set_present(true);
            page_directory_table_entry.set_read_write(true);
        } else {
            page_table =
                unsafe { core::mem::transmute(page_directory_table_entry.address() << 12) };
        }

        // Level 1: Page Table -> Page
        let page_table = unsafe { &mut *page_table };
        let page_table_entry = &mut page_table.entries[indexer.index(PageMapLevel::Level1)];
        let address_physical: u64 = address_physical.into();
        page_table_entry.set_address(address_physical >> 12);
        page_table_entry.set_present(true);
        page_table_entry.set_read_write(true);
    }
}
