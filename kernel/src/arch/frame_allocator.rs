use core::ops::Range;

use common::{
    arch::{Address64, AddressPhysical},
    efi_memory_descriptor::{EFIMemoryDescriptor, EFIMemoryDescriptorList, EFIMemoryType},
};
use log::*;
use spin::Mutex;

use crate::{
    arch::PhysicalFrame,
    constants::{BITS_IN_BYTE, FRAME_SIZE, PAGE_SIZE},
    utilities::Bitmap,
};

static mut FRAME_ALLOCATOR: Option<Mutex<FrameAllocator>> = None;

const PAGE_ALLOCATED: bool = true;
const PAGE_FREE: bool = false;
const MAX_UNMAPPABLE_MEMORY_REGIONS: usize = 32;

pub fn configure(memory_descriptors: &EFIMemoryDescriptorList) {
    assert!(unsafe { FRAME_ALLOCATOR.is_none() });

    // SAFETY: Call is safe since initialization happens at kernel start-up before
    // multi-core/multi-threading is enabled.
    unsafe {
        FRAME_ALLOCATOR = Some(Mutex::new(FrameAllocator::new(*memory_descriptors)));
    }
}

pub fn instance() -> &'static Mutex<FrameAllocator> {
    assert!(unsafe { FRAME_ALLOCATOR.is_some() });

    unsafe { FRAME_ALLOCATOR.as_ref().unwrap() }
}

#[derive(Copy, Clone)]
struct MemoryRegion {
    pub start_address: Address64,
    pub end_address: Address64,
    pub size: usize,
}

pub struct FrameAllocator {
    used_memory: usize,
    reserved_memory: usize,
    free_memory: usize,
    total_memory: usize,
    unmappable_memory: [Range<Address64>; MAX_UNMAPPABLE_MEMORY_REGIONS],
    unmappable_memory_len: usize,
    unmappable_memory_bytes: usize,
    frame_allocation_table: Bitmap,
    last_allocated_frame_index: usize,
}

impl FrameAllocator {
    fn new(mut descriptor_list: EFIMemoryDescriptorList) -> Self {
        let mut this = Self {
            used_memory: 0,
            reserved_memory: 0,
            free_memory: 0,
            total_memory: total_memory(&descriptor_list),
            unmappable_memory: unsafe { core::mem::zeroed() },
            unmappable_memory_len: 0,
            unmappable_memory_bytes: 0,
            frame_allocation_table: Bitmap::empty(),
            last_allocated_frame_index: 0,
        };

        descriptor_list
            .as_slice_mut()
            .sort_unstable_by(|a, b| a.physical_start.cmp(&b.physical_start));

        let descriptor_list = descriptor_list.as_slice();

        let mut unusable_mem_regions = [MemoryRegion {
            start_address: Address64::from(0_usize),
            end_address: Address64::from(0_usize),
            size: 0,
        }; MAX_UNMAPPABLE_MEMORY_REGIONS];
        let (unusable_mem_regions, total_unusable_memory) =
            unusable_memory_regions(descriptor_list, &mut unusable_mem_regions);

        this.unmappable_memory_len = unusable_mem_regions.len();
        for (i, region) in unusable_mem_regions.iter().enumerate() {
            this.unmappable_memory[i] = Range {
                start: region.start_address,
                end: region.end_address,
            }
        }
        this.unmappable_memory_bytes = total_unusable_memory;

        this.free_memory = this.total_memory + total_unusable_memory;

        this.initialize_frame_allocation_table(
            this.free_memory,
            largest_free_memory_region(descriptor_list, EFIMemoryType::CONVENTIONAL),
        );

        this.reserve_unusable_memory(unusable_mem_regions);

        // Mark the pages that are not EfiConventionalMemory as reserved
        for descriptor in descriptor_list {
            if descriptor.r#type != EFIMemoryType::CONVENTIONAL
                && descriptor.r#type != EFIMemoryType::CUSTOM_MEMORY_DESCRIPTORS
            {
                this.reserve_frames(descriptor.physical_start, descriptor.page_count as usize);
            }
        }

        assert!(this.free_memory < this.total_memory);

        this
    }

    pub fn free_memory(&self) -> usize {
        self.free_memory
    }

    pub fn used_memory(&self) -> usize {
        self.used_memory
    }

    pub fn reserved_memory(&self) -> usize {
        self.reserved_memory
    }

    pub fn total_memory(&self) -> usize {
        self.total_memory
    }

    #[allow(dead_code)]
    pub fn unmappable_memory(&self) -> &[Range<Address64>] {
        &self.unmappable_memory[..self.unmappable_memory_len]
    }

    pub fn physical_address_range(&self) -> Range<AddressPhysical> {
        Range {
            start: AddressPhysical::from(0_usize),
            end: AddressPhysical::from(self.total_memory + self.unmappable_memory_bytes),
        }
    }

    pub fn request_frame(&mut self) -> PhysicalFrame {
        for i in self.last_allocated_frame_index + 1..self.frame_allocation_table.bit_count() {
            let page_allocated = self.frame_allocation_table.get(i);
            if page_allocated {
                continue;
            }

            let address = AddressPhysical::from(i * FRAME_SIZE);

            self.lock_frame(address);
            self.last_allocated_frame_index = i;

            return PhysicalFrame::new(address);
        }

        panic!("Failed to allocate physical frame, OOM");
    }

    fn initialize_frame_allocation_table(&mut self, free_memory: usize, buffer: MemoryRegion) {
        let frame_table_size = free_memory / PAGE_SIZE / BITS_IN_BYTE + 1_usize;

        assert!(
            frame_table_size <= buffer.size,
            "Not enough memory to store frame allocation table"
        );

        self.frame_allocation_table =
            Bitmap::new_zeroed(buffer.start_address.as_mut_ptr(), frame_table_size);

        info!(
            "Total number of system memory frames: {}",
            frame_table_size * BITS_IN_BYTE
        );

        // Lock the frames that are used up by the allocation table
        self.lock_frames(
            AddressPhysical::from(buffer.start_address),
            common::page_count!(frame_table_size),
        );
    }

    fn reserve_unusable_memory(&mut self, unusable_mem_regions: &[MemoryRegion]) {
        // Contains EFI framebuffer
        for mem_region in unusable_mem_regions {
            let frame_count = common::page_count!(mem_region.size);
            self.reserve_frames(AddressPhysical::from(mem_region.start_address), frame_count)
        }
    }

    fn lock_frame(&mut self, address: AddressPhysical) {
        let address: usize = address.into();
        let index = address / FRAME_SIZE;

        let frame_locked = self.frame_allocation_table.get(index);
        if frame_locked {
            return;
        }

        self.frame_allocation_table.set(index, PAGE_ALLOCATED);
        self.free_memory -= FRAME_SIZE;
        self.used_memory += FRAME_SIZE;
    }

    #[allow(dead_code)]
    fn lock_frames(&mut self, address: AddressPhysical, count: usize) {
        for i in 0..(count as usize) {
            self.lock_frame(address + (i * FRAME_SIZE));
        }
    }

    fn unlock_frame(&mut self, address: AddressPhysical) {
        let address: usize = address.into();
        let index = address / FRAME_SIZE;

        let frame_locked = self.frame_allocation_table.get(index);
        if !frame_locked {
            return;
        }

        self.frame_allocation_table.set(index, PAGE_FREE);
        self.free_memory += FRAME_SIZE;
        self.used_memory -= FRAME_SIZE;

        if self.last_allocated_frame_index > index {
            self.last_allocated_frame_index = index;
        }
    }

    #[allow(dead_code)]
    fn unlock_frames(&mut self, address: AddressPhysical, count: usize) {
        for i in 0..count {
            self.unlock_frame(address + (i * FRAME_SIZE));
        }
    }

    fn reserve_frame(&mut self, address: AddressPhysical) {
        let address: usize = address.into();
        let index = address / FRAME_SIZE;

        let frame_locked = self.frame_allocation_table.get(index);
        if frame_locked {
            return;
        }

        self.frame_allocation_table.set(index, PAGE_ALLOCATED);
        self.free_memory -= FRAME_SIZE;
        self.reserved_memory += FRAME_SIZE;
    }

    fn reserve_frames(&mut self, address: AddressPhysical, count: usize) {
        for i in 0..count {
            self.reserve_frame(address + (i * FRAME_SIZE));
        }
    }

    #[allow(dead_code)]
    fn release_frame(&mut self, address: AddressPhysical) {
        let address: usize = address.into();
        let index = address / FRAME_SIZE;

        let frame_locked = self.frame_allocation_table.get(index);
        if !frame_locked {
            return;
        }

        self.frame_allocation_table.set(index, PAGE_FREE);
        self.free_memory += FRAME_SIZE;
        self.reserved_memory -= FRAME_SIZE;

        if self.last_allocated_frame_index > index {
            self.last_allocated_frame_index = index;
        }
    }

    #[allow(dead_code)]
    fn release_frames(&mut self, address: AddressPhysical, count: usize) {
        for i in 0..count {
            self.reserve_frame(address + (i * FRAME_SIZE));
        }
    }
}

fn total_memory(descriptor_list: &EFIMemoryDescriptorList) -> usize {
    let mut total_frames = 0_usize;

    for descriptor in descriptor_list.as_slice() {
        total_frames += descriptor.page_count as usize;
    }

    total_frames * FRAME_SIZE
}

fn largest_free_memory_region(
    efi_memory_descriptors: &[EFIMemoryDescriptor],
    memory_type: EFIMemoryType,
) -> MemoryRegion {
    let mut largest_free_descriptor_address = Address64::from(0_u64);
    let mut largest_free_descriptor_size = 0_usize;

    for descriptor in efi_memory_descriptors {
        let descriptor_size = common::byte_count!(descriptor.page_count);
        if descriptor.r#type == memory_type && descriptor_size > largest_free_descriptor_size {
            largest_free_descriptor_address = descriptor.physical_start;
            largest_free_descriptor_size = descriptor_size;
        }
    }

    MemoryRegion {
        start_address: largest_free_descriptor_address,
        end_address: largest_free_descriptor_address + largest_free_descriptor_size - 1_u64,
        size: largest_free_descriptor_size,
    }
}

fn unusable_memory_regions<'a>(
    efi_memory_descriptors: &[EFIMemoryDescriptor],
    unusable_memory_region_buffer: &'a mut [MemoryRegion],
) -> (&'a [MemoryRegion], usize) {
    let mut unusable_region_count = 0_usize;
    let mut total_unusable_memory = 0_usize;

    let mut previous_descriptor_end_address = Address64::from(0_u64);

    for descriptor in efi_memory_descriptors {
        if previous_descriptor_end_address != descriptor.physical_start {
            assert!(
                unusable_region_count < unusable_memory_region_buffer.len(),
                "Too many sparse memory segments (> 32), cannot continue"
            );

            let unusable_region = &mut unusable_memory_region_buffer[unusable_region_count];
            unusable_region.start_address = previous_descriptor_end_address;
            unusable_region.end_address = descriptor.physical_start;
            unusable_region.size =
                (descriptor.physical_start - previous_descriptor_end_address).as_usize();

            info!(
                "Found unmapped memory region, of {} bytes, from {} to {}, marking it as unusable",
                unusable_region.size, unusable_region.start_address, unusable_region.end_address
            );

            total_unusable_memory += unusable_region.size;
            unusable_region_count += 1;
        }

        previous_descriptor_end_address =
            descriptor.physical_start + (common::byte_count!(descriptor.page_count) as u64);
    }

    info!(
        "Found {} memory regions that are not mapped to a descriptor",
        unusable_region_count
    );

    (
        &unusable_memory_region_buffer[..unusable_region_count],
        total_unusable_memory,
    )
}
