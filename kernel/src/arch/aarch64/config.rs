pub mod cpu {
    use common::arch::read_multi_processor_affinity;
    use log::warn;

    pub fn configure() {
        read_multi_processor_affinity();
    }
}

pub mod paging {
    use log::warn;
    use spin::Mutex;

    use crate::arch::{AddressPhysical, AddressVirtual, FrameAllocator};

    pub fn configure(_frame_allocator: &Mutex<FrameAllocator>) {
        warn!("paging::configure: Paging not (yet) implemented for AARCH64");
    }

    pub fn activate() {
        warn!("paging::activate: Paging not (yet) implemented for AARCH64");
    }

    pub fn map(_address_virtual: AddressVirtual, _address_physical: AddressPhysical) {}
}
