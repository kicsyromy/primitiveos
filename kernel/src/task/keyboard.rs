use conquer_once::spin::OnceCell;
use core::{
    pin::Pin,
    task::{Context, Poll},
};
use crossbeam::queue::ArrayQueue;
use futures_util::{stream::Stream, task::AtomicWaker, StreamExt};
use log::*;
use pc_keyboard::{layouts, DecodedKey, HandleControl, Keyboard, ScancodeSet1};

use crate::drivers::{self, Console};

static SCANCODE_QUEUE: OnceCell<ArrayQueue<u8>> = OnceCell::uninit();
static WAKER: AtomicWaker = AtomicWaker::new();

pub struct ScancodeStream {
    _dummy: (),
}

impl ScancodeStream {
    pub fn new() -> Self {
        SCANCODE_QUEUE
            .try_init_once(|| ArrayQueue::new(100))
            .expect("Failed to initialize scan code queue");
        ScancodeStream { _dummy: () }
    }
}

impl Stream for ScancodeStream {
    type Item = u8;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let queue = SCANCODE_QUEUE.try_get().unwrap();

        let scan_code = queue.pop();
        if scan_code.is_some() {
            return Poll::Ready(scan_code);
        }

        WAKER.register(&cx.waker());

        // In the case where a wakeup happens right before returning the queue needs to be checked again
        // In case a keyboard event happened while the waker was being registered the queue needs to be checked again
        match queue.pop() {
            Some(scan_code) => {
                WAKER.take();
                Poll::Ready(Some(scan_code))
            }
            None => Poll::Pending,
        }
    }
}

pub fn queue_scan_code(scan_code: u8) {
    if !SCANCODE_QUEUE.is_initialized() {
        error!("Scan code queue is not initialized");
        return;
    }

    // SAFETY: A check is performed beforehand to make sure the queue is initialized
    let queue = unsafe { SCANCODE_QUEUE.get_unchecked() };

    let push_result = queue.push(scan_code);
    if push_result.is_err() {
        warn!("Scan code queue is full, dropping input {}", scan_code);
        return;
    }

    WAKER.wake();
}

pub async fn handle_keyboard_events() {
    use alloc::string::ToString;

    let mut scan_code_stream = ScancodeStream::new();
    let mut keyboard = Keyboard::new(
        layouts::Us104Key,
        ScancodeSet1,
        HandleControl::MapLettersToUnicode,
    );

    loop {
        let scan_code = scan_code_stream.next().await.unwrap();
        let key_event = keyboard.add_byte(scan_code);
        if key_event.is_err() {
            warn!("Failed to process scan code {}", scan_code);
            continue;
        }
        let key_event = key_event.unwrap();
        if key_event.is_none() {
            warn!("Failed to process scan code {}", scan_code);
            continue;
        }

        let key_event = key_event.unwrap();
        let key = keyboard.process_keyevent(key_event.clone());
        if key.is_none() {
            continue;
        }

        let mut efi_fb_writer = drivers::EfiConsole::console().unwrap().lock();
        match key.unwrap() {
            DecodedKey::RawKey(key) => {
                let args = format_args!("{:?}", key).to_string();
                efi_fb_writer
                    .write_str_utf8(args.as_str(), 0xff317ecc)
                    .unwrap();
            }
            DecodedKey::Unicode(character) => {
                efi_fb_writer
                    .write_char_utf8(character, 0xff317ecc)
                    .unwrap();
            }
        }
    }
}
