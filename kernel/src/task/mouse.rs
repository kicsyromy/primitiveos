use crate::drivers;
use conquer_once::spin::OnceCell;
use core::{
    pin::Pin,
    task::{Context, Poll},
};
use crossbeam::queue::ArrayQueue;
use futures_util::{stream::Stream, task::AtomicWaker, StreamExt};
use log::*;

static MOUSE_DATA_QUEUE: OnceCell<ArrayQueue<u8>> = OnceCell::uninit();
static WAKER: AtomicWaker = AtomicWaker::new();

pub struct MouseDataStream {
    _dummy: (),
}

impl MouseDataStream {
    pub fn new() -> Self {
        MOUSE_DATA_QUEUE
            .try_init_once(|| ArrayQueue::new(100))
            .expect("Failed to initialize mouse code queue");
        MouseDataStream { _dummy: () }
    }
}

impl Stream for MouseDataStream {
    type Item = u8;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let queue = MOUSE_DATA_QUEUE.try_get().unwrap();

        let mouse_data = queue.pop();
        if mouse_data.is_some() {
            return Poll::Ready(mouse_data);
        }

        WAKER.register(&cx.waker());

        // In the case where a wakeup happens right before returning the queue needs to be checked again
        // In case a mouse event happened while the waker was being registered the queue needs to be checked again
        match queue.pop() {
            Some(mouse_data) => {
                WAKER.take();
                Poll::Ready(Some(mouse_data))
            }
            None => Poll::Pending,
        }
    }
}

pub fn queue_mouse_event(mouse_data: u8) {
    if !MOUSE_DATA_QUEUE.is_initialized() {
        error!("Mouse data queue is not initialized");
        return;
    }

    // SAFETY: A check is performed beforehand to make sure the queue is initialized
    let queue = unsafe { MOUSE_DATA_QUEUE.get_unchecked() };

    let push_result = queue.push(mouse_data);
    if push_result.is_err() {
        warn!("Mouse code queue is full, dropping input {}", mouse_data);
        return;
    }

    WAKER.wake();
}

const Y_OVERFLOW: u8 = 0b10000000;
const X_OVERFLOW: u8 = 0b01000000;
const Y_SIGN_BIT: u8 = 0b00100000;
const X_SIGN_BIT: u8 = 0b00010000;
const ALWAYS_1: u8 = 0b00001000;
#[allow(dead_code)]
const MIDDLE_BUTTON: u8 = 0b00000100;
#[allow(dead_code)]
const RIGHT_BUTTON: u8 = 0b00000010;
#[allow(dead_code)]
const LEFT_BUTTON: u8 = 0b00000001;

pub async fn handle_mouse_events() {
    let mut mouse_stream = MouseDataStream::new();
    let mut mouse_cycle = 0_u8;
    let mut mouse_packet: [u8; 4] = [0_u8; 4];

    let mut mouse_x = 0_isize;
    let mut mouse_y = 0_isize;

    loop {
        let mouse_data = mouse_stream.next().await.unwrap();

        match mouse_cycle {
            0 => {
                if mouse_data & ALWAYS_1 == 0 {
                    continue;
                }
                mouse_packet[0] = mouse_data;
                mouse_cycle += 1;

                continue;
            }
            1 => {
                mouse_packet[1] = mouse_data;
                mouse_cycle += 1;

                continue;
            }
            2 => {
                mouse_packet[2] = mouse_data;
                mouse_cycle = 0;
            }
            _ => {}
        }

        let x_negative = (mouse_packet[0] & X_SIGN_BIT) != 0;
        let y_negative = (mouse_packet[0] & Y_SIGN_BIT) != 0;
        let x_overflow = (mouse_packet[0] & X_OVERFLOW) != 0;
        let y_overflow = (mouse_packet[0] & Y_OVERFLOW) != 0;

        if !x_negative {
            mouse_x += mouse_packet[1] as isize;
            if x_overflow {
                mouse_x += 255_isize;
            }
        } else {
            mouse_packet[1] = (256 as isize - mouse_packet[1] as isize) as u8;
            mouse_x -= mouse_packet[1] as isize;
            if x_overflow {
                mouse_x -= 255_isize;
            }
        }

        // NOTE: Inverse condition
        if y_negative {
            mouse_packet[2] = (256 as isize - mouse_packet[2] as isize) as u8;
            mouse_y += mouse_packet[2] as isize;
            if y_overflow {
                mouse_y += 255_isize;
            }
        } else {
            mouse_y -= mouse_packet[2] as isize;
            if y_overflow {
                mouse_y -= 255_isize;
            }
        }

        let fb = drivers::EfiGop::framebuffer().unwrap().lock();

        if mouse_x >= fb.width as isize {
            mouse_x = (fb.width - 1) as isize
        } else if mouse_x < 0 {
            mouse_x = 0
        }

        if mouse_y >= fb.height as isize {
            mouse_y = (fb.height - 1) as isize
        } else if mouse_y < 0 {
            mouse_y = 0
        }

        //fb_renderer.update_mouse_position(mouse_x as usize, mouse_y as usize);
    }
}
