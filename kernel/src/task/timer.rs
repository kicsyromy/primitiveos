use core::{
    pin::Pin,
    sync::atomic::{AtomicBool, AtomicUsize, Ordering},
    task::{Context, Poll},
};
use futures_util::{stream::Stream, task::AtomicWaker, StreamExt};
use log::*;
use spin::Mutex;

static TIMER_TICKED: AtomicBool = AtomicBool::new(false);
static WAKER: AtomicWaker = AtomicWaker::new();

static CURRENT_TICK_HANDLER: Mutex<fn()> = Mutex::new(initial_tick_handler);
static TICK_COUNT: AtomicUsize = AtomicUsize::new(0);

pub struct TimerStream {
    _dummy: (),
}

impl TimerStream {
    pub fn new() -> Self {
        TimerStream { _dummy: () }
    }
}

impl Stream for TimerStream {
    type Item = ();

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        if TIMER_TICKED.load(Ordering::Acquire) {
            TIMER_TICKED.store(false, Ordering::Release);
            return Poll::Ready(Some(()));
        }

        WAKER.register(&cx.waker());

        // In the case where a wakeup happens right before returning the queue needs to be checked again
        // In case a keyboard event happened while the waker was being registered the queue needs to be checked again
        if TIMER_TICKED.load(Ordering::Acquire) {
            WAKER.take();
            TIMER_TICKED.store(false, Ordering::Release);
            Poll::Ready(Some(()))
        } else {
            Poll::Pending
        }
    }
}

pub fn queue_timer_tick() {
    TIMER_TICKED.store(true, Ordering::Release);
    WAKER.wake();
}

pub async fn handle_timer_ticks() {
    let mut timer_stream = TimerStream::new();

    loop {
        timer_stream.next().await.unwrap();

        let handler: fn() = *(CURRENT_TICK_HANDLER.lock());
        (handler)();
    }
}

fn initial_tick_handler() {
    *(CURRENT_TICK_HANDLER.lock()) = main_tick_handler;
    TICK_COUNT.fetch_add(1, Ordering::Relaxed);
    // efi_framebuffer_renderer().lock().enable_double_buffering();
}

fn main_tick_handler() {
    let mut count = TICK_COUNT.fetch_add(1, Ordering::Relaxed);
    count += 1;

    if count % 500 == 0 {
        debug!(
            "{} ticks have passed since the system timer has started",
            count
        );
    }
    // efi_framebuffer_renderer().lock().swap_buffers();
}
