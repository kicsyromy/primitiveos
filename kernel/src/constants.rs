pub const BITS_IN_BYTE: usize = 8;
pub use common::boot_information::PAGE_SIZE;
pub const FRAME_SIZE: usize = common::boot_information::PAGE_SIZE;
