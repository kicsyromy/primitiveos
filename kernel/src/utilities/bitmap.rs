use crate::constants::BITS_IN_BYTE;

pub struct Bitmap {
    buffer: *mut u8,
    byte_count: usize,
}

impl Bitmap {
    #[allow(dead_code)]
    pub const fn new(buffer: *mut u8, byte_count: usize) -> Self {
        Self { buffer, byte_count }
    }

    pub fn new_zeroed(buffer: *mut u8, byte_count: usize) -> Self {
        unsafe { common::libc_functions::memset(buffer, 0, byte_count) };

        Self { buffer, byte_count }
    }

    pub fn empty() -> Self {
        Self {
            buffer: core::ptr::null_mut(),
            byte_count: 0,
        }
    }

    #[inline]
    pub fn as_slice(&self) -> &[u8] {
        unsafe { core::slice::from_raw_parts(self.buffer, self.byte_count) }
    }

    #[inline]
    pub fn as_slice_mut(&mut self) -> &mut [u8] {
        unsafe { core::slice::from_raw_parts_mut(self.buffer, self.byte_count) }
    }

    pub fn bit_count(&self) -> usize {
        self.byte_count * BITS_IN_BYTE
    }

    pub fn get(&self, index: usize) -> bool {
        let byte_index = index / BITS_IN_BYTE;
        assert!(byte_index < self.byte_count, "Bitmap: Index out of range");

        let bit_index = index % BITS_IN_BYTE;
        let bit_indexer = 0b10000000_u8 >> bit_index;

        self.as_slice()[byte_index] & bit_indexer > 0
    }

    pub fn set(&mut self, index: usize, value: bool) {
        let byte_index = index / BITS_IN_BYTE;
        assert!(byte_index < self.byte_count, "Bitmap: Index out of range");

        let bit_index = index % BITS_IN_BYTE;
        let bit_indexer = 0b10000000_u8 >> bit_index;

        if value {
            self.as_slice_mut()[byte_index] |= bit_indexer
        } else {
            self.as_slice_mut()[byte_index] &= !bit_indexer
        }
    }
}
