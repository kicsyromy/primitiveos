use spin::Mutex;
use uart_16550::SerialPort;

pub const STD_SERIAL_PORT_ADDRESS: u16 = 0x3F8;

static mut SERIAL1: Option<Mutex<SerialPort>> = None;

pub fn init_serial1(port: u16) {
    let mut serial_port = unsafe { SerialPort::new(port) };
    serial_port.init();
    unsafe { SERIAL1 = Some(Mutex::new(serial_port)) }
}

fn serial1() -> &'static Mutex<SerialPort> {
    assert!(unsafe { SERIAL1.is_some() });
    unsafe { SERIAL1.as_ref().unwrap() }
}

fn write(text: &str) -> core::fmt::Result {
    use core::fmt::*;
    let serial1 = serial1().try_lock();

    if serial1.is_none() {
        return Err(core::fmt::Error::default());
    }

    serial1.unwrap().write_str(text)
}

pub struct SerialWriter;

impl core::fmt::Write for SerialWriter {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        write(s)
    }
}
