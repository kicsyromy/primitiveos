#[cfg(test)]
const QEMU_EXIT_CODE_SUCCESS: u32 = 0x10;
#[cfg(test)]
const QEMU_EXIT_CODE_FAIL: u32 = 0x11;

#[cfg(test)]
pub trait Testable {
    fn run(&self, writer: &mut crate::serial::SerialWriter);
}

#[cfg(test)]
impl<T> Testable for T
where
    T: Fn(),
{
    fn run(&self, writer: &mut crate::serial::SerialWriter) {
        use core::fmt::Write;
        write!(writer, "{}...\t\t", core::any::type_name::<T>()).unwrap();
        self();
        writeln!(writer, "[ok]").unwrap();
    }
}

#[cfg(test)]
pub fn test_runner(tests: &[&dyn Testable]) {
    use crate::serial::SerialWriter;
    use core::fmt::Write;
    let mut sw = SerialWriter {};
    writeln!(&mut sw, "Running {} tests...", tests.len()).unwrap();
    for test in tests {
        test.run(&mut sw);
    }

    exit_qemu(QEMU_EXIT_CODE_SUCCESS);
}

#[cfg(test)]
pub fn run_tests() {
    crate::test_main();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    use crate::serial::SerialWriter;
    use core::fmt::Write;
    let mut sw = SerialWriter {};
    writeln!(&mut sw, "[failed]\nError: {}", info).unwrap();

    exit_qemu(QEMU_EXIT_CODE_FAIL);

    loop {}
}

#[cfg(test)]
fn exit_qemu(exit_code: u32) {
    const EXIT_PORT: u8 = 0xf4;

    unsafe {
        asm! {
            "mov dx, {port}\n\
            out dx, {:e}\n",
            in(reg) exit_code,
            port = const EXIT_PORT
        }
    }
}
