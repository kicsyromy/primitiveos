use common::{
    arch::{Address, Address32, Address64},
    impl_parse_for_enum,
};

use crate::{
    acpi::sdt::{SDTHeader, SDTType, SDT_APIC},
    arch::AddressPhysical,
};

/// Multiple APIC Descriptor Table
/// The MADT describes all of the interrupt controllers in the system.
/// It can also be used to enumerate the processors currently available.

const MADT_HEADER_SIZE_BYTES: usize = core::mem::size_of::<MADTHeader>();

#[allow(dead_code)]
const LOCAL_APIC_SIZE_BYTES: usize = 6;
#[allow(dead_code)]
const IO_APIC_SIZE_BYTES: usize = 10;
#[allow(dead_code)]
const INTERRUPT_SOURCE_OVERRIDE_SIZE_BYTES: usize = 8;
#[allow(dead_code)]
const NON_MASKABLE_INTERRUPTS_SIZE_BYTES: usize = 4;
#[allow(dead_code)]
const LOCAL_APIC_ADDRESS_OVERRIDE_SIZE_BYTES: usize = 10;

#[allow(dead_code)]
#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(u8)]
pub enum MADTEntryType {
    LocalAPIC = 0x0,
    IoAPIC,
    InterruptSourceOverride,
    NonMaskableInterrupts = 0x4,
    LocalAPICAddressOverride,
    GicDistributor = 0xC,
}

impl_parse_for_enum!(MADTEntryType, u8, MADTHeaderParseError);

impl MADTEntryType {
    pub fn from_u8(value: u8) -> Option<Self> {
        match value {
            0x0 => Some(MADTEntryType::LocalAPIC),
            0x1 => Some(MADTEntryType::IoAPIC),
            0x2 => Some(MADTEntryType::InterruptSourceOverride),
            0x4 => Some(MADTEntryType::NonMaskableInterrupts),
            0x5 => Some(MADTEntryType::LocalAPICAddressOverride),
            0xC => Some(MADTEntryType::GicDistributor),
            _ => None,
        }
    }
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct MADTHeader {
    entry_type: MADTEntryType,
    record_length: u8,
}

impl MADTHeader {
    pub fn parse(address: AddressPhysical) -> super::ParseResult<Self, MADTHeaderParseError> {
        use common::binary_parser::parsers;
        use common::parse_tuple;

        let madt_header_slice = unsafe {
            core::slice::from_raw_parts_mut(address.as_mut_ptr(), MADT_HEADER_SIZE_BYTES)
        };

        let (_, (entry_type, record_length)) = parse_tuple!(
            madt_header_slice,
            (parse, MADTEntryType::parse, MADTHeaderParseError::EntryType),
            (parse, parsers::u8, MADTHeaderParseError::RecordLength)
        )?;

        Ok(Self {
            entry_type,
            record_length,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub enum MADTHeaderParseError {
    EntryType,
    RecordLength,
    Unknown,
}

impl Default for MADTHeaderParseError {
    fn default() -> Self {
        MADTHeaderParseError::Unknown
    }
}

#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
pub struct LocalAPIC {
    acpi_processor_id: u8,
    id: u8,
    flags: u32,
}

impl LocalAPIC {
    pub fn parse(address: Address) -> super::ParseResult<Self, LocalAPICParseError> {
        use common::binary_parser::parsers;
        use common::parse_tuple;

        let local_apic_slice =
            unsafe { core::slice::from_raw_parts_mut(address.as_mut_ptr(), LOCAL_APIC_SIZE_BYTES) };

        let (_, (acpi_processor_id, id, flags)) = parse_tuple!(
            local_apic_slice,
            (parse, parsers::u8, LocalAPICParseError::ProcessorId),
            (parse, parsers::u8, LocalAPICParseError::Id),
            (parse, parsers::u32, LocalAPICParseError::Flags)
        )?;

        Ok(Self {
            acpi_processor_id,
            id,
            flags,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub enum LocalAPICParseError {
    ProcessorId,
    Id,
    Flags,
    Unknown,
}

impl Default for LocalAPICParseError {
    fn default() -> Self {
        LocalAPICParseError::Unknown
    }
}

#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
pub struct IoAPIC {
    id: u8,
    _reserved: u8,
    address: Address32,
    global_system_interrupt_base: u32,
}

impl IoAPIC {
    pub fn parse(address: Address) -> super::ParseResult<Self, IoAPICParseError> {
        use common::binary_parser::parsers;
        use common::parse_tuple;

        let slice =
            unsafe { core::slice::from_raw_parts_mut(address.as_mut_ptr(), IO_APIC_SIZE_BYTES) };

        let (_, (id, _reserved, address, global_system_interrupt_base)) = parse_tuple!(
            slice,
            (parse, parsers::u8, IoAPICParseError::Id),
            (skip, 1_usize),
            (
                parse_map_result,
                parsers::u32,
                IoAPICParseError::Address,
                |value, _| Ok(Address32::from(value))
            ),
            (
                parse,
                parsers::u32,
                IoAPICParseError::GlobalSystemInterruptBase
            )
        )?;

        Ok(Self {
            id,
            _reserved: 0,
            address,
            global_system_interrupt_base,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub enum IoAPICParseError {
    Id,
    Address,
    GlobalSystemInterruptBase,
    Unknown,
}

impl Default for IoAPICParseError {
    fn default() -> Self {
        IoAPICParseError::Unknown
    }
}

#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
pub struct InterruptSourceOverride {
    bus_source: u8,
    irq_source: u8,
    global_system_interrupt: u32,
    flags: u16,
}

impl InterruptSourceOverride {
    pub fn parse(address: Address) -> super::ParseResult<Self, InterruptSourceOverrideParseError> {
        use common::binary_parser::parsers;
        use common::parse_tuple;

        let slice = unsafe {
            core::slice::from_raw_parts_mut(
                address.as_mut_ptr(),
                INTERRUPT_SOURCE_OVERRIDE_SIZE_BYTES,
            )
        };

        let (_, (bus_source, irq_source, global_system_interrupt, flags)) = parse_tuple!(
            slice,
            (
                parse,
                parsers::u8,
                InterruptSourceOverrideParseError::BusSource
            ),
            (
                parse,
                parsers::u8,
                InterruptSourceOverrideParseError::IRQSource
            ),
            (
                parse,
                parsers::u32,
                InterruptSourceOverrideParseError::GlobalSystemInterrupt
            ),
            (
                parse,
                parsers::u16,
                InterruptSourceOverrideParseError::Flags
            )
        )?;

        Ok(Self {
            bus_source,
            irq_source,
            global_system_interrupt,
            flags,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub enum InterruptSourceOverrideParseError {
    BusSource,
    IRQSource,
    GlobalSystemInterrupt,
    Flags,
    Unknown,
}

impl Default for InterruptSourceOverrideParseError {
    fn default() -> Self {
        InterruptSourceOverrideParseError::Unknown
    }
}

#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
pub struct NonMaskableInterrupts {
    acpi_processor_id: u8,
    flags: u16,
    lint: u8,
}

impl NonMaskableInterrupts {
    pub fn parse(address: Address) -> super::ParseResult<Self, NonMaskableInterruptsParseError> {
        use common::binary_parser::parsers;
        use common::parse_tuple;

        let slice = unsafe {
            core::slice::from_raw_parts_mut(
                address.as_mut_ptr(),
                NON_MASKABLE_INTERRUPTS_SIZE_BYTES,
            )
        };

        let (_, (acpi_processor_id, flags, lint)) = parse_tuple!(
            slice,
            (
                parse,
                parsers::u8,
                NonMaskableInterruptsParseError::ProcessorId
            ),
            (parse, parsers::u16, NonMaskableInterruptsParseError::Flags),
            (parse, parsers::u8, NonMaskableInterruptsParseError::Lint)
        )?;

        Ok(Self {
            acpi_processor_id,
            flags,
            lint,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub enum NonMaskableInterruptsParseError {
    ProcessorId,
    Flags,
    Lint,
    Unknown,
}

impl Default for NonMaskableInterruptsParseError {
    fn default() -> Self {
        NonMaskableInterruptsParseError::Unknown
    }
}

#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
pub struct LocalAPICAddressOverride {
    _reserved: u16,
    address: Address64,
}

impl LocalAPICAddressOverride {
    pub fn parse(address: Address) -> super::ParseResult<Self, LocalAPICAddressOverrideParseError> {
        use common::binary_parser::parsers;
        use common::parse_tuple;

        let slice = unsafe {
            core::slice::from_raw_parts_mut(
                address.as_mut_ptr(),
                LOCAL_APIC_ADDRESS_OVERRIDE_SIZE_BYTES,
            )
        };

        let (_, (_reserved, address)) = parse_tuple!(
            slice,
            (skip, 2),
            (
                parse_map_result,
                parsers::u64,
                LocalAPICAddressOverrideParseError::Address,
                |value, _| Ok(Address64::from(value))
            )
        )?;

        Ok(Self {
            _reserved: 0,
            address,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub enum LocalAPICAddressOverrideParseError {
    Address,
    Unknown,
}

impl Default for LocalAPICAddressOverrideParseError {
    fn default() -> Self {
        LocalAPICAddressOverrideParseError::Unknown
    }
}

#[derive(Copy, Clone)]
pub union RecordData {
    local_apic: LocalAPIC,
    io_apic: IoAPIC,
    interrupt_source_override: InterruptSourceOverride,
    non_maskable_interrupts: NonMaskableInterrupts,
    local_apic_address_override: LocalAPICAddressOverride,
    unimplemented: (),
}

#[derive(Copy, Clone)]
pub struct Record {
    header: MADTHeader,
    data: RecordData,
}

impl core::fmt::Debug for Record {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        // SAFETY: Access to union fields are safe since the header tells us the data type

        f.write_str("MADTRecord { ")?;
        f.write_fmt(format_args!("{:?}", self.header))?;
        f.write_str(", ")?;

        match self.header.entry_type {
            MADTEntryType::LocalAPIC => {
                f.write_fmt(format_args!("{:?}", unsafe { self.data.local_apic }))?;
            }
            MADTEntryType::IoAPIC => {
                f.write_fmt(format_args!("{:?}", unsafe { self.data.io_apic }))?;
            }
            MADTEntryType::InterruptSourceOverride => {
                f.write_fmt(format_args!("{:?}", unsafe {
                    self.data.interrupt_source_override
                }))?;
            }
            MADTEntryType::NonMaskableInterrupts => {
                f.write_fmt(format_args!("{:?}", unsafe {
                    self.data.non_maskable_interrupts
                }))?;
            }
            MADTEntryType::LocalAPICAddressOverride => {
                f.write_fmt(format_args!("{:?}", unsafe {
                    self.data.local_apic_address_override
                }))?;
            }
            MADTEntryType::GicDistributor => {
                f.write_fmt(format_args!("GIC Distributor (GICD)",))?;
            }
        }

        f.write_str(" }")
    }
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct MADT {
    sdt_header: SDTHeader,
    local_apic_address: Address32,
    flags: u32,
    records_address: AddressPhysical,
    records_size: usize,
}

impl MADT {
    pub fn iter(&self) -> MADTIterator {
        MADTIterator::new(self.records_address, self.records_size)
    }
}

impl SDTType<MADT, MADTParseError> for MADT {
    fn signature() -> &'static [u8] {
        SDT_APIC
    }

    fn parse(
        header: SDTHeader,
        address: AddressPhysical,
        byte_count: usize,
    ) -> super::ParseResult<MADT, MADTParseError> {
        use static_assertions::*;

        use common::binary_parser::parsers;
        use common::parse_tuple;

        const_assert_eq!(LOCAL_APIC_SIZE_BYTES, 6);
        const_assert_eq!(IO_APIC_SIZE_BYTES, 10);
        const_assert_eq!(INTERRUPT_SOURCE_OVERRIDE_SIZE_BYTES, 8);
        const_assert_eq!(NON_MASKABLE_INTERRUPTS_SIZE_BYTES, 4);
        const_assert_eq!(LOCAL_APIC_ADDRESS_OVERRIDE_SIZE_BYTES, 10);

        let start_fields_size: usize =
            core::mem::size_of::<Address32>() + core::mem::size_of::<u32>();
        let records_address = address + start_fields_size;
        let records_size = byte_count - start_fields_size;

        let slice =
            unsafe { core::slice::from_raw_parts_mut(address.as_mut_ptr(), start_fields_size) };

        let (_, (local_apic_address, flags)) = parse_tuple!(
            slice,
            (
                parse_map_result,
                parsers::u32,
                MADTParseError::LocalAPICAddress,
                |value, _| Ok(Address32::from(value))
            ),
            (parse, parsers::u32, MADTParseError::Flags)
        )?;

        Ok(Self {
            sdt_header: header,
            local_apic_address,
            flags,
            records_address,
            records_size,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub enum MADTParseError {
    LocalAPICAddress,
    Flags,
    Unknown,
}

impl Default for MADTParseError {
    fn default() -> Self {
        MADTParseError::Unknown
    }
}

pub struct MADTIterator {
    current_address: AddressPhysical,
    remaining_records_size: usize,
}

impl MADTIterator {
    pub fn new(records_address: AddressPhysical, records_size: usize) -> Self {
        Self {
            current_address: records_address,
            remaining_records_size: records_size,
        }
    }
}

impl Iterator for MADTIterator {
    type Item = Record;

    fn next(&mut self) -> Option<Self::Item> {
        use log::error;

        if self.remaining_records_size == 0 {
            return None;
        }

        let header = MADTHeader::parse(self.current_address)
            .map_err(|e| {
                error!("Failed to parse MADTHeader: {:?}", e);
                e
            })
            .ok()?;

        let record_address = self.current_address + MADT_HEADER_SIZE_BYTES;
        // header.record_length contains the size in bytes of the record *inclusive* of the header itself
        let record_data: RecordData;
        match header.entry_type {
            MADTEntryType::LocalAPIC => {
                let value = LocalAPIC::parse(record_address)
                    .map_err(|e| {
                        error!("Failed to parse Local APIC MADT record: {:?}", e);
                        e
                    })
                    .ok()?;
                record_data = RecordData { local_apic: value };
            }
            MADTEntryType::IoAPIC => {
                let value = IoAPIC::parse(record_address)
                    .map_err(|e| {
                        error!("Failed to parse IO APIC MADT record: {:?}", e);
                        e
                    })
                    .ok()?;
                record_data = RecordData { io_apic: value };
            }
            MADTEntryType::InterruptSourceOverride => {
                let value = InterruptSourceOverride::parse(record_address)
                    .map_err(|e| {
                        error!(
                            "Failed to parse Interrupt Source Override MADT record: {:?}",
                            e
                        );
                        e
                    })
                    .ok()?;
                record_data = RecordData {
                    interrupt_source_override: value,
                };
            }
            MADTEntryType::NonMaskableInterrupts => {
                let value = NonMaskableInterrupts::parse(record_address)
                    .map_err(|e| {
                        error!(
                            "Failed to parse Non Maskable Interrupts MADT record: {:?}",
                            e
                        );
                        e
                    })
                    .ok()?;
                record_data = RecordData {
                    non_maskable_interrupts: value,
                };
            }
            MADTEntryType::LocalAPICAddressOverride => {
                let value = LocalAPICAddressOverride::parse(record_address)
                    .map_err(|e| {
                        error!(
                            "Failed to parse Local APIC Address Override MADT record: {:?}",
                            e
                        );
                        e
                    })
                    .ok()?;
                record_data = RecordData {
                    local_apic_address_override: value,
                };
            }
            _ => record_data = RecordData { unimplemented: () },
        }

        self.current_address = self.current_address + header.record_length as usize;
        self.remaining_records_size = self.remaining_records_size - header.record_length as usize;

        Some(Record {
            header,
            data: record_data,
        })
    }
}
