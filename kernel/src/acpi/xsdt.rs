use crate::{
    acpi::sdt::{SDTHeader, SDT, SDT_HEADER_SIZE_BYTES},
    arch::AddressPhysical,
};

/// RSDT (Root System Description Table) is a data structure used in the ACPI programming interface.
/// This table contains pointers to all the other System Description Tables.
///
/// The (X/R)SDT is the main System Description Table. However there are many kinds of SDT.
/// All the SDT may be split into two parts. One (the header) which is common to all the SDT and
/// another (data) which is different for each table.

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct XSDT {
    header: SDTHeader,
    address: AddressPhysical,
    sdt_entries_start: AddressPhysical,
    sdt_entry_count: usize,
}

impl XSDT {
    pub unsafe fn parse(address: AddressPhysical) -> Option<Self> {
        use log::error;

        use crate::acpi::validate_sdt_checksum;

        let xsdt_header = SDTHeader::parse(address)
            .map_err(|e| {
                error!("XSDT: Failed to parse SDT header: {:?}", e);
                e
            })
            .ok()?;

        let entry_count = {
            let header_len = xsdt_header.length as usize;
            (header_len - SDT_HEADER_SIZE_BYTES) / core::mem::size_of::<u64>()
        };

        let this = Self {
            header: xsdt_header,
            address,
            sdt_entries_start: address + SDT_HEADER_SIZE_BYTES,
            sdt_entry_count: entry_count,
        };

        if validate_sdt_checksum(this.address, this.header.length as usize).is_err() {
            return None;
        }

        Some(this)
    }

    #[allow(dead_code)]
    pub fn len(&self) -> usize {
        self.sdt_entry_count
    }

    pub fn iter(&self) -> XSDTIterator {
        XSDTIterator::new(self, self.sdt_entry_count)
    }

    pub fn sdt_entry(&self, index: usize) -> Option<SDT> {
        let entry_address =
            usize::from(self.sdt_entries_start) + (index * core::mem::size_of::<AddressPhysical>());
        let sdt_address = unsafe { AddressPhysical::from(*(entry_address as *const usize)) };

        SDT::parse(sdt_address)
    }
}

pub struct XSDTIterator<'a> {
    xsdt: &'a XSDT,
    index: usize,
    length: usize,
}

impl<'a> XSDTIterator<'a> {
    pub fn new(xsdt: &'a XSDT, length: usize) -> Self {
        Self {
            xsdt,
            index: 0,
            length,
        }
    }
}

impl<'a> core::iter::Iterator for XSDTIterator<'a> {
    type Item = Option<SDT>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index == self.length {
            return None;
        }

        let sdt = self.xsdt.sdt_entry(self.index);
        self.index += 1;

        Some(sdt)
    }
}
