use crate::arch::AddressPhysical;
use common::binary_parser::ParseError;

/// System Description Table contains a common header between all tables and data that contains
/// table specific data.

/// Multiple APIC Description Table (MADT)
#[allow(dead_code)]
pub const SDT_APIC: &[u8] = b"APIC";
/// Boot Error Record Table (BERT)
#[allow(dead_code)]
pub const SDT_BERT: &[u8] = b"BERT";
/// Corrected Platform Error Polling Table (CPEP)
#[allow(dead_code)]
pub const SDT_CPEP: &[u8] = b"CPEP";
/// Differentiated System Description Table (DSDT)
#[allow(dead_code)]
pub const SDT_DSDT: &[u8] = b"DSDT";
/// Embedded Controller Boot Resources Table (ECDT)
#[allow(dead_code)]
pub const SDT_ECDT: &[u8] = b"ECDT";
/// Error Injection Table (EINJ)
#[allow(dead_code)]
pub const SDT_EINJ: &[u8] = b"EINJ";
/// Error Record Serialization Table (ERST)
#[allow(dead_code)]
pub const SDT_ERST: &[u8] = b"ERST";
/// Fixed ACPI Description Table (FADT)
#[allow(dead_code)]
pub const SDT_FACP: &[u8] = b"FACP";
/// Firmware ACPPI Control Structure (FACS)
#[allow(dead_code)]
pub const SDT_FACS: &[u8] = b"FACS";
/// Hardware Error Source Table (HEST)
#[allow(dead_code)]
pub const SDT_HEST: &[u8] = b"HEST";
/// Maximum System Characteristics Table (MSCT)
#[allow(dead_code)]
pub const SDT_MSCT: &[u8] = b"MSCT";
/// Memory Power State Table (MPST)
#[allow(dead_code)]
pub const SDT_MPST: &[u8] = b"MPST";
/// Platform Memory Topology Table (PMTT)
#[allow(dead_code)]
pub const SDT_PMTT: &[u8] = b"PMTT";
/// Persistent System Description Table (PSDT)
#[allow(dead_code)]
pub const SDT_PSDT: &[u8] = b"PSDT";
/// ACPI RAS FeatureTable (RASF)
#[allow(dead_code)]
pub const SDT_RASF: &[u8] = b"RASF";
/// Root System Description Table (This wiki page; included for completeness)
#[allow(dead_code)]
pub const SDT_RSDT: &[u8] = b"RSDT";
/// Smart Battery Specification Table (SBST)
#[allow(dead_code)]
pub const SDT_SBST: &[u8] = b"SBST";
/// System Locality System Information Table (SLIT)
#[allow(dead_code)]
pub const SDT_SLIT: &[u8] = b"SLIT";
/// System Resource Affinity Table (SRAT)
#[allow(dead_code)]
pub const SDT_SRAT: &[u8] = b"SRAT";
/// Secondary System Description Table (SSDT)
#[allow(dead_code)]
pub const SDT_SSDT: &[u8] = b"SSDT";
/// Extended System Description Table (XSDT; 64-bit version of the RSDT)
#[allow(dead_code)]
pub const SDT_XSDT: &[u8] = b"XSDT";

pub const SDT_HEADER_SIZE_BYTES: usize = core::mem::size_of::<SDTHeader>();
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SDTHeader {
    pub signature: [u8; 4],
    pub length: u32,
    pub revision: u8,
    pub checksum: u8,
    pub oem_id: [u8; 6],
    pub oem_table_id: [u8; 8],
    pub oem_revision: u32,
    pub creator_id: u32,
    pub creator_revision: u32,
}

impl SDTHeader {
    pub fn parse(address: AddressPhysical) -> super::ParseResult<Self, SDTHeaderParseError> {
        use common::binary_parser::*;
        use common::parse_tuple;

        let sdt_header_slice =
            unsafe { core::slice::from_raw_parts_mut(address.as_mut_ptr(), SDT_HEADER_SIZE_BYTES) };

        let (
            _,
            (
                signature,
                length,
                revision,
                checksum,
                oem_id,
                oem_table_id,
                oem_revision,
                creator_id,
                creator_revision,
            ),
        ) = parse_tuple!(
            sdt_header_slice,
            (passthrough, 4_usize, |x, _| {
                let mut signature = [0_u8; 4];
                signature.copy_from_slice(x);
                Ok(signature)
            }),
            (parse, parsers::u32, SDTHeaderParseError::Length),
            (parse, parsers::u8, SDTHeaderParseError::Revision),
            (parse, parsers::u8, SDTHeaderParseError::Checksum),
            (passthrough, 6_usize, |x, _| {
                let mut signature = [0_u8; 6];
                signature.copy_from_slice(x);
                Ok(signature)
            }),
            (passthrough, 8_usize, |x, _| {
                let mut signature = [0_u8; 8];
                signature.copy_from_slice(x);
                Ok(signature)
            }),
            (parse, parsers::u32, SDTHeaderParseError::OemRevision),
            (parse, parsers::u32, SDTHeaderParseError::CreatorId),
            (parse, parsers::u32, SDTHeaderParseError::CreatorRevision)
        )?;

        Ok(Self {
            signature,
            length,
            revision,
            checksum,
            oem_id,
            oem_table_id,
            oem_revision,
            creator_id,
            creator_revision,
        })
    }
}

impl core::fmt::Debug for SDTHeader {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str("SDTHeader { ")?;
        f.write_fmt(format_args!("Signature: \"{}\", ", unsafe {
            core::str::from_utf8_unchecked(&self.signature)
        }))?;
        f.write_fmt(format_args!("Length: {}, ", self.length))?;
        f.write_fmt(format_args!("Revision: {}, ", self.revision))?;
        f.write_fmt(format_args!("Checksum: {}, ", self.checksum))?;
        f.write_fmt(format_args!(
            "OEM ID: {}, ",
            unsafe { core::str::from_utf8_unchecked(&self.oem_id) }.trim_end()
        ))?;
        f.write_fmt(format_args!(
            "OEM Table ID: {}, ",
            unsafe { core::str::from_utf8_unchecked(&self.oem_table_id) }.trim_end()
        ))?;
        f.write_fmt(format_args!("OEM Revision: {}, ", self.oem_revision))?;
        f.write_fmt(format_args!("Creator ID: {}, ", self.creator_id))?;
        f.write_fmt(format_args!(
            "Creator Revision: {}, ",
            self.creator_revision
        ))?;
        f.write_str("}")
    }
}

#[derive(Debug, Copy, Clone)]
pub enum SDTHeaderParseError {
    Length,
    Revision,
    Checksum,
    OemRevision,
    CreatorId,
    CreatorRevision,
    Unknown,
}

impl Default for SDTHeaderParseError {
    fn default() -> Self {
        SDTHeaderParseError::Unknown
    }
}

impl<'a> From<common::binary_parser::ParseError<'a, SDTHeaderParseError>> for SDTHeaderParseError {
    fn from(e: ParseError<'a, SDTHeaderParseError>) -> Self {
        e.internal_error().clone()
    }
}

#[derive(Debug, Copy, Clone)]
pub struct SDT {
    header: SDTHeader,
    data_ptr: AddressPhysical,
}

impl SDT {
    pub fn parse(address: AddressPhysical) -> Option<Self> {
        use log::error;

        use crate::acpi::validate_sdt_checksum;

        let this = Self {
            header: SDTHeader::parse(address)
                .map_err(|e| {
                    error!("SDT: Failed to parse SDT header: {:?}", e);
                    e
                })
                .ok()?,
            data_ptr: address + SDT_HEADER_SIZE_BYTES,
        };

        if validate_sdt_checksum(address, this.header.length as usize).is_err() {
            return None;
        }

        Some(this)
    }

    #[allow(dead_code)]
    pub fn header(&self) -> &SDTHeader {
        &self.header
    }

    #[allow(dead_code)]
    pub fn data<SDT: SDTType<SDT, ParseError>, ParseError: core::fmt::Debug + Default + Sized>(
        &self,
    ) -> Option<SDT> {
        if SDT::signature() != self.header.signature {
            return None;
        }

        SDT::parse(
            self.header,
            self.data_ptr,
            self.header.length as usize - SDT_HEADER_SIZE_BYTES,
        )
        .ok()
    }
}

pub trait SDTType<SDT, ParseError: core::fmt::Debug + Default + Sized> {
    fn signature() -> &'static [u8];
    fn parse(
        header: SDTHeader,
        address: AddressPhysical,
        byte_count: usize,
    ) -> super::ParseResult<SDT, ParseError>;
}
