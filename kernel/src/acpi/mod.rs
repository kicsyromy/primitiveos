use crate::arch::AddressPhysical;

pub mod madt;
pub mod sdt;
pub mod xsdt;

type ParseResult<T, E> = core::result::Result<T, common::binary_parser::ParseError<'static, E>>;

fn validate_sdt_checksum(address: AddressPhysical, byte_count: usize) -> Result<(), ()> {
    let self_bytes =
        unsafe { core::slice::from_raw_parts(usize::from(address) as *const u8, byte_count) };

    let mut sum = 0_u8;
    for byte in self_bytes {
        sum = sum.wrapping_add(*byte);
    }

    if sum != 0 {
        return Err(());
    }

    Ok(())
}
