#![no_std]
#![no_main]
#![feature(alloc_error_handler)]
#![feature(asm)]
#![feature(asm_const)]
#![feature(asm_sym)]
#![feature(global_asm)]
#![feature(naked_functions)]
#![feature(custom_test_frameworks)]
#![test_runner(crate::test::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;
extern crate log;

use common::{
    acpi::{RSDPDescriptorV1, RSDPDescriptorV2},
    boot_information::BootInformation,
};
use log::*;

use crate::logger::EfiConsoleWriter;
#[cfg(target_arch = "x86_64")]
use crate::serial::SerialWriter;
use crate::{
    acpi::{
        madt::{MADTParseError, MADT},
        sdt::SDT_APIC,
    },
    arch::AddressPhysical,
    task::{executor::Executor, Task},
};

mod acpi;
mod allocator;
mod arch;
mod constants;
mod drivers;
mod logger;
mod task;
mod test;
mod utilities;

#[cfg(target_arch = "x86_64")]
mod serial;

static mut BOOT_INFORMATION: Option<BootInformation> = None;

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    unsafe {
        drivers::EfiConsole::console().unwrap().force_unlock();
        drivers::EfiGop::framebuffer().unwrap().force_unlock();
    }
    error!("Kernel panic: {:?}", info);
    loop {}
}

#[alloc_error_handler]
fn alloc_error_handler(layout: core::alloc::Layout) -> ! {
    panic!("allocation error: {:?}", layout)
}

fn print_boot_info() {
    let base_address = drivers::EfiGop::framebuffer().unwrap().lock().base_address;
    info!("Framebuffer base address: {}", base_address);

    let page_allocator = arch::frame_allocator::instance().lock();
    info!(
        "Total available memory: {} bytes",
        page_allocator.total_memory()
    );
    info!("Free memory: {} bytes", page_allocator.free_memory());
    info!("Used memory: {} bytes", page_allocator.used_memory());
    info!(
        "Reserved memory: {} bytes",
        page_allocator.reserved_memory()
    );
}

fn print_acpi_table_info(
    rsdp_descriptor_v1: Option<RSDPDescriptorV1>,
    rsdp_descriptor_v2: RSDPDescriptorV2,
) {
    if rsdp_descriptor_v1.is_some() {
        let rsdp_descriptor_v1 = rsdp_descriptor_v1.unwrap();
        info!("{:?}", rsdp_descriptor_v1);
    }

    info!("XSDT Pointer: {:?}", rsdp_descriptor_v2);

    let xsdt =
        unsafe { acpi::xsdt::XSDT::parse(AddressPhysical::from(rsdp_descriptor_v2.xsdt_address)) };

    if xsdt.is_none() {
        panic!("Failed to find XSDT");
    }

    let xsdt = xsdt.unwrap();
    info!("{:?}", xsdt);

    for entry in xsdt.iter() {
        let entry = entry.unwrap();
        if entry.header().signature == SDT_APIC {
            let madt = entry.data::<MADT, MADTParseError>().unwrap();
            info!("{:?}", madt);
            for record in madt.iter() {
                info!("\t{:?}", record);
            }
        } else {
            info!("{:?}", entry);
        }
    }
}

#[allow(improper_ctypes_definitions)]
#[no_mangle]
extern "C" fn _start(boot_information: BootInformation) -> ! {
    let stack_bottom = 0x10_001 + common::boot_information::PAGE_SIZE & !0xFFF;

    unsafe { BOOT_INFORMATION = Some(boot_information) };

    let stack_top = stack_bottom + 16 * 1024 * 1024;
    #[cfg(target_arch = "x86_64")]
    unsafe {
        asm!("mov rsp, {}", in(reg) stack_top);
    }

    let boot_information = unsafe { BOOT_INFORMATION.as_mut().unwrap() };

    #[cfg(target_arch = "x86_64")]
    serial::init_serial1(serial::STD_SERIAL_PORT_ADDRESS);

    drivers::EfiGop::load(boot_information.framebuffer).unwrap();
    drivers::EfiConsole::load(boot_information.font).unwrap();

    logger::init_logging().unwrap_or_else(|err| {
        use core::fmt::Write;

        #[cfg(target_arch = "x86_64")]
        SerialWriter {}
            .write_fmt(format_args!("Failed to initialize logging: {:?}\n", err))
            .unwrap();
        EfiConsoleWriter::new(0xffff0000)
            .write_fmt(format_args!("Failed to initialize logging: {:?}\n", err))
            .unwrap();
        panic!();
    });

    info!("Booting PrimitiveOS v{}.{}.{}...", 0, 0, 1);

    info!("Configuring CPU...");
    arch::cpu::configure();

    info!("Setting up page frame allocator...");
    arch::frame_allocator::configure(&boot_information.memory_descriptors);

    info!("Setting up paging...");
    arch::paging::configure(arch::frame_allocator::instance());

    info!("Initializing kernel heap...");
    allocator::initialize_heap();

    info!("Activate paging...");
    arch::paging::activate();

    info!("Activating kernel heap...");
    allocator::activate();

    print_boot_info();

    info!("Initializing ACPI tables...");
    // print_acpi_table_info(
    //     boot_information.rsdp_descriptor_v1,
    //     boot_information.rsdp_descriptor_v2,
    // );

    info!("Starting PIC system timer...");
    #[cfg(target_arch = "x86_64")]
    arch::x86_64::pic_chip::start_pic_timer();

    info!("Enabling PS/2 keyboard support...");
    #[cfg(target_arch = "x86_64")]
    arch::x86_64::pic_chip::enable_ps2_keyboard();

    info!("Enabling PS/2 mouse support...");
    #[cfg(target_arch = "x86_64")]
    arch::x86_64::pic_chip::enable_ps2_mouse();

    info!("Enabling interrupts...");
    arch::enable_interrupts();

    // breakpoint();
    // cause_page_fault();
    // stack_overflow();

    info!("Kernel initialized");

    #[cfg(test)]
    test::run_tests();

    info!("Spawning tasks executor...");
    let mut executor = Executor::new();
    #[cfg(target_arch = "x86_64")]
    executor.spawn(Task::new(task::timer::handle_timer_ticks()));
    #[cfg(target_arch = "x86_64")]
    executor.spawn(Task::new(task::mouse::handle_mouse_events()));
    #[cfg(target_arch = "x86_64")]
    executor.spawn(Task::new(task::keyboard::handle_keyboard_events()));

    executor.run();
}

#[allow(dead_code)]
fn breakpoint() {
    unsafe {
        asm! {
        "int3\n"
        }
    }
}

#[allow(dead_code)]
fn cause_page_fault() {
    let x = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    unsafe { *(0xdeadbeafff as *mut u64) = x[4] };
}

#[allow(dead_code)]
fn stack_overflow() {
    let mut i = 0_usize;
    loop {
        unsafe {
            asm! { "push {}", in(reg) i }
        }
        i += 1;
    }
}
