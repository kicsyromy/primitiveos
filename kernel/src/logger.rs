use crate::drivers::{Console, DriverError, EfiConsole};
use log::{Level, LevelFilter, Metadata, Record, SetLoggerError};

static LOGGER: PrimitiveLogger = PrimitiveLogger;

struct PrimitiveLogger;

impl log::Log for PrimitiveLogger {
    fn enabled(&self, _: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        use crate::arch::{disable_interrupts, enable_interrupts};
        #[cfg(target_arch = "x86_64")]
        use crate::serial::SerialWriter;
        use core::fmt::Write;

        disable_interrupts();

        #[cfg(target_arch = "x86_64")]
        writeln!(
            &mut SerialWriter {},
            "[ {} ] {}",
            {
                match record.level() {
                    Level::Error => "ERROR",
                    Level::Warn => " WARN",
                    Level::Info => " INFO",
                    Level::Debug => "DEBUG",
                    Level::Trace => "TRACE",
                }
            },
            record.args()
        )
        .unwrap();

        {
            match record.level() {
                Level::Debug => {
                    let mut color_fb = EfiConsoleWriter::new(0xff317ecc);
                    writeln!(&mut color_fb, "[ DEBUG ] {}", record.args()).unwrap();
                }
                Level::Warn => {
                    let mut color_fb = EfiConsoleWriter::new(0xffffff00);
                    writeln!(&mut color_fb, "[  WARN ] {}", record.args()).unwrap();
                }
                Level::Error => {
                    let mut color_fb = EfiConsoleWriter::new(0xffff0000);
                    writeln!(&mut color_fb, "[ ERROR ] {}", record.args()).unwrap();
                }
                Level::Info => {
                    let mut color_fb = EfiConsoleWriter::new(0xffffffff);
                    writeln!(&mut color_fb, "[  INFO ] {}", record.args()).unwrap();
                }
                Level::Trace => {
                    let mut color_fb = EfiConsoleWriter::new(0xff317ecc);
                    writeln!(&mut color_fb, "[ TRACE ] {}", record.args()).unwrap();
                }
            }
        }

        enable_interrupts();
    }

    fn flush(&self) {}
}

pub fn init_logging() -> Result<(), SetLoggerError> {
    log::set_logger(&LOGGER).map(|()| log::set_max_level(LevelFilter::Debug))
}

pub struct EfiConsoleWriter {
    color: u32,
}

impl EfiConsoleWriter {
    pub fn new(color: u32) -> Self {
        Self { color }
    }
}

impl core::fmt::Write for EfiConsoleWriter {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        let console = EfiConsole::console();
        if console.is_err() {
            return Err(console.err().unwrap().into());
        }

        let mut console = console.unwrap().lock();
        console.write_str_utf8(s, self.color)?;
        Ok(())
    }
}

impl From<DriverError> for core::fmt::Error {
    fn from(_: DriverError) -> Self {
        core::fmt::Error {}
    }
}
