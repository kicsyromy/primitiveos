use common::{arch::AddressVirtual, boot_information::PAGE_SIZE};
use linked_list_allocator::LockedHeap;

use crate::arch::{frame_allocator, paging::map};

const HEAP_START: usize = 0x_4444_4444_0000;
const HEAP_SIZE: usize = 16 * 1024 * 1024; // 16 MiB

#[global_allocator]
static ALLOCATOR: LockedHeap = LockedHeap::empty();

pub fn initialize_heap() {
    for page_index in 0..common::page_count!(HEAP_SIZE) {
        let frame = frame_allocator::instance().lock().request_frame();
        map(
            AddressVirtual::from(HEAP_START + (page_index * PAGE_SIZE)),
            frame.address(),
        );
    }
}

pub fn activate() {
    #[cfg(target_arch = "x86_64")]
    unsafe {
        ALLOCATOR.lock().init(HEAP_START, HEAP_SIZE);
    }
}
