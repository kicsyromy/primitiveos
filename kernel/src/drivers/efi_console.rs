use common::{
    efi_framebuffer::{EFIFramebuffer, PIXEL_SIZE_BYTES},
    libc_functions,
    psf1_font::{PSF1Font, PSF1_CHAR_HEIGHT, PSF1_CHAR_WIDTH},
};
use conquer_once::{spin::OnceCell, TryInitError};
use core::ops::DerefMut;
use spin::Mutex;

use crate::drivers::{
    console::{Console, TextCursor},
    ConsoleError, DeviceDriver, DriverError, EfiGop, EfiGopError,
};

static EFI_CONSOLE: OnceCell<Mutex<EfiConsole>> = OnceCell::uninit();

#[derive(Debug, Copy, Clone)]
pub enum EfiConsoleError {
    EfiGop(EfiGopError),
    InitializationFailed(&'static str),
    NotInitialized(&'static str),
}

impl From<EfiConsoleError> for ConsoleError {
    fn from(e: EfiConsoleError) -> Self {
        ConsoleError::Efi(e)
    }
}

impl From<EfiConsoleError> for DriverError {
    fn from(e: EfiConsoleError) -> Self {
        DriverError::Console(ConsoleError::Efi(e))
    }
}

impl From<TryInitError> for EfiConsoleError {
    fn from(e: TryInitError) -> Self {
        match e {
            TryInitError::AlreadyInit => {
                EfiConsoleError::InitializationFailed("Driver already loaded")
            }

            TryInitError::WouldBlock => EfiConsoleError::InitializationFailed(
                "Driver is in the process of being initialized",
            ),
        }
    }
}

pub struct EfiConsole {
    text_cursor: TextCursor,
    font: PSF1Font,
}

impl EfiConsole {
    pub fn load(font: PSF1Font) -> Result<(), DriverError> {
        // Try to load the framebuffer to make sure it's initialized
        let _ = EfiGop::framebuffer()?.lock();

        match EFI_CONSOLE.try_init_once(move || {
            Mutex::new(EfiConsole {
                text_cursor: TextCursor { x: 0, y: 0 },
                font,
            })
        }) {
            Err(e) => {
                let e: EfiConsoleError = e.into();
                Err(e.into())
            }
            Ok(_) => Ok(()),
        }
    }

    pub fn console() -> Result<&'static Mutex<EfiConsole>, DriverError> {
        if !EFI_CONSOLE.is_initialized() {
            return Err(EfiConsoleError::NotInitialized(
                "EFI console not initialized, was load() called?",
            )
            .into());
        }

        // SAFETY: Safe since check is performed beforehand
        Ok(unsafe { EFI_CONSOLE.get_unchecked() })
    }

    fn put_char_ascii(
        &mut self,
        fb: &mut EFIFramebuffer,
        character: u8,
        color: u32,
        x_offset: usize,
        y_offset: usize,
    ) {
        let character = match character {
            0x20..=0x7e => character,
            _ => 0,
        };

        let mut character_ptr = (self.font.glyph_buffer.as_ptr() as usize
            + (character as usize * self.font.header.char_size as usize))
            as *const u8;

        for y in y_offset..y_offset + PSF1_CHAR_HEIGHT {
            let character_pixel = unsafe { *character_ptr };
            for x in x_offset..x_offset + PSF1_CHAR_WIDTH {
                if character_pixel & 0b10000000 >> (x - x_offset) > 0 {
                    fb.put_pixel(x, y, color);
                }
            }
            character_ptr = (character_ptr as usize + 1) as *const _;
        }
    }

    fn write_str_ascii(&mut self, fb: &mut EFIFramebuffer, text: &[u8], color: u32) {
        for char in text {
            // Filter the out special characters and put printable ones on screen
            match char {
                b'\n' => {
                    self.text_cursor.x = 0;
                    self.text_cursor.y += PSF1_CHAR_HEIGHT;
                }
                b'\t' => {
                    // FIXME: This will get cut off
                    self.text_cursor.x += 4 * PSF1_CHAR_WIDTH;
                }
                _ => {
                    self.put_char_ascii(fb, *char, color, self.text_cursor.x, self.text_cursor.y);
                    self.text_cursor.x += PSF1_CHAR_WIDTH;
                }
            }

            self.update_cursor_position(fb);
            self.scroll_screen_buffer_if_necessary(fb);
        }
    }

    fn update_cursor_position(&mut self, fb: &mut EFIFramebuffer) {
        if self.text_cursor.x >= fb.width {
            self.text_cursor.x = 0;
            self.text_cursor.y += PSF1_CHAR_HEIGHT;
        }
    }

    fn scroll_screen_buffer_if_necessary(&mut self, fb: &mut EFIFramebuffer) {
        if self.text_cursor.y > fb.height - PSF1_CHAR_HEIGHT {
            let line_size_bytes = fb.pixels_per_scanline * PIXEL_SIZE_BYTES * PSF1_CHAR_HEIGHT;
            unsafe {
                libc_functions::memmove(
                    fb.base_address.as_mut_ptr(),
                    (fb.base_address + line_size_bytes).as_ptr(),
                    fb.buffer_size - line_size_bytes,
                )
            };
            unsafe {
                libc_functions::memset(
                    (fb.base_address + fb.buffer_size - line_size_bytes).as_mut_ptr(),
                    0,
                    line_size_bytes,
                )
            };

            self.text_cursor.y -= PSF1_CHAR_HEIGHT;
        }
    }
}

impl DeviceDriver for EfiConsole {
    fn id() -> &'static str {
        "EfiConsole"
    }
}

impl Console for EfiConsole {
    fn write_char_utf8(&mut self, character: char, color: u32) -> Result<(), DriverError> {
        let mut fb = EfiGop::framebuffer()?.lock();

        let mut buffer = [0_u8; 4];
        let str = character.encode_utf8(&mut buffer);
        self.write_str_ascii(fb.deref_mut(), str.as_bytes(), color);

        Ok(())
    }

    fn write_str_utf8(&mut self, string: &str, color: u32) -> Result<(), DriverError> {
        let mut fb = EfiGop::framebuffer()?.lock();
        self.write_str_ascii(fb.deref_mut(), string.as_bytes(), color);

        Ok(())
    }

    fn set_cursor_position(&mut self, position: (usize, usize)) -> Result<(), DriverError> {
        let (x, y) = position;

        self.text_cursor.x = x;
        self.text_cursor.y = y;

        Ok(())
    }
}
