#[derive(Debug, Copy, Clone)]
pub enum DriverError {
    Load(&'static str, &'static str),
    Unload(&'static str, &'static str),
    EfiGop(EfiGopError),
    Console(ConsoleError),
}

pub trait DeviceDriver {
    fn id() -> &'static str;

    unsafe fn load() -> Result<(), DriverError> {
        Ok(())
    }

    unsafe fn unload() -> Result<(), DriverError> {
        Err(DriverError::Unload(
            Self::id(),
            "Driver does not support unloading",
        ))
    }
}

mod console;
pub use console::{Console, ConsoleError};

mod efi_console;
pub use efi_console::{EfiConsole, EfiConsoleError};

mod efi_graphics_output_protocol;
pub use efi_graphics_output_protocol::{EfiGop, EfiGopError};
