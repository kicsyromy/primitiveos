use crate::drivers::{DeviceDriver, DriverError, EfiConsoleError};

#[derive(Debug, Copy, Clone)]
pub enum ConsoleError {
    IOError(&'static str),
    Efi(EfiConsoleError),
}

impl From<ConsoleError> for DriverError {
    fn from(e: ConsoleError) -> Self {
        DriverError::Console(e)
    }
}

#[derive(Debug, Copy, Clone)]
pub struct TextCursor {
    pub x: usize,
    pub y: usize,
}

pub trait Console: DeviceDriver {
    fn write_char_utf8(&mut self, character: char, color: u32) -> Result<(), DriverError>;
    fn write_str_utf8(&mut self, string: &str, color: u32) -> Result<(), DriverError>;
    fn set_cursor_position(&mut self, position: (usize, usize)) -> Result<(), DriverError>;
}
