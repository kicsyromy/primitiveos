use common::efi_framebuffer::EFIFramebuffer;
use conquer_once::{spin::OnceCell, TryInitError};
use spin::Mutex;

use crate::drivers::{DeviceDriver, DriverError};

static EFI_FRAMEBUFFER: OnceCell<Mutex<EFIFramebuffer>> = OnceCell::uninit();

#[derive(Debug, Copy, Clone)]
pub enum EfiGopError {
    FrambufferInitFailed(&'static str),
    FramebufferNotInitialized(&'static str),
}

impl From<EfiGopError> for DriverError {
    fn from(e: EfiGopError) -> Self {
        DriverError::EfiGop(e)
    }
}

impl From<TryInitError> for EfiGopError {
    fn from(e: TryInitError) -> Self {
        match e {
            TryInitError::AlreadyInit => EfiGopError::FrambufferInitFailed("Driver already loaded"),
            TryInitError::WouldBlock => {
                EfiGopError::FrambufferInitFailed("Driver is in the process of being initialized")
            }
        }
    }
}

pub struct EfiGop;

impl EfiGop {
    pub fn load(mut framebuffer: EFIFramebuffer) -> Result<(), DriverError> {
        match EFI_FRAMEBUFFER.try_init_once(move || {
            framebuffer.clear();
            Mutex::new(framebuffer)
        }) {
            Err(e) => {
                let e: EfiGopError = e.into();
                Err(e.into())
            }
            Ok(_) => Ok(()),
        }
    }

    pub fn framebuffer() -> Result<&'static Mutex<EFIFramebuffer>, DriverError> {
        if !EFI_FRAMEBUFFER.is_initialized() {
            return Err(EfiGopError::FramebufferNotInitialized(
                "Framebuffer not initialized, was load() called?",
            )
            .into());
        }

        // SAFETY: Safe since check is performed beforehand
        Ok(unsafe { EFI_FRAMEBUFFER.get_unchecked() })
    }
}

impl DeviceDriver for EfiGop {
    fn id() -> &'static str {
        "EfiGop"
    }
}
