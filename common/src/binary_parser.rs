use core::fmt::Debug;

#[derive(Debug, Copy, Clone)]
pub enum ParseError<'a, ErrorExt: Debug + Sized + Default> {
    UnexpectedValue((&'a [u8], usize), ErrorExt),
    CannotConvert((&'a [u8], usize), ErrorExt),
    NotEnoughInputData((&'a [u8], usize), ErrorExt),
}

impl<'a, ErrorExt: Debug + Sized + Default> ParseError<'a, ErrorExt> {
    pub fn internal_error(&self) -> &ErrorExt {
        match &self {
            ParseError::UnexpectedValue(_, e) => e,
            ParseError::CannotConvert(_, e) => e,
            ParseError::NotEnoughInputData(_, e) => e,
        }
    }
}

pub struct ParseContinuation<'a, Error> {
    pub slice: &'a [u8],
    pub offset: usize,
    _unused: core::marker::PhantomData<Error>,
}

impl<'a, Error: Debug + Sized + Default> ParseContinuation<'a, Error> {
    pub fn new(slice: &'a [u8]) -> ParseContinuation<'a, Error> {
        ParseContinuation {
            slice,
            offset: 0,
            _unused: core::marker::PhantomData,
        }
    }

    pub fn parse<O: Sized, F>(
        self,
        parser: F,
        error: Error,
    ) -> Result<(ParseContinuation<'a, Error>, O), ParseError<'a, Error>>
    where
        F: core::ops::FnOnce(
            Self,
            Error,
        )
            -> Result<(ParseContinuation<'a, Error>, O), ParseError<'a, Error>>,
    {
        parser(self, error)
    }

    pub fn parse_map_result<O: Sized, F, MO: Sized, M>(
        self,
        parser: F,
        error: Error,
        mapper: M,
    ) -> Result<(ParseContinuation<'a, Error>, MO), ParseError<'a, Error>>
    where
        F: core::ops::FnOnce(
            Self,
            Error,
        )
            -> Result<(ParseContinuation<'a, Error>, O), ParseError<'a, Error>>,
        M: core::ops::FnOnce(O, (&'a [u8], usize)) -> Result<MO, ParseError<'a, Error>>,
    {
        let size_of_output = core::mem::size_of::<O>();
        if self.slice.len() < size_of_output {
            return Err(ParseError::NotEnoughInputData(
                (self.slice, self.offset),
                Default::default(),
            ));
        }

        let offset = self.offset;
        let slice = &self.slice[..size_of_output];

        let (parser, result) = parser(self, error)?;
        let result = mapper(result, (slice, offset))?;

        Ok((parser, result))
    }

    pub fn skip(
        mut self,
        count: usize,
    ) -> Result<(ParseContinuation<'a, Error>, ()), ParseError<'a, Error>> {
        if self.slice.len() < count {
            return Err(ParseError::NotEnoughInputData(
                (&self.slice, self.offset),
                Default::default(),
            ));
        }

        self.offset += count;
        self.slice = &self.slice[count..];

        Ok((self, ()))
    }

    pub fn passthrough<O: Sized, F>(
        mut self,
        count: usize,
        f: F,
    ) -> Result<(ParseContinuation<'a, Error>, O), ParseError<'a, Error>>
    where
        F: core::ops::FnOnce(&'a [u8], usize) -> Result<O, ParseError<'a, Error>>,
    {
        if self.slice.len() < count {
            return Err(ParseError::NotEnoughInputData(
                (&self.slice, self.offset),
                Default::default(),
            ));
        }

        self.offset += count;

        let result = f(&self.slice[..count], self.offset)?;

        self.slice = &self.slice[count..];

        Ok((self, result))
    }
}

#[macro_export]
macro_rules! impl_parse_for_enum {
    ($type: ident, $result_type: ident, $error_type: ident) => {
        impl $type {
            pub fn parse(
                input: $crate::binary_parser::ParseContinuation<$error_type>,
                error: $error_type,
            ) -> core::result::Result<($crate::binary_parser::ParseContinuation<$error_type>, Self), $crate::binary_parser::ParseError<$error_type>> {
                use paste::paste;

                input.parse_map_result($crate::binary_parser::parsers::$result_type, error, |x, (buffer, offset) | {
                    paste! {
                        Self::[<from_ $result_type>](x).ok_or($crate::binary_parser::ParseError::CannotConvert((buffer, offset), error))
                    }
                })
            }
        }
    };
}

#[macro_export]
macro_rules! parse_tuple {
    ($slice: expr, $(($func:ident, $($args:expr),+)),+) => {{
        let mut parser = $crate::binary_parser::ParseContinuation::new($slice);
        let values = (
            $({
                let (p, value) = parser.$func($($args),*)?;
                parser = p;
                value
            }
        ),*);
        Ok((parser, values))
    }};
}

pub mod parsers {
    use core::{fmt::Debug, result::Result};

    use super::ParseContinuation;
    use super::ParseError;

    fn read_value<T: Sized>(input: &[u8]) -> T {
        let size_of_t: usize = core::mem::size_of::<T>();

        let mut value: T = unsafe { core::mem::zeroed() };
        unsafe {
            core::ptr::copy_nonoverlapping(
                input.as_ptr(),
                &mut value as *mut T as *mut u8,
                size_of_t,
            )
        }

        value
    }

    pub fn integer<Int: Sized, Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, Int), ParseError<Error>> {
        let size_of_output = core::mem::size_of::<Int>();
        if input.slice.len() < size_of_output {
            return Err(ParseError::NotEnoughInputData(
                (input.slice, input.offset),
                error,
            ));
        }

        Ok((
            ParseContinuation::<Error> {
                slice: &input.slice[size_of_output..],
                offset: input.offset + size_of_output,
                _unused: core::marker::PhantomData,
            },
            read_value(input.slice),
        ))
    }

    pub fn u8<Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, u8), ParseError<Error>> {
        integer(input, error)
    }

    pub fn i8<Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, i8), ParseError<Error>> {
        integer(input, error)
    }

    pub fn u16<Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, u16), ParseError<Error>> {
        integer(input, error)
    }

    pub fn i16<Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, i16), ParseError<Error>> {
        integer(input, error)
    }

    pub fn u32<Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, u32), ParseError<Error>> {
        integer(input, error)
    }

    pub fn i32<Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, i32), ParseError<Error>> {
        integer(input, error)
    }

    pub fn u64<Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, u64), ParseError<Error>> {
        integer(input, error)
    }

    pub fn i64<Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, i64), ParseError<Error>> {
        integer(input, error)
    }

    pub fn address<Error: Debug + Sized + Default>(
        input: ParseContinuation<Error>,
        error: Error,
    ) -> Result<(ParseContinuation<Error>, crate::arch::Address), ParseError<Error>> {
        #[cfg(target_pointer_width = "64")]
        let (parser, result) = integer::<u64, Error>(input, error)?;

        #[cfg(target_pointer_width = "32")]
        let (parser, result) = integer::<u32, Error>(input, error)?;

        Ok((parser, crate::arch::Address::from(result)))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn read_single_value() -> Result<(), ParseError<'static, ()>> {
        const ORIGINAL_VALUE: u32 = 23;
        static mut BUFFER: [u8; 4] = [0_u8; 4];
        let buffer = unsafe { &mut BUFFER };

        unsafe {
            core::ptr::copy_nonoverlapping(
                &ORIGINAL_VALUE as *const _ as *const u8,
                BUFFER.as_mut_ptr(),
                core::mem::size_of::<u32>(),
            )
        }

        let parser = ParseContinuation::<()>::new(buffer);
        let (_, read_value) = parser.parse(parsers::u32, Default::default())?;
        assert_eq!(read_value, ORIGINAL_VALUE);

        Ok(())
    }

    #[test]
    fn read_multiple_values() -> Result<(), ParseError<'static, ()>> {
        const U8: u8 = 0xFF;
        const U16: u16 = 0xFFFF;
        const U32: u32 = 0xFFFFFFFF;
        const U64: u64 = 0xFFFFFFFFFFFFFFFF;

        static mut BUFFER: [u8; 15] = [0_u8; 15];
        let buffer = unsafe { &mut BUFFER };

        unsafe {
            core::ptr::copy_nonoverlapping(
                &U8 as *const _ as *const u8,
                buffer.as_mut_ptr(),
                core::mem::size_of::<u8>(),
            );

            core::ptr::copy_nonoverlapping(
                &U16 as *const _ as *const u8,
                buffer[1..].as_mut_ptr(),
                core::mem::size_of::<u16>(),
            );

            core::ptr::copy_nonoverlapping(
                &U32 as *const _ as *const u8,
                buffer[3..].as_mut_ptr(),
                core::mem::size_of::<u32>(),
            );

            core::ptr::copy_nonoverlapping(
                &U64 as *const _ as *const u8,
                buffer[7..].as_mut_ptr(),
                core::mem::size_of::<u64>(),
            );
        }

        let parser = ParseContinuation::<()>::new(buffer);
        let (parser, read_u8) = parser.parse(parsers::u8, Default::default())?;
        assert_eq!(read_u8, U8);
        let (parser, read_u16) = parser.parse(parsers::u16, Default::default())?;
        assert_eq!(read_u16, U16);
        let (parser, read_u32) = parser.parse(parsers::u32, Default::default())?;
        assert_eq!(read_u32, U32);
        let (_, read_u64) = parser.parse(parsers::u64, Default::default())?;
        assert_eq!(read_u64, U64);

        Ok(())
    }

    #[test]
    fn map_result() -> Result<(), ParseError<'static, ()>> {
        const ORIGINAL_VALUE: u32 = 69;
        static mut BUFFER: [u8; 4] = [0_u8; 4];
        let buffer = unsafe { &mut BUFFER };

        unsafe {
            core::ptr::copy_nonoverlapping(
                &ORIGINAL_VALUE as *const _ as *const u8,
                buffer.as_mut_ptr(),
                core::mem::size_of::<u32>(),
            )
        }

        let (_, mapped_value) = parse_tuple!(
            buffer,
            (
                parse_map_result,
                parsers::u32,
                Default::default(),
                |x, _| Ok(x as u8)
            )
        )?;
        assert_eq!(mapped_value, ORIGINAL_VALUE as u8);

        Ok(())
    }

    #[test]
    fn parse_tuple() -> Result<(), ParseError<'static, ()>> {
        const U8: u8 = 0xFF;
        const U16: u16 = 0xFFFF;
        const U32: u32 = 0xFFFFFFFF;
        const U64: u64 = 0xFFFFFFFFFFFFFFFF;

        static mut BUFFER: [u8; 15] = [0_u8; 15];
        let buffer = unsafe { &mut BUFFER };

        unsafe {
            core::ptr::copy_nonoverlapping(
                &U8 as *const _ as *const u8,
                buffer.as_mut_ptr(),
                core::mem::size_of::<u8>(),
            );

            core::ptr::copy_nonoverlapping(
                &U16 as *const _ as *const u8,
                buffer[1..].as_mut_ptr(),
                core::mem::size_of::<u16>(),
            );

            core::ptr::copy_nonoverlapping(
                &U32 as *const _ as *const u8,
                buffer[3..].as_mut_ptr(),
                core::mem::size_of::<u32>(),
            );

            core::ptr::copy_nonoverlapping(
                &U64 as *const _ as *const u8,
                buffer[7..].as_mut_ptr(),
                core::mem::size_of::<u64>(),
            );
        }

        let (_, (read_u8, read_u16, read_u32, read_u64)) = parse_tuple!(
            buffer,
            (parse, parsers::u8, Default::default()),
            (parse, parsers::u16, Default::default()),
            (parse, parsers::u32, Default::default()),
            (parse, parsers::u64, Default::default())
        )?;

        assert_eq!(read_u8, U8);
        assert_eq!(read_u16, U16);
        assert_eq!(read_u32, U32);
        assert_eq!(read_u64, U64);

        Ok(())
    }

    #[test]
    fn skip() -> Result<(), ParseError<'static, ()>> {
        const U8: u8 = 0xFF;
        const U16: u16 = 0xFFFF;

        static mut BUFFER: [u8; 3] = [0_u8; 3];
        let buffer = unsafe { &mut BUFFER };

        unsafe {
            core::ptr::copy_nonoverlapping(
                &U8 as *const _ as *const u8,
                buffer.as_mut_ptr(),
                core::mem::size_of::<u8>(),
            );

            core::ptr::copy_nonoverlapping(
                &U16 as *const _ as *const u8,
                buffer[1..].as_mut_ptr(),
                core::mem::size_of::<u16>(),
            );
        }

        let (_, (_, read_u16)) = parse_tuple!(
            buffer,
            (skip, 1_usize),
            (parse, parsers::u16, Default::default())
        )?;

        assert_eq!(read_u16, U16);

        Ok(())
    }

    #[test]
    fn passthrough() -> Result<(), ParseError<'static, ()>> {
        static mut BUFFER: [u8; 4] = [b'0', b'1', b'2', b'3'];
        let buffer = unsafe { &mut BUFFER };

        let (_, _) = parse_tuple!(
            buffer,
            (passthrough, buffer.len(), |x, offset| {
                assert_eq!(offset, buffer.len());
                assert_eq!(x, buffer);

                Ok(())
            })
        )?;

        Ok(())
    }
}
