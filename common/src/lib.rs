#![no_std]
#![feature(asm)]
#![feature(global_asm)]

#[macro_export]
macro_rules! page_count {
    ($byte_count: expr) => {{
        let byte_count = ($byte_count) as usize;
        (byte_count + $crate::boot_information::PAGE_SIZE - 1) / $crate::boot_information::PAGE_SIZE
    }};
}

#[macro_export]
macro_rules! byte_count {
    ($page_count: expr) => {{
        let page_count = ($page_count) as usize;
        page_count * $crate::boot_information::PAGE_SIZE
    }};
}

pub mod acpi;
pub mod arch;
pub mod binary_parser;
pub mod boot_information;
pub mod efi_framebuffer;
pub mod efi_memory_descriptor;
pub mod elf;
pub mod libc_functions;
pub mod psf1_font;
