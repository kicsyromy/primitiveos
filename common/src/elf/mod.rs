use dynamic_entry::*;
use header::*;
use program_header::*;

use crate::arch::Address;
use crate::binary_parser;

#[cfg(test)]
mod test_elf_binaries;

mod dynamic_entry;
mod header;
mod loader;
mod program_header;

type ParseResult<'a, O, E> = core::result::Result<(ParseContinuation<'a, E>, O), ParseError<'a, E>>;
type ParseContinuation<'a, E> = binary_parser::ParseContinuation<'a, E>;

const MAX_DYNAMIC_ENTRIES: usize = 32;
const MAX_PH_COUNT: usize = 10;

pub type ParseError<'a, E> = binary_parser::ParseError<'a, E>;

pub type SegmentType = program_header::SegmentType;
pub type RelType = dynamic_entry::RelType;
pub type Loader<'a, 'e> = loader::Loader<'a, 'e>;
pub type LoaderSegment<'a, 'e> = loader::Segment<'a, 'e>;

#[derive(Debug)]
pub struct Elf<'a> {
    pub header: Header<'a>,
    pub program_headers: [Option<ProgramHeader<'a>>; MAX_PH_COUNT],
}

impl<'a> Elf<'a> {
    pub fn parse(buffer: &'a [u8]) -> core::result::Result<Self, ParseError<'a, ElfParseError>> {
        let (_, header) = Header::<'a>::parse(buffer)?;
        let mut this = Self {
            header,
            program_headers: [None; MAX_PH_COUNT],
        };
        this.parse_program_headers()?;

        Ok(this)
    }

    pub fn segment_at(&self, address: Address) -> Option<&ProgramHeader> {
        self.program_headers
            .iter()
            .filter(|ph| ph.is_some() && ph.as_ref().unwrap().r#type == SegmentType::Load)
            .find(|ph| ph.as_ref().unwrap().memory_range().contains(&address))
            .map(|ph| ph.as_ref().unwrap())
    }

    pub fn segment_of_type(&self, r#type: SegmentType) -> Option<&ProgramHeader> {
        self.program_headers
            .iter()
            .find(|ph| ph.is_some() && ph.as_ref().unwrap().r#type == r#type)
            .map(|ph| ph.as_ref().unwrap())
    }

    pub fn dynamic_entry(&self, tag: DynamicTag) -> Option<Address> {
        match self.segment_of_type(SegmentType::Dynamic) {
            Some(ProgramHeader {
                contents: SegmentContents::Dynamic(entries),
                ..
            }) => entries
                .iter()
                .find(|e| e.is_some() && e.as_ref().unwrap().tag == tag)
                .map(|e| e.as_ref().unwrap().address),
            _ => None,
        }
    }

    pub fn rela_entries_iter(&self) -> Option<RelaEntriesIterator> {
        let address = self.dynamic_entry(DynamicTag::Rela)?;
        let len = self.dynamic_entry(DynamicTag::RelaSz)?;
        let seg = self.segment_at(address)?;

        let buffer = &seg.data[(address - seg.memory_range().start).into()..][..len.into()];
        Some(RelaEntriesIterator::new(buffer))
    }

    fn parse_program_headers(&mut self) -> Result<(), ParseError<'a, ProgramHeaderParseError>> {
        let mut program_headers: [Option<ProgramHeader<'a>>; MAX_PH_COUNT] =
            self.header.program_headers()?;

        program_headers.sort_unstable_by(|a, b| {
            if a.is_none() {
                return core::cmp::Ordering::Greater;
            }

            if b.is_none() {
                return core::cmp::Ordering::Less;
            }

            let a = a.as_ref().unwrap();
            let b = b.as_ref().unwrap();

            (a.physical_address.as_usize() + a.memory_size.as_usize())
                .cmp(&(b.physical_address.as_usize() + b.memory_size.as_usize()))
        });
        self.program_headers = program_headers;

        Ok(())
    }
}

#[derive(Debug, Copy, Clone)]
pub enum ElfParseError {
    Header(HeaderParseError),
    ProgramHeader(ProgramHeaderParseError),
    Unknown,
}

impl From<HeaderParseError> for ElfParseError {
    fn from(e: HeaderParseError) -> Self {
        ElfParseError::Header(e)
    }
}

impl From<ProgramHeaderParseError> for ElfParseError {
    fn from(e: ProgramHeaderParseError) -> Self {
        ElfParseError::ProgramHeader(e)
    }
}

impl<'a> From<ParseError<'a, HeaderParseError>> for ParseError<'a, ElfParseError> {
    fn from(e: ParseError<'a, HeaderParseError>) -> Self {
        match e {
            ParseError::UnexpectedValue(pos, e) => {
                ParseError::UnexpectedValue(pos, ElfParseError::Header(e))
            }
            ParseError::CannotConvert(pos, e) => {
                ParseError::CannotConvert(pos, ElfParseError::Header(e))
            }
            ParseError::NotEnoughInputData(pos, e) => {
                ParseError::NotEnoughInputData(pos, ElfParseError::Header(e))
            }
        }
    }
}

impl<'a> From<ParseError<'a, ProgramHeaderParseError>> for ParseError<'a, ElfParseError> {
    fn from(e: ParseError<'a, ProgramHeaderParseError>) -> Self {
        match e {
            ParseError::UnexpectedValue(pos, e) => {
                ParseError::UnexpectedValue(pos, ElfParseError::ProgramHeader(e))
            }
            ParseError::CannotConvert(pos, e) => {
                ParseError::CannotConvert(pos, ElfParseError::ProgramHeader(e))
            }
            ParseError::NotEnoughInputData(pos, e) => {
                ParseError::NotEnoughInputData(pos, ElfParseError::ProgramHeader(e))
            }
        }
    }
}

impl Default for ElfParseError {
    fn default() -> Self {
        ElfParseError::Unknown
    }
}

pub struct RelaEntriesIterator<'a> {
    buffer: &'a [u8],
    offset: usize,
}

impl<'a> RelaEntriesIterator<'a> {
    pub fn new(buffer: &'a [u8]) -> Self {
        Self { buffer, offset: 0 }
    }
}

impl<'a> core::iter::Iterator for RelaEntriesIterator<'a> {
    type Item = RelaEntry;

    fn next(&mut self) -> Option<Self::Item> {
        if self.offset >= self.buffer.len() {
            return None;
        }

        let result =
            if let Ok(rela) = RelaEntry::parse(&self.buffer[self.offset..][..RelaEntry::size()]) {
                Some(rela.1)
            } else {
                None
            };

        self.offset += RelaEntry::size();

        result
    }
}

#[cfg(test)]
mod test {
    use self::super::{Elf, ElfParseError};

    const ELF_64_FILE_CONTENT: &[u8] = &super::test_elf_binaries::TEST_ELF64_EXECUTABLE;

    #[test]
    fn parse() -> Result<(), ElfParseError> {
        let _elf_file = Elf::parse(ELF_64_FILE_CONTENT).map_err(|e| e.internal_error().clone())?;

        Ok(())
    }
}
