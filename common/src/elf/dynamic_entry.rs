use crate::{arch::Address, impl_parse_for_enum, parse_tuple};

use super::ParseResult;

const DYNAMIC_TAG_NULL: u64 = 0;
const DYNAMIC_TAG_NEEDED: u64 = 1;
const DYNAMIC_TAG_PLT_REL_SZ: u64 = 2;
const DYNAMIC_TAG_PLT_GOT: u64 = 3;
const DYNAMIC_TAG_HASH: u64 = 4;
const DYNAMIC_TAG_STR_TAB: u64 = 5;
const DYNAMIC_TAG_SYM_TAB: u64 = 6;
const DYNAMIC_TAG_RELA: u64 = 7;
const DYNAMIC_TAG_RELA_SZ: u64 = 8;
const DYNAMIC_TAG_RELA_ENT: u64 = 9;
const DYNAMIC_TAG_STR_SZ: u64 = 10;
const DYNAMIC_TAG_SYM_ENT: u64 = 11;
const DYNAMIC_TAG_INIT: u64 = 12;
const DYNAMIC_TAG_FINI: u64 = 13;
const DYNAMIC_TAG_SO_NAME: u64 = 14;
const DYNAMIC_TAG_R_PATH: u64 = 15;
const DYNAMIC_TAG_SYMBOLIC: u64 = 16;
const DYNAMIC_TAG_REL: u64 = 17;
const DYNAMIC_TAG_REL_SZ: u64 = 18;
const DYNAMIC_TAG_REL_ENT: u64 = 19;
const DYNAMIC_TAG_PLT_REL: u64 = 20;
const DYNAMIC_TAG_DEBUG: u64 = 21;
const DYNAMIC_TAG_TEXT_REL: u64 = 22;
const DYNAMIC_TAG_JMP_REL: u64 = 23;
const DYNAMIC_TAG_BIND_NOW: u64 = 24;
const DYNAMIC_TAG_INIT_ARRAY: u64 = 25;
const DYNAMIC_TAG_FINI_ARRAY: u64 = 26;
const DYNAMIC_TAG_INIT_ARRAY_SZ: u64 = 27;
const DYNAMIC_TAG_FINI_ARRAY_SZ: u64 = 28;
const DYNAMIC_TAG_LO_OS: u64 = 0x60000000;
const DYNAMIC_TAG_HI_OS: u64 = 0x6FFFFfff;
const DYNAMIC_TAG_LO_PROC: u64 = 0x70000000;
const DYNAMIC_TAG_HI_PROC: u64 = 0x7FFfffff;
const DYNAMIC_TAG_GNU_HASH: u64 = 0x6Ffffef5;
const DYNAMIC_TAG_FLAGS1: u64 = 0x6FFffffb;
const DYNAMIC_TAG_RELA_COUNT: u64 = 0x6ffffff9;

pub const DYNAMIC_ENTRY_SIZE_BYTES: usize = 16;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u64)]
pub enum DynamicTag {
    Null = DYNAMIC_TAG_NULL,
    Needed = DYNAMIC_TAG_NEEDED,
    PltRelSz = DYNAMIC_TAG_PLT_REL_SZ,
    PltGot = DYNAMIC_TAG_PLT_GOT,
    Hash = DYNAMIC_TAG_HASH,
    StrTab = DYNAMIC_TAG_STR_TAB,
    SymTab = DYNAMIC_TAG_SYM_TAB,
    Rela = DYNAMIC_TAG_RELA,
    RelaSz = DYNAMIC_TAG_RELA_SZ,
    RelaEnt = DYNAMIC_TAG_RELA_ENT,
    StrSz = DYNAMIC_TAG_STR_SZ,
    SymEnt = DYNAMIC_TAG_SYM_ENT,
    Init = DYNAMIC_TAG_INIT,
    Fini = DYNAMIC_TAG_FINI,
    SoName = DYNAMIC_TAG_SO_NAME,
    RPath = DYNAMIC_TAG_R_PATH,
    Symbolic = DYNAMIC_TAG_SYMBOLIC,
    Rel = DYNAMIC_TAG_REL,
    RelSz = DYNAMIC_TAG_REL_SZ,
    RelEnt = DYNAMIC_TAG_REL_ENT,
    PltRel = DYNAMIC_TAG_PLT_REL,
    Debug = DYNAMIC_TAG_DEBUG,
    TextRel = DYNAMIC_TAG_TEXT_REL,
    JmpRel = DYNAMIC_TAG_JMP_REL,
    BindNow = DYNAMIC_TAG_BIND_NOW,
    InitArray = DYNAMIC_TAG_INIT_ARRAY,
    FiniArray = DYNAMIC_TAG_FINI_ARRAY,
    InitArraySz = DYNAMIC_TAG_INIT_ARRAY_SZ,
    FiniArraySz = DYNAMIC_TAG_FINI_ARRAY_SZ,
    LoOs = DYNAMIC_TAG_LO_OS,
    HiOs = DYNAMIC_TAG_HI_OS,
    LoProc = DYNAMIC_TAG_LO_PROC,
    HiProc = DYNAMIC_TAG_HI_PROC,
    GnuHash = DYNAMIC_TAG_GNU_HASH,
    Flags1 = DYNAMIC_TAG_FLAGS1,
    RelaCount = DYNAMIC_TAG_RELA_COUNT,
}

impl_parse_for_enum!(DynamicTag, u64, DynamicEntryParseError);

impl DynamicTag {
    pub fn from_u64(value: u64) -> Option<Self> {
        use DynamicTag::*;

        match value {
            DYNAMIC_TAG_NULL => Some(Null),
            DYNAMIC_TAG_NEEDED => Some(Needed),
            DYNAMIC_TAG_PLT_REL_SZ => Some(PltRelSz),
            DYNAMIC_TAG_PLT_GOT => Some(PltGot),
            DYNAMIC_TAG_HASH => Some(Hash),
            DYNAMIC_TAG_STR_TAB => Some(StrTab),
            DYNAMIC_TAG_SYM_TAB => Some(SymTab),
            DYNAMIC_TAG_RELA => Some(Rela),
            DYNAMIC_TAG_RELA_SZ => Some(RelaSz),
            DYNAMIC_TAG_RELA_ENT => Some(RelaEnt),
            DYNAMIC_TAG_STR_SZ => Some(StrSz),
            DYNAMIC_TAG_SYM_ENT => Some(SymEnt),
            DYNAMIC_TAG_INIT => Some(Init),
            DYNAMIC_TAG_FINI => Some(Fini),
            DYNAMIC_TAG_SO_NAME => Some(SoName),
            DYNAMIC_TAG_R_PATH => Some(RPath),
            DYNAMIC_TAG_SYMBOLIC => Some(Symbolic),
            DYNAMIC_TAG_REL => Some(Rel),
            DYNAMIC_TAG_REL_SZ => Some(RelSz),
            DYNAMIC_TAG_REL_ENT => Some(RelEnt),
            DYNAMIC_TAG_PLT_REL => Some(PltRel),
            DYNAMIC_TAG_DEBUG => Some(Debug),
            DYNAMIC_TAG_TEXT_REL => Some(TextRel),
            DYNAMIC_TAG_JMP_REL => Some(JmpRel),
            DYNAMIC_TAG_BIND_NOW => Some(BindNow),
            DYNAMIC_TAG_INIT_ARRAY => Some(InitArray),
            DYNAMIC_TAG_FINI_ARRAY => Some(FiniArray),
            DYNAMIC_TAG_INIT_ARRAY_SZ => Some(InitArraySz),
            DYNAMIC_TAG_FINI_ARRAY_SZ => Some(FiniArraySz),
            DYNAMIC_TAG_LO_OS => Some(LoOs),
            DYNAMIC_TAG_HI_OS => Some(HiOs),
            DYNAMIC_TAG_LO_PROC => Some(LoProc),
            DYNAMIC_TAG_HI_PROC => Some(HiProc),
            DYNAMIC_TAG_GNU_HASH => Some(GnuHash),
            DYNAMIC_TAG_FLAGS1 => Some(Flags1),
            DYNAMIC_TAG_RELA_COUNT => Some(RelaCount),
            _ => None,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct DynamicEntry {
    pub tag: DynamicTag,
    pub address: Address,
}

impl DynamicEntry {
    pub fn parse(buffer: &[u8]) -> ParseResult<Self, DynamicEntryParseError> {
        use super::binary_parser::parsers;

        let (parse_continuation, (tag, address)) = parse_tuple!(
            buffer,
            (parse, DynamicTag::parse, DynamicEntryParseError::DynamicTag),
            (parse, parsers::address, DynamicEntryParseError::Address)
        )?;

        Ok((parse_continuation, Self { tag, address }))
    }
}

const REL_TYPE_X86_64_GLOB_DAT: u32 = 6;
const REL_TYPE_X86_64_JUMP_SLOT: u32 = 7;
const REL_TYPE_X86_64_RELATIVE: u32 = 8;
const REL_TYPE_AARCH64_GLOB_DAT: u32 = 1025;
const REL_TYPE_AARCH64_JUMP_SLOT: u32 = 1026;
const REL_TYPE_AARCH64_RELATIVE: u32 = 1027;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u32)]
#[allow(non_camel_case_types)]
pub enum RelType {
    X86_64_GlobDat = REL_TYPE_X86_64_GLOB_DAT,
    X86_64_JumpSlot = REL_TYPE_X86_64_JUMP_SLOT,
    X86_64_Relative = REL_TYPE_X86_64_RELATIVE,
    Aarch64_GlobDat = REL_TYPE_AARCH64_GLOB_DAT,
    Aarch64_JumpSlot = REL_TYPE_AARCH64_JUMP_SLOT,
    Aarch64_Relative = REL_TYPE_AARCH64_RELATIVE,
}

impl_parse_for_enum!(RelType, u32, RelaEntryParseError);

impl RelType {
    pub fn from_u32(value: u32) -> Option<Self> {
        match value {
            REL_TYPE_X86_64_GLOB_DAT => Some(RelType::X86_64_GlobDat),
            REL_TYPE_X86_64_JUMP_SLOT => Some(RelType::X86_64_JumpSlot),
            REL_TYPE_X86_64_RELATIVE => Some(RelType::X86_64_Relative),
            REL_TYPE_AARCH64_GLOB_DAT => Some(RelType::Aarch64_GlobDat),
            REL_TYPE_AARCH64_JUMP_SLOT => Some(RelType::Aarch64_JumpSlot),
            REL_TYPE_AARCH64_RELATIVE => Some(RelType::Aarch64_Relative),
            _ => None,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct RelaEntry {
    pub offset: Address,
    pub r#type: RelType,
    pub sym: u32,
    pub addend: Address,
}

impl RelaEntry {
    pub fn parse(buffer: &[u8]) -> ParseResult<Self, RelaEntryParseError> {
        use super::binary_parser::parsers;

        let (parse_continuation, (offset, r#type, sym, addend)) = parse_tuple!(
            buffer,
            (parse, parsers::address, RelaEntryParseError::Address),
            (parse, RelType::parse, RelaEntryParseError::RelType),
            (parse, parsers::u32, RelaEntryParseError::Sym),
            (parse, parsers::address, RelaEntryParseError::Addend)
        )?;

        Ok((
            parse_continuation,
            Self {
                offset,
                r#type,
                sym,
                addend,
            },
        ))
    }

    #[inline]
    pub const fn size() -> usize {
        const RELA_ENTRY_SIZE: usize = 24;
        RELA_ENTRY_SIZE
    }
}

#[derive(Debug, Copy, Clone)]
pub enum DynamicEntryParseError {
    DynamicTag,
    Address,
    Unknown,
}

impl Default for DynamicEntryParseError {
    fn default() -> Self {
        DynamicEntryParseError::Unknown
    }
}

#[derive(Debug, Copy, Clone)]
pub enum RelaEntryParseError {
    Address,
    RelType,
    Sym,
    Addend,
    Unknown,
}

impl Default for RelaEntryParseError {
    fn default() -> Self {
        RelaEntryParseError::Unknown
    }
}

#[cfg(test)]
mod test {
    use crate::elf::{header::Header, program_header::ProgramHeaderParseError};

    const ELF_64_FILE_CONTENT: &[u8] = &super::super::test_elf_binaries::TEST_ELF64_EXECUTABLE;

    #[test]
    fn parse() -> Result<(), ProgramHeaderParseError> {
        let (_, header) =
            Header::parse(ELF_64_FILE_CONTENT).expect("Failed to parse ELF header content");
        let _program_headers = header
            .program_headers()
            .expect("Failed to parse ELF program headers");

        Ok(())
    }
}
