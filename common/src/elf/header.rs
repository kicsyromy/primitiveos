use crate::{arch::Address, impl_parse_for_enum, parse_tuple};

use super::{
    program_header::{ProgramHeader, ProgramHeaderParseError},
    ParseResult,
};

pub const MAGIC: [u8; 4] = [0x7f, 0x45, 0x4c, 0x46];

const CLASS_32_BIT: u8 = 0x1;
const CLASS_64_BIT: u8 = 0x2;

const ENDIANNESS_LITTLE: u8 = 0x1;
const ENDIANNESS_BIG: u8 = 0x2;

const VERSION: u8 = 0x1;

const OS_ABI_0: u8 = 0x0;
const OS_ABI_3: u8 = 0x3;

const TYPE_NONE: u16 = 0x0;
const TYPE_REL: u16 = 0x1;
const TYPE_EXEC: u16 = 0x2;
const TYPE_DYN: u16 = 0x3;
const TYPE_CORE: u16 = 0x4;

const MACHINE_X86: u16 = 0x3;
const MACHINE_X86_64: u16 = 0x3e;
const MACHINE_ARM: u16 = 0x28;
const MACHINE_AARCH64: u16 = 0xb7;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Class {
    ELF32 = CLASS_32_BIT,
    ELF64 = CLASS_64_BIT,
}

impl_parse_for_enum!(Class, u8, HeaderParseError);

impl Class {
    pub fn from_u8(value: u8) -> Option<Self> {
        match value {
            CLASS_32_BIT => Some(Class::ELF32),
            CLASS_64_BIT => Some(Class::ELF64),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Endianness {
    Little = ENDIANNESS_LITTLE,
    Big = ENDIANNESS_BIG,
}

impl_parse_for_enum!(Endianness, u8, HeaderParseError);

impl Endianness {
    pub fn from_u8(value: u8) -> Option<Self> {
        match value {
            ENDIANNESS_LITTLE => Some(Endianness::Little),
            ENDIANNESS_BIG => Some(Endianness::Big),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u16)]
pub enum Type {
    None = TYPE_NONE,
    Rel = TYPE_REL,
    Exec = TYPE_EXEC,
    Dyn = TYPE_DYN,
    Core = TYPE_CORE,
}

impl_parse_for_enum!(Type, u16, HeaderParseError);

impl Type {
    pub fn from_u16(value: u16) -> Option<Self> {
        match value {
            TYPE_NONE => Some(Type::None),
            TYPE_REL => Some(Type::Rel),
            TYPE_EXEC => Some(Type::Exec),
            TYPE_DYN => Some(Type::Dyn),
            TYPE_CORE => Some(Type::Core),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u16)]
pub enum Machine {
    X86 = MACHINE_X86,
    X86_64 = MACHINE_X86_64,
    Arm = MACHINE_ARM,
    Aarch64 = MACHINE_AARCH64,
}

impl_parse_for_enum!(Machine, u16, HeaderParseError);

impl Machine {
    pub fn from_u16(value: u16) -> Option<Self> {
        match value {
            MACHINE_X86 => Some(Machine::X86),
            MACHINE_X86_64 => Some(Machine::X86_64),
            MACHINE_ARM => Some(Machine::Arm),
            MACHINE_AARCH64 => Some(Machine::Aarch64),
            _ => None,
        }
    }
}

#[derive(Copy, Clone)]
pub struct Header<'a> {
    pub class: Class,
    pub endianness: Endianness,
    pub elf_version: u8,
    pub os_abi: u8,
    pub r#type: Type,
    pub machine: Machine,
    pub version: u32,
    pub entry_point: Address,
    pub ph_offset: Address,
    pub sh_offset: Address,
    pub flags: u32,
    pub header_size: usize,
    pub ph_entry_size: usize,
    pub ph_entry_count: usize,
    pub sh_entry_size: usize,
    pub sh_entry_count: usize,
    pub sh_entry_names_index: usize,
    buffer: &'a [u8],
}

impl<'a> Header<'a> {
    pub fn parse(buffer: &'a [u8]) -> ParseResult<Self, HeaderParseError> {
        use super::binary_parser::{parsers, ParseError};

        let (
            parse_continuation,
            (
                _magic,
                class,
                endianness,
                elf_version,
                os_abi,
                _padding,
                r#type,
                machine,
                version,
                entry_point,
                ph_offset,
                sh_offset,
                flags,
                header_size,
                ph_entry_size,
                ph_entry_count,
                sh_entry_size,
                sh_entry_count,
                sh_entry_names_index,
            ),
        ) = parse_tuple!(
            buffer,
            (passthrough, 4_usize, |x, offset| if x != MAGIC {
                Err(ParseError::UnexpectedValue(
                    (x, offset),
                    HeaderParseError::Magic,
                ))
            } else {
                Ok(())
            }),
            (parse, Class::parse, HeaderParseError::Class),
            (parse, Endianness::parse, HeaderParseError::Endianness),
            // ELF Version
            (
                parse_map_result,
                parsers::u8,
                HeaderParseError::ELFVersion,
                |x, (buffer, offset)| {
                    if x != VERSION {
                        Err(ParseError::UnexpectedValue(
                            (buffer, offset),
                            HeaderParseError::ELFVersion,
                        ))
                    } else {
                        Ok(x)
                    }
                }
            ),
            // OS ABI
            (
                parse_map_result,
                parsers::u8,
                HeaderParseError::ABI,
                |x, (buffer, offset)| {
                    if x != OS_ABI_0 && x != OS_ABI_3 {
                        Err(ParseError::UnexpectedValue(
                            (buffer, offset),
                            HeaderParseError::ABI,
                        ))
                    } else {
                        Ok(x)
                    }
                }
            ),
            (skip, 8_usize),
            (parse, Type::parse, HeaderParseError::Type),
            (parse, Machine::parse, HeaderParseError::Machine),
            // Version
            (
                parse_map_result,
                parsers::u32,
                HeaderParseError::Version,
                |x, (buffer, offset)| {
                    if x != VERSION as u32 {
                        Err(ParseError::UnexpectedValue(
                            (buffer, offset),
                            HeaderParseError::Version,
                        ))
                    } else {
                        Ok(x)
                    }
                }
            ),
            // Entry Point
            (parse, parsers::address, HeaderParseError::EntryPoint),
            // Program Header Offset
            (parse, parsers::address, HeaderParseError::ProgramHeader),
            // Section Header Offset
            (parse, parsers::address, HeaderParseError::SectionHeader),
            // CPU Specific Flags
            (parse, parsers::u32, HeaderParseError::Flags),
            // Header Size
            (parse, parsers::u16, HeaderParseError::HeaderSize),
            // Program Header Entry Size
            (parse, parsers::u16, HeaderParseError::ProgramHeaderSize),
            // Program Header Entry Count
            (parse, parsers::u16, HeaderParseError::ProgramHeaderCount),
            // Section Header Entry Size
            (parse, parsers::u16, HeaderParseError::SectionHeaderSize),
            // Section Header Entry Count
            (parse, parsers::u16, HeaderParseError::SectionHeaderCount),
            // Index of Entry with Section Names
            (parse, parsers::u16, HeaderParseError::SectionNamesIndex)
        )?;

        Ok((
            parse_continuation,
            Self {
                class,
                endianness,
                elf_version,
                os_abi,
                r#type,
                machine,
                version,
                entry_point,
                ph_offset,
                sh_offset,
                flags,
                header_size: header_size as usize,
                ph_entry_size: ph_entry_size as usize,
                ph_entry_count: ph_entry_count as usize,
                sh_entry_size: sh_entry_size as usize,
                sh_entry_count: sh_entry_count as usize,
                sh_entry_names_index: sh_entry_names_index as usize,
                buffer,
            },
        ))
    }

    pub fn program_headers(
        &self,
    ) -> core::result::Result<
        [Option<ProgramHeader<'a>>; super::MAX_PH_COUNT],
        crate::binary_parser::ParseError<'a, ProgramHeaderParseError>,
    > {
        use super::MAX_PH_COUNT;

        assert!(self.ph_entry_count <= MAX_PH_COUNT);
        let mut program_headers: [Option<ProgramHeader>; MAX_PH_COUNT] = [None; MAX_PH_COUNT];

        let ph_slices = (self.buffer[self.ph_offset.into()..]).chunks(self.ph_entry_size);
        for (i, ph_slice) in ph_slices.take(self.ph_entry_count).enumerate() {
            let (_, ph) = ProgramHeader::parse(self.buffer, ph_slice)?;
            program_headers[i] = Some(ph);
        }

        Ok(program_headers)
    }
}

#[derive(Debug, Copy, Clone)]
pub enum HeaderParseError {
    Magic,
    Class,
    Endianness,
    ELFVersion,
    ABI,
    Type,
    Machine,
    Version,
    EntryPoint,
    ProgramHeader,
    SectionHeader,
    Flags,
    HeaderSize,
    ProgramHeaderSize,
    ProgramHeaderCount,
    SectionHeaderSize,
    SectionHeaderCount,
    SectionNamesIndex,
    Unknown,
}

impl HeaderParseError {
    #[allow(dead_code)]
    fn as_str(&self) -> &'static str {
        match self {
            HeaderParseError::Magic => "Invalid MAGIC bytes",
            HeaderParseError::Class => "Unexpected ELF class",
            HeaderParseError::Endianness => "Invalid endianness",
            HeaderParseError::ELFVersion => "Unexpected ELF version",
            HeaderParseError::ABI => "Unexpected ABI",
            HeaderParseError::Type => "Unexpected ELF type",
            HeaderParseError::Machine => "Unsupported machine type",
            HeaderParseError::Version => "Unexpected program header version",
            HeaderParseError::EntryPoint => "Invalid entry point",
            HeaderParseError::ProgramHeader => "Invalid program header offset",
            HeaderParseError::SectionHeader => "Invalid section header offset",
            HeaderParseError::Flags => "Unsupported flags",
            HeaderParseError::HeaderSize => "Invalid header size",
            HeaderParseError::ProgramHeaderSize => "Invalid program header size",
            HeaderParseError::ProgramHeaderCount => "Invalid program header count",
            HeaderParseError::SectionHeaderSize => "Invalid section header size",
            HeaderParseError::SectionHeaderCount => "Invalid section header count",
            HeaderParseError::SectionNamesIndex => "Invalid section names starting index",
            HeaderParseError::Unknown => "Unknown error",
        }
    }
}

impl Default for HeaderParseError {
    fn default() -> Self {
        HeaderParseError::Unknown
    }
}

#[cfg(test)]
mod test {
    use crate::{
        arch::Address,
        elf::header::{Class, Endianness, Header, HeaderParseError, Machine, Type, OS_ABI_3},
    };

    const ELF_64_FILE_CONTENT: &[u8] = &super::super::test_elf_binaries::TEST_ELF64_EXECUTABLE;

    #[test]
    fn parse() -> Result<(), HeaderParseError> {
        let (_, header) =
            Header::parse(ELF_64_FILE_CONTENT).map_err(|e| e.internal_error().clone())?;
        assert_eq!(header.class, Class::ELF64);
        assert_eq!(header.endianness, Endianness::Little);
        assert_eq!(header.elf_version, 1);
        assert_eq!(header.os_abi, OS_ABI_3);
        assert_eq!(header.r#type, Type::Exec);
        assert_eq!(header.machine, Machine::X86_64);
        assert_eq!(header.version, 1);
        assert_eq!(header.entry_point, Address::from(0x401ca0_u64));
        assert_eq!(header.ph_offset, Address::from(64_u64));
        assert_eq!(header.sh_offset, Address::from(724800_u64));
        assert_eq!(header.flags, 0);
        assert_eq!(header.header_size, 64);
        assert_eq!(header.ph_entry_size, 56);
        assert_eq!(header.ph_entry_count, 8);
        assert_eq!(header.sh_entry_size, 64);
        assert_eq!(header.sh_entry_count, 31);
        assert_eq!(header.sh_entry_names_index, 30);

        Ok(())
    }
}

impl<'a> core::fmt::Debug for Header<'a> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str("Header { ")?;
        f.write_fmt(format_args!("{:?}, ", self.class))?;
        f.write_fmt(format_args!("{:?}, ", self.endianness))?;
        f.write_fmt(format_args!("{:?}, ", self.elf_version))?;
        f.write_fmt(format_args!("{:?}, ", self.os_abi))?;
        f.write_fmt(format_args!("{:?}, ", self.r#type))?;
        f.write_fmt(format_args!("{:?}, ", self.machine))?;
        f.write_fmt(format_args!("{:?}, ", self.version))?;
        f.write_fmt(format_args!("{:?}, ", self.entry_point))?;
        f.write_fmt(format_args!("{:?}, ", self.ph_offset))?;
        f.write_fmt(format_args!("{:?}, ", self.sh_offset))?;
        f.write_fmt(format_args!("{:?}, ", self.flags))?;
        f.write_fmt(format_args!("{:?}, ", self.header_size))?;
        f.write_fmt(format_args!("{:?}, ", self.ph_entry_size))?;
        f.write_fmt(format_args!("{:?}, ", self.ph_entry_count))?;
        f.write_fmt(format_args!("{:?}, ", self.sh_entry_size))?;
        f.write_fmt(format_args!("{:?}, ", self.sh_entry_count))?;
        f.write_fmt(format_args!("{:?} ", self.sh_entry_names_index))?;
        f.write_str("}")
    }
}
