use bitflags::bitflags;

use crate::{arch::Address, impl_parse_for_enum, parse_tuple};

use super::{dynamic_entry::DynamicEntry, ParseResult, MAX_DYNAMIC_ENTRIES};

const SEGMENT_TYPE_NULL: u32 = 0x0;
const SEGMENT_TYPE_LOAD: u32 = 0x1;
const SEGMENT_TYPE_DYNAMIC: u32 = 0x2;
const SEGMENT_TYPE_INTERP: u32 = 0x3;
const SEGMENT_TYPE_NOTE: u32 = 0x4;
const SEGMENT_TYPE_SHLIB: u32 = 0x5;
const SEGMENT_TYPE_PHDR: u32 = 0x6;
const SEGMENT_TYPE_TLS: u32 = 0x7;
const SEGMENT_TYPE_LO_OS: u32 = 0x6000_0000;
const SEGMENT_TYPE_HI_OS: u32 = 0x6FFF_FFFF;
const SEGMENT_TYPE_LO_PROC: u32 = 0x7000_0000;
const SEGMENT_TYPE_HI_PROC: u32 = 0x7FFF_FFFF;
const SEGMENT_TYPE_GNU_EH_FRAME: u32 = 0x6474_E550;
const SEGMENT_TYPE_GNU_STACK: u32 = 0x6474_E551;
const SEGMENT_TYPE_GNU_REL_RO: u32 = 0x6474_E552;
const SEGMENT_TYPE_GNU_PROPERTY: u32 = 0x6474_E553;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum SegmentType {
    Null = SEGMENT_TYPE_NULL,
    Load = SEGMENT_TYPE_LOAD,
    Dynamic = SEGMENT_TYPE_DYNAMIC,
    Interp = SEGMENT_TYPE_INTERP,
    Note = SEGMENT_TYPE_NOTE,
    SharedLib = SEGMENT_TYPE_SHLIB,
    ProgramHeader = SEGMENT_TYPE_PHDR,
    TLS = SEGMENT_TYPE_TLS,
    LoOS = SEGMENT_TYPE_LO_OS,
    HiOS = SEGMENT_TYPE_HI_OS,
    LoProc = SEGMENT_TYPE_LO_PROC,
    HiProc = SEGMENT_TYPE_HI_PROC,
    GnuEhFrame = SEGMENT_TYPE_GNU_EH_FRAME,
    GnuStack = SEGMENT_TYPE_GNU_STACK,
    GnuRelRo = SEGMENT_TYPE_GNU_REL_RO,
    GnuProperty = SEGMENT_TYPE_GNU_PROPERTY,
}

impl_parse_for_enum!(SegmentType, u32, ProgramHeaderParseError);

impl SegmentType {
    pub fn from_u32(value: u32) -> Option<Self> {
        match value {
            SEGMENT_TYPE_NULL => Some(SegmentType::Null),
            SEGMENT_TYPE_LOAD => Some(SegmentType::Load),
            SEGMENT_TYPE_DYNAMIC => Some(SegmentType::Dynamic),
            SEGMENT_TYPE_INTERP => Some(SegmentType::Interp),
            SEGMENT_TYPE_NOTE => Some(SegmentType::Note),
            SEGMENT_TYPE_SHLIB => Some(SegmentType::SharedLib),
            SEGMENT_TYPE_PHDR => Some(SegmentType::ProgramHeader),
            SEGMENT_TYPE_TLS => Some(SegmentType::TLS),
            SEGMENT_TYPE_LO_OS => Some(SegmentType::LoOS),
            SEGMENT_TYPE_HI_OS => Some(SegmentType::HiOS),
            SEGMENT_TYPE_LO_PROC => Some(SegmentType::LoProc),
            SEGMENT_TYPE_HI_PROC => Some(SegmentType::HiProc),
            SEGMENT_TYPE_GNU_EH_FRAME => Some(SegmentType::GnuEhFrame),
            SEGMENT_TYPE_GNU_STACK => Some(SegmentType::GnuStack),
            SEGMENT_TYPE_GNU_REL_RO => Some(SegmentType::GnuRelRo),
            SEGMENT_TYPE_GNU_PROPERTY => Some(SegmentType::GnuProperty),
            _ => None,
        }
    }
}

bitflags! {
    pub struct SegmentFlags: u32 {
        const EXECUTE = 0x1;
        const WRITE = 0x2;
        const READ = 0x4;
    }
}

impl SegmentFlags {
    pub fn from_u32(value: u32) -> Option<Self> {
        Self::from_bits(value)
    }
}

impl_parse_for_enum!(SegmentFlags, u32, ProgramHeaderParseError);

#[derive(Debug, Copy, Clone)]
pub enum SegmentContents {
    Dynamic([Option<DynamicEntry>; MAX_DYNAMIC_ENTRIES]),
    Unknown,
}

#[derive(Copy, Clone)]
pub struct ProgramHeader<'a> {
    pub r#type: SegmentType,
    pub flags: SegmentFlags,
    pub offset: Address,
    pub virtual_address: Address,
    pub physical_address: Address,
    pub file_size: Address,
    pub memory_size: Address,
    pub alignment: Address,
    pub data: &'a [u8],
    pub contents: SegmentContents,
}

impl<'a> ProgramHeader<'a> {
    pub fn parse(
        full_buffer: &'a [u8],
        buffer: &'a [u8],
    ) -> ParseResult<'a, Self, ProgramHeaderParseError> {
        use super::{
            binary_parser::parsers,
            dynamic_entry::{DynamicTag, DYNAMIC_ENTRY_SIZE_BYTES},
        };

        let (continuation, (r#type, flags)) = parse_tuple!(
            buffer,
            (
                parse,
                SegmentType::parse,
                ProgramHeaderParseError::SegmentType
            ),
            (
                parse,
                SegmentFlags::parse,
                ProgramHeaderParseError::SegmentFlags
            )
        )?;

        let (
            continuation,
            (offset, virtual_address, physical_address, file_size, memory_size, alignment),
        ) = parse_tuple!(
            &continuation.slice,
            (parse, parsers::address, ProgramHeaderParseError::Offset),
            (
                parse,
                parsers::address,
                ProgramHeaderParseError::AddressVirtual
            ),
            (
                parse,
                parsers::address,
                ProgramHeaderParseError::AddressPhysical
            ),
            (parse, parsers::address, ProgramHeaderParseError::FileSize),
            (parse, parsers::address, ProgramHeaderParseError::MemorySize),
            (parse, parsers::address, ProgramHeaderParseError::Alignment)
        )?;

        let slice = &full_buffer[offset.into()..][..file_size.into()];

        let (_, contents) = match r#type {
            SegmentType::Dynamic => {
                let count = slice.len() / DYNAMIC_ENTRY_SIZE_BYTES;
                assert!(count <= MAX_DYNAMIC_ENTRIES);

                let mut contents = [None; MAX_DYNAMIC_ENTRIES];
                for (i, segment) in slice.chunks(DYNAMIC_ENTRY_SIZE_BYTES).enumerate() {
                    if let Ok((_, entry)) = DynamicEntry::parse(segment) {
                        if entry.tag == DynamicTag::Null {
                            break;
                        }
                        contents[i] = Some(entry);
                    }
                }

                (slice, SegmentContents::Dynamic(contents))
            }
            _ => (slice, SegmentContents::Unknown),
        };

        let res = Self {
            r#type,
            flags,
            offset,
            virtual_address,
            physical_address,
            file_size,
            memory_size,
            alignment,
            data: slice,
            contents,
        };

        Ok((continuation, res))
    }

    pub fn file_range(&self) -> core::ops::Range<Address> {
        self.offset..self.offset + self.file_size
    }

    pub fn memory_range(&self) -> core::ops::Range<Address> {
        self.physical_address..self.physical_address + self.memory_size
    }
}

impl<'a> core::fmt::Debug for ProgramHeader<'a> {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(
            f,
            "file {:?} | mem {:?} | align {:?} | ",
            self.file_range(),
            self.memory_range(),
            self.alignment
        )?;

        for (flag, letter) in &[
            (SegmentFlags::READ, "R"),
            (SegmentFlags::WRITE, "W"),
            (SegmentFlags::EXECUTE, "X"),
        ] {
            if self.flags.contains(*flag) {
                write!(f, "{}", *letter)?;
            } else {
                write!(f, ".")?;
            }
        }

        write!(f, " {:?}", self.r#type)
    }
}

#[derive(Debug, Copy, Clone)]
pub enum ProgramHeaderParseError {
    SegmentType,
    SegmentFlags,
    Offset,
    AddressVirtual,
    AddressPhysical,
    FileSize,
    MemorySize,
    Alignment,
    Unknown,
}

impl Default for ProgramHeaderParseError {
    fn default() -> Self {
        ProgramHeaderParseError::Unknown
    }
}

#[cfg(test)]
mod test {
    use crate::{
        arch::Address,
        elf::{
            header::Header,
            program_header::{ProgramHeaderParseError, SegmentFlags, SegmentType},
        },
    };

    const ELF_64_FILE_CONTENT: &[u8] = &super::super::test_elf_binaries::TEST_ELF64_EXECUTABLE;

    #[test]
    fn parse() -> Result<(), ProgramHeaderParseError> {
        let (_, header) =
            Header::parse(ELF_64_FILE_CONTENT).expect("Failed to parse ELF header content");

        let program_headers = header
            .program_headers()
            .map_err(|e| e.internal_error().clone())?;

        let ph = program_headers[0].as_ref().unwrap();
        assert_eq!(ph.r#type, SegmentType::Load);
        assert_eq!(ph.flags, SegmentFlags::READ);
        assert_eq!(ph.offset, Address::from(0_u64));
        assert_eq!(ph.virtual_address, Address::from(0x0000000000400000_u64));
        assert_eq!(ph.physical_address, Address::from(0x0000000000400000_u64));
        assert_eq!(ph.file_size, Address::from(0x0000000000000488_u64));
        assert_eq!(ph.memory_size, Address::from(0x0000000000000488_u64));
        assert_eq!(ph.alignment, Address::from(0x1000_u64));

        let ph = program_headers[header.ph_entry_count - 2].as_ref().unwrap();
        assert_eq!(ph.r#type, SegmentType::GnuStack);
        assert_eq!(ph.flags, SegmentFlags::READ | SegmentFlags::WRITE);
        assert_eq!(ph.offset, Address::from(0_u64));
        assert_eq!(ph.virtual_address, Address::from(0x0_u64));
        assert_eq!(ph.physical_address, Address::from(0x0_u64));
        assert_eq!(ph.file_size, Address::from(0x0_u64));
        assert_eq!(ph.memory_size, Address::from(0x0_u64));
        assert_eq!(ph.alignment, Address::from(0x10_u64));

        Ok(())
    }
}
