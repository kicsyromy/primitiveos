use core::ops::Range;

use crate::{
    arch::Address,
    elf::{program_header::ProgramHeader, Elf, RelType, SegmentType, MAX_PH_COUNT},
};

pub struct Loader<'a, 'e> {
    elf_file: &'a Elf<'e>,
    start_offset: Address,
    end_offset: Address,
    loadable_program_headers: [Option<&'a ProgramHeader<'e>>; MAX_PH_COUNT],
    loadable_ph_count: usize,
}

impl<'a, 'e> Loader<'a, 'e> {
    pub fn new(elf_file: &'a Elf<'e>) -> Self {
        let mut loadable_program_headers: [Option<&'a ProgramHeader<'e>>; MAX_PH_COUNT] =
            [None; MAX_PH_COUNT];
        let mut loadable_ph_count = 0;
        for (i, program_header) in elf_file
            .program_headers
            .iter()
            .filter(|ph| ph.is_some() && ph.as_ref().unwrap().r#type == SegmentType::Load)
            .map(|ph| (ph.as_ref().unwrap()))
            .enumerate()
        {
            loadable_program_headers[i] = Some(program_header);
            loadable_ph_count += 1;
        }

        let first_loadable_program_header = loadable_program_headers[0].unwrap();
        let last_loadable_program_header = loadable_program_headers[loadable_ph_count - 1].unwrap();

        let end_offset = last_loadable_program_header.memory_range().end;
        let start_offset = first_loadable_program_header.memory_range().start;

        Self {
            elf_file,
            start_offset,
            end_offset,
            loadable_program_headers,
            loadable_ph_count,
        }
    }

    pub fn start_offset(&self) -> Address {
        self.start_offset
    }

    pub fn bytes_memory_mapped(&self) -> usize {
        self.end_offset.as_usize() - self.start_offset.as_usize() + 1
    }

    pub fn loadable_program_headers(&self) -> &[Option<&ProgramHeader>] {
        &self.loadable_program_headers[..self.loadable_ph_count]
    }

    pub fn segment(
        &self,
        elf_memory_buffer: &'a mut [u8],
        program_header: &ProgramHeader<'e>,
    ) -> Segment {
        Segment::new(elf_memory_buffer, program_header, self)
    }
}

pub struct Segment<'a, 'b> {
    elf_loader: &'a Loader<'a, 'b>,
    memory_slice: &'a mut [u8],
    ph_data: &'b [u8],
    range: Range<Address>,
    size_in_memory: usize,
    size_in_file: usize,
}

impl<'a, 'b> Segment<'a, 'b> {
    pub fn new(
        elf_memory_buffer: &'a mut [u8],
        program_header: &ProgramHeader<'b>,
        elf_loader: &'a Loader<'a, 'b>,
    ) -> Self {
        use log::*;

        let range = program_header.memory_range();
        let size_in_memory = program_header.memory_size.as_usize();

        let start = range.start;
        let end = range.end;
        assert_eq!((end - start).as_usize(), size_in_memory);

        info!("Segment from {} to {}", start, end);

        let segment_offset = start - elf_loader.start_offset;
        assert!((segment_offset + size_in_memory).as_usize() < elf_memory_buffer.len());
        let memory_slice = &mut elf_memory_buffer[segment_offset.as_usize()..][..size_in_memory];

        Self {
            elf_loader,
            memory_slice,
            ph_data: program_header.data,
            range,
            size_in_memory,
            size_in_file: program_header.file_size.as_usize(),
        }
    }

    #[inline]
    pub fn range(&self) -> Range<Address> {
        self.range.clone()
    }

    pub fn load(&mut self) {
        unsafe {
            crate::libc_functions::memcpy(
                self.memory_slice.as_mut_ptr(),
                self.ph_data.as_ptr(),
                self.size_in_memory,
            );
        }
    }

    pub fn zero_bss(&mut self) {
        let extra_zero_count = self.size_in_memory - self.size_in_file;
        if extra_zero_count > 0 {
            let extra_zeroes = &mut self.memory_slice[self.size_in_file..self.size_in_memory];
            unsafe {
                crate::libc_functions::memset(extra_zeroes.as_mut_ptr(), 0x00, extra_zero_count);
            }
        }
    }

    pub fn apply_relocations(&mut self, base_address: Address) {
        let real_segment_start = Address::from(self.memory_slice.as_mut_ptr());

        for relocation in self.elf_loader.elf_file.rela_entries_iter().unwrap() {
            if self.range.contains(&relocation.offset) {
                let offset_into_segment =
                    relocation.offset.as_usize() - self.range.start.as_usize();

                // info!(
                //     "Applying {:?} relocation @ 0x{:x} from segment start",
                //     relocation.r#type, offset_into_segment
                // );

                let relocation_address =
                    (real_segment_start + offset_into_segment).as_mut_ptr() as *mut u64;
                match relocation.r#type {
                    RelType::X86_64_Relative | RelType::Aarch64_Relative => {
                        let relocation_value = relocation.addend.as_u64() + base_address.as_u64();
                        unsafe {
                            // info!(
                            //     "Replacing {} with {}",
                            //     *relocation_address, relocation_value
                            // );
                            *relocation_address = relocation_value;
                        }
                    }
                    _ => {
                        panic!("Unsupported relocation {:?}", relocation.r#type);
                    }
                }
            }
        }
    }
}
