pub const PSF1_MAGIC0: u8 = 0x36;
pub const PSF1_MAGIC1: u8 = 0x04;

pub const PSF1_CHAR_WIDTH: usize = 8;
pub const PSF1_CHAR_HEIGHT: usize = 16;

pub const PSF1_FONT_FILE_SIZE_BYTES: usize = 5_312;
pub const PSF1_HEADER_SIZE_BYTES: usize = core::mem::size_of::<PSF1Header>();
pub const PSF1_GLYPH_BUFFER_SIZE_BYTES: usize = PSF1_FONT_FILE_SIZE_BYTES - PSF1_HEADER_SIZE_BYTES;

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct PSF1Header {
    pub magic: [u8; 2],
    pub mode: u8,
    pub char_size: u8,
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct PSF1Font {
    pub header: PSF1Header,
    pub glyph_buffer: [u8; PSF1_GLYPH_BUFFER_SIZE_BYTES],
}
