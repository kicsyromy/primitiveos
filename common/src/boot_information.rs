use crate::{
    acpi::{RSDPDescriptorV1, RSDPDescriptorV2},
    efi_framebuffer::EFIFramebuffer,
    efi_memory_descriptor::EFIMemoryDescriptorList,
    psf1_font::PSF1Font,
};

pub const PAGE_SIZE: usize = 0x1000;

#[derive(Debug)]
#[repr(C)]
pub struct BootInformation {
    pub kernel_start_address: usize,
    pub kernel_size: usize,
    pub framebuffer: EFIFramebuffer,
    pub font: PSF1Font,
    pub memory_descriptors: EFIMemoryDescriptorList,
    pub rsdp_descriptor_v1: Option<RSDPDescriptorV1>,
    pub rsdp_descriptor_v2: RSDPDescriptorV2,
}
