use crate::arch::Address64;
use bitflags::bitflags;

#[repr(u32)]
#[allow(non_camel_case_types)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum EFIMemoryType {
    RESERVED = 0,
    LOADER_CODE = 1,
    LOADER_DATA = 2,
    BOOT_SERVICES_CODE = 3,
    BOOT_SERVICES_DATA = 4,
    RUNTIME_SERVICES_CODE = 5,
    RUNTIME_SERVICES_DATA = 6,
    CONVENTIONAL = 7,
    UNUSABLE = 8,
    ACPI_RECLAIM = 9,
    ACPI_NON_VOLATILE = 10,
    MMIO = 11,
    MMIO_PORT_SPACE = 12,
    PAL_CODE = 13,
    PERSISTENT_MEMORY = 14,
    CUSTOM_MEMORY_DESCRIPTORS = 0x80000000,
}

impl EFIMemoryType {
    pub fn as_str(&self) -> &'static str {
        match &self {
            EFIMemoryType::RESERVED => "EfiReservedMemory",
            EFIMemoryType::LOADER_CODE => "EfiLoaderCode",
            EFIMemoryType::LOADER_DATA => "EfiLoaderData",
            EFIMemoryType::BOOT_SERVICES_CODE => "EfiBootServicesCode",
            EFIMemoryType::BOOT_SERVICES_DATA => "EfiBootServicesData",
            EFIMemoryType::RUNTIME_SERVICES_CODE => "EfiRuntimeServicesCode",
            EFIMemoryType::RUNTIME_SERVICES_DATA => "EfiRuntimeServicesData",
            EFIMemoryType::CONVENTIONAL => "EfiConventionalMemory",
            EFIMemoryType::UNUSABLE => "EfiUnusableMemory",
            EFIMemoryType::ACPI_RECLAIM => "EfiACPIReclaimMemory",
            EFIMemoryType::ACPI_NON_VOLATILE => "EfiACPINonVolaileMemory",
            EFIMemoryType::MMIO => "EfiMemoryMappedIO",
            EFIMemoryType::MMIO_PORT_SPACE => "EfiMemoryMappedIOPortSpace",
            EFIMemoryType::PAL_CODE => "EfiPalCode",
            EFIMemoryType::PERSISTENT_MEMORY => "EfiPersistentMemory",
            EFIMemoryType::CUSTOM_MEMORY_DESCRIPTORS => "CustomMemoryDescriptors",
        }
    }
}

bitflags! {
    pub struct EFIMemoryAttribute: u64 {
        const UNCACHEABLE = 0x1;
        const WRITE_COMBINE = 0x2;
        const WRITE_THROUGH = 0x4;
        const WRITE_BACK = 0x8;
        const UNCACHABLE_EXPORTED = 0x10;
        const WRITE_PROTECT = 0x1000;
        const READ_PROTECT = 0x2000;
        const EXECUTE_PROTECT = 0x4000;
        const NON_VOLATILE = 0x8000;
        const MORE_RELIABLE = 0x10000;
        const READ_ONLY = 0x20000;
        const RUNTIME = 0x8000_0000_0000_0000;
    }
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct EFIMemoryDescriptor {
    pub r#type: EFIMemoryType,
    padding: u32,
    pub physical_start: Address64,
    pub virtual_start: Address64,
    pub page_count: u64,
    pub attribute: EFIMemoryAttribute,
}

impl EFIMemoryDescriptor {
    pub fn new(
        r#type: EFIMemoryType,
        physical_start: Address64,
        virtual_start: Address64,
        page_count: u64,
        attribute: EFIMemoryAttribute,
    ) -> Self {
        Self {
            r#type,
            padding: 0,
            physical_start,
            virtual_start,
            page_count,
            attribute,
        }
    }
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct EFIMemoryDescriptorList {
    buffer: *mut EFIMemoryDescriptor,
    count: usize,
}

impl EFIMemoryDescriptorList {
    pub fn new(buffer: *mut EFIMemoryDescriptor, count: usize) -> Self {
        Self { buffer, count }
    }

    pub fn as_slice(&self) -> &[EFIMemoryDescriptor] {
        use core::slice::from_raw_parts;

        unsafe { from_raw_parts(self.buffer as *const EFIMemoryDescriptor, self.count) }
    }

    pub fn as_slice_mut(&mut self) -> &mut [EFIMemoryDescriptor] {
        use core::slice::from_raw_parts_mut;

        unsafe { from_raw_parts_mut(self.buffer as *mut EFIMemoryDescriptor, self.count) }
    }
}
