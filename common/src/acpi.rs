/// RSDP (Root System Description Pointer) is a data structure used in the ACPI programming interface.
use crate::arch::AddressVirtual;

const SIGNATURE_V1: [u8; 8] = [b'R', b'S', b'D', b' ', b'P', b'T', b'R', b' '];
// Probably some better way to do this with some fancy iter.map.sum.mut.put.bla stuff,
// no idea how so it will do for now
const SIGNATURE_BYTE_SUM: usize = b'R' as usize
    + b'S' as usize
    + b'D' as usize
    + b' ' as usize
    + b'P' as usize
    + b'T' as usize
    + b'R' as usize
    + b' ' as usize;

/// ACPI Version 1.0 Descriptor definition
#[allow(dead_code)]
#[derive(Copy, Clone)]
#[repr(C, packed)]
pub struct RSDPDescriptorV1 {
    /// This 8-byte string (not null terminated!) must contain "RSD PTR ". It stands on a 16-byte boundary.
    pub signature: [u8; 8],
    /// The value to add to all the other bytes (of the Version 1.0 table) to calculate the Checksum of the table.
    /// If this value added to all the others and casted to byte isn't equal to 0, the table must be ignored.
    pub checksum: u8,
    /// The specification says: "An OEM-supplied string that identifies the OEM".
    pub oem_id: [u8; 6],
    /// The revision of the ACPI. Larger revision numbers are backward compatible to lower revision numbers.
    pub revision: u8,
    /// 32-bit physical address of the RSDT table.
    pub rsdt_address: u32,
}

#[allow(dead_code)]
impl RSDPDescriptorV1 {
    // TODO: Figure out if this is supposed to be physical or virtual (treating it as virtual anyway)
    pub unsafe fn copy_from(address: AddressVirtual) -> Option<Self> {
        let address: usize = address.into();

        let this = *(core::mem::transmute::<usize, &Self>(address));
        if this.validate().is_err() {
            return None;
        }

        Some(this)
    }

    pub(crate) fn validate(&self) -> Result<(), ()> {
        if self.signature != SIGNATURE_V1 {
            return Err(());
        }

        self.verify_checksum()
    }

    // Assumes signature is valid
    fn verify_checksum(&self) -> Result<(), ()> {
        let mut total_sum = SIGNATURE_BYTE_SUM + self.checksum as usize;

        for byte in &self.oem_id {
            total_sum += *byte as usize;
        }

        total_sum += self.revision as usize;

        let address = self.rsdt_address as usize;
        total_sum += address & 0xff;
        total_sum += (address >> 8) & 0xff;
        total_sum += (address >> 16) & 0xff;
        total_sum += (address >> 24) & 0xff;

        if (total_sum & 0xff) != 0 {
            return Err(());
        }

        Ok(())
    }
}

impl core::fmt::Debug for RSDPDescriptorV1 {
    // Field accesses (for fields larger than a byte) are unsafe in packed structs (safe_packed_borrows)
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str("RSDPDescriptorV1 { ")?;
        f.write_fmt(format_args!("Signature: \"{}\", ", unsafe {
            core::str::from_utf8_unchecked(&self.signature)
        }))?;
        f.write_fmt(format_args!("Checksum: {}, ", self.checksum))?;
        f.write_fmt(format_args!(
            "OEM ID: {}, ",
            unsafe { core::str::from_utf8_unchecked(&self.oem_id) }.trim_end()
        ))?;
        f.write_fmt(format_args!("Revision: {}, ", self.revision))?;
        let rsdt_address = self.rsdt_address;
        f.write_fmt(format_args!("RSDT Physical Address: 0x{:x} ", rsdt_address))?;
        f.write_str("}")
    }
}

/// ACPI Version 2.0+ Descriptor definition
#[derive(Copy, Clone)]
#[repr(C, packed)]
pub struct RSDPDescriptorV2 {
    pub v1: RSDPDescriptorV1,
    /// The size of the entire table since offset 0 to the end.
    pub length: u32,
    /// 64-bit physical address of the XSDT table.
    pub xsdt_address: u64,
    /// Field used to calculate the checksum of the entire table, including both checksum fields.
    pub extended_checksum: u8,
    /// 3 bytes to be ignored in reading and that must not be written.
    _reserved: [u8; 3],
}

impl RSDPDescriptorV2 {
    // TODO: Figure out if this is supposed to be physical or virtual (treating it as virtual anyway)
    pub unsafe fn copy_from(address: AddressVirtual) -> Option<Self> {
        let address: usize = address.into();

        let this = *(core::mem::transmute::<usize, &Self>(address));
        if this.validate().is_err() {
            return None;
        }

        Some(this)
    }

    fn validate(&self) -> Result<(), ()> {
        self.v1.validate()?;

        self.verify_checksum()
    }

    fn verify_checksum(&self) -> Result<(), ()> {
        let mut total_sum = 0_usize;

        let length = self.length as usize;
        total_sum += length & 0xff;
        total_sum += (length >> 8) & 0xff;
        total_sum += (length >> 16) & 0xff;
        total_sum += (length >> 24) & 0xff;

        let xsdt_address = self.xsdt_address as usize;
        total_sum += xsdt_address & 0xff;
        total_sum += (xsdt_address >> 8) & 0xff;
        total_sum += (xsdt_address >> 16) & 0xff;
        total_sum += (xsdt_address >> 24) & 0xff;
        total_sum += (xsdt_address >> 32) & 0xff;
        total_sum += (xsdt_address >> 40) & 0xff;
        total_sum += (xsdt_address >> 48) & 0xff;
        total_sum += (xsdt_address >> 56) & 0xff;

        total_sum += self.extended_checksum as usize;

        if (total_sum & 0xff) != 0 {
            return Err(());
        }

        Ok(())
    }
}

impl core::fmt::Debug for RSDPDescriptorV2 {
    // Field accesses (for fields larger than a byte) are unsafe in packed structs (safe_packed_borrows)
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str("RSDPDescriptorV2 { ")?;
        f.write_fmt(format_args!("{:?}, ", self.v1))?;
        let length = self.length;
        f.write_fmt(format_args!("Length: {}, ", length))?;
        let xsdt_address = self.xsdt_address;
        f.write_fmt(format_args!(
            "XSDT Physical Address: 0x{:x}, ",
            xsdt_address
        ))?;
        f.write_fmt(format_args!(
            "Extended Checksum: {} ",
            self.extended_checksum
        ))?;
        f.write_str("}")
    }
}
