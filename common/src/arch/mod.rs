use core::ops::{Add, AddAssign, Sub, SubAssign};
use derive_more::{Add, Sub};

#[cfg(target_arch = "x86_64")]
mod x86_64;
#[cfg(target_arch = "x86_64")]
pub use x86_64::*;

#[cfg(target_arch = "aarch64")]
mod aarch64;
#[cfg(target_arch = "aarch64")]
pub use aarch64::*;

#[cfg(target_pointer_width = "64")]
pub type Address = Address64;
#[cfg(target_pointer_width = "32")]
pub type Address = Address32;

pub type AddressPhysical = Address;
pub type AddressVirtual = Address;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Add, Sub)]
#[repr(transparent)]
pub struct Address32(u32);

impl Address32 {
    pub const fn as_u32(&self) -> u32 {
        self.0
    }

    pub const fn as_usize(&self) -> usize {
        self.0 as usize
    }

    pub fn as_ptr(&self) -> *const u8 {
        unsafe { core::mem::transmute(self.0 as usize) }
    }

    pub fn as_mut_ptr(&self) -> *mut u8 {
        unsafe { core::mem::transmute(self.0 as usize) }
    }
}

impl From<u32> for Address32 {
    fn from(x: u32) -> Self {
        Address32(x)
    }
}

impl From<Address32> for u32 {
    fn from(x: Address32) -> Self {
        x.0
    }
}

#[cfg(target_pointer_width = "32")]
impl From<*const u8> for Address32 {
    fn from(x: *const u8) -> Self {
        unsafe { core::mem::transmute(x) }
    }
}

#[cfg(target_pointer_width = "32")]
impl From<*mut u8> for Address32 {
    fn from(x: *mut u8) -> Self {
        unsafe { core::mem::transmute(x) }
    }
}

impl Add<u32> for Address32 {
    type Output = Address32;

    fn add(self, rhs: u32) -> Self::Output {
        Address32::from(self.0 + rhs)
    }
}

impl AddAssign<u32> for Address32 {
    fn add_assign(&mut self, rhs: u32) {
        self.0 += rhs
    }
}

impl Sub<u32> for Address32 {
    type Output = Address32;

    fn sub(self, rhs: u32) -> Self::Output {
        Address32::from(self.0 - rhs)
    }
}

impl SubAssign<u32> for Address32 {
    fn sub_assign(&mut self, rhs: u32) {
        self.0 -= rhs
    }
}

#[cfg(target_pointer_width = "32")]
impl From<usize> for Address32 {
    fn from(x: usize) -> Self {
        Address32(x as u32)
    }
}

#[cfg(target_pointer_width = "32")]
impl From<Address32> for usize {
    fn from(x: Address32) -> Self {
        x.0 as usize
    }
}

#[cfg(target_pointer_width = "32")]
impl Add<usize> for Address32 {
    type Output = Address32;

    fn add(self, rhs: usize) -> Self::Output {
        Address32::from(self.0 + rhs as u32)
    }
}

#[cfg(target_pointer_width = "32")]
impl AddAssign<usize> for Address32 {
    fn add_assign(&mut self, rhs: usize) {
        self.0 += rhs as u32
    }
}

#[cfg(target_pointer_width = "32")]
impl Sub<usize> for Address32 {
    type Output = Address32;

    fn sub(self, rhs: usize) -> Self::Output {
        Address32::from(self.0 - rhs as u32)
    }
}

#[cfg(target_pointer_width = "32")]
impl SubAssign<usize> for Address32 {
    fn sub_assign(&mut self, rhs: usize) {
        self.0 -= rhs as u32
    }
}

impl core::fmt::Debug for Address32 {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!("0x{:x}", self.0))
    }
}

impl core::fmt::Display for Address32 {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!("0x{:x}", self.0))
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Add, Sub)]
#[repr(transparent)]
pub struct Address64(u64);

impl Address64 {
    pub const fn as_u64(&self) -> u64 {
        self.0
    }

    pub const fn as_usize(&self) -> usize {
        self.0 as usize
    }

    #[cfg(target_pointer_width = "64")]
    pub fn as_ptr(&self) -> *const u8 {
        unsafe { core::mem::transmute(self.0) }
    }

    #[cfg(target_pointer_width = "32")]
    pub fn as_ptr(&self) -> *const u8 {
        assert!(self.0 <= core::u32::MAX as u64);
        unsafe { core::mem::transmute(self.0 as usize) }
    }

    #[cfg(target_pointer_width = "64")]
    pub fn as_mut_ptr(&self) -> *mut u8 {
        unsafe { core::mem::transmute(self.0) }
    }

    #[cfg(target_pointer_width = "32")]
    pub fn as_mut_ptr(&self) -> *mut u8 {
        assert!(self.0 <= core::u32::MAX as u64);
        unsafe { core::mem::transmute(self.0 as usize) }
    }
}

impl From<u64> for Address64 {
    fn from(x: u64) -> Self {
        Address64(x)
    }
}

impl From<Address64> for u64 {
    fn from(x: Address64) -> Self {
        x.0
    }
}

impl From<*const u8> for Address64 {
    fn from(x: *const u8) -> Self {
        unsafe { core::mem::transmute(x) }
    }
}

impl From<*mut u8> for Address64 {
    fn from(x: *mut u8) -> Self {
        unsafe { core::mem::transmute(x) }
    }
}

impl Add<u64> for Address64 {
    type Output = Address64;

    fn add(self, rhs: u64) -> Self::Output {
        Address64::from(self.0 + rhs)
    }
}

impl AddAssign<u64> for Address64 {
    fn add_assign(&mut self, rhs: u64) {
        self.0 += rhs
    }
}

impl Sub<u64> for Address64 {
    type Output = Address64;

    fn sub(self, rhs: u64) -> Self::Output {
        Address64::from(self.0 - rhs)
    }
}

impl SubAssign<u64> for Address64 {
    fn sub_assign(&mut self, rhs: u64) {
        self.0 -= rhs
    }
}

#[cfg(target_pointer_width = "64")]
impl From<usize> for Address64 {
    fn from(x: usize) -> Self {
        Address64(x as u64)
    }
}

#[cfg(target_pointer_width = "64")]
impl From<Address64> for usize {
    fn from(x: Address64) -> Self {
        x.0 as usize
    }
}

#[cfg(target_pointer_width = "64")]
impl Add<usize> for Address64 {
    type Output = Address64;

    fn add(self, rhs: usize) -> Self::Output {
        Address64::from(self.0 + rhs as u64)
    }
}

#[cfg(target_pointer_width = "64")]
impl AddAssign<usize> for Address64 {
    fn add_assign(&mut self, rhs: usize) {
        self.0 += rhs as u64
    }
}

#[cfg(target_pointer_width = "64")]
impl Sub<usize> for Address64 {
    type Output = Address64;

    fn sub(self, rhs: usize) -> Self::Output {
        Address64::from(self.0 - rhs as u64)
    }
}

#[cfg(target_pointer_width = "64")]
impl SubAssign<usize> for Address64 {
    fn sub_assign(&mut self, rhs: usize) {
        self.0 -= rhs as u64
    }
}

impl core::fmt::Debug for Address64 {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!("0x{:x}", self.0))
    }
}

impl core::fmt::Display for Address64 {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!("0x{:x}", self.0))
    }
}
