#[derive(Debug, Copy, Clone)]
struct MultiprocessorAffinity
{
    cpu_id: u8,
    multi_threaded_cores: bool,
    single_core_system: bool,
}

// This function uses the MPIDR_EL1 register that provides an additional core identification
// mechanism for scheduling purposes in a cluster.
pub fn read_multi_processor_affinity() {
    use log::warn;

    let mpidr_el1: u64;
    unsafe {
        asm!(
            r#"
                mrs     {}, mpidr_el1
            "#,
            out(reg) mpidr_el1
        );
    }

    let cpu_id = (mpidr_el1 >> 8) & 0b0000_0111;
    let mt = (mpidr_el1 >> 24) & 0b0000_0001;
    let u = (mpidr_el1 >> 24) & 0b0100_0000;

    warn!("MPIDR_EL1 {}", mpidr_el1);
    warn!("{:?}", MultiprocessorAffinity{
        cpu_id: cpu_id as u8,
        multi_threaded_cores: mt != 0,
        single_core_system: u != 0
    });
}

pub fn disable_interrupts() {}

pub fn enable_interrupts() {}

pub fn sleep() {
    unsafe {
        asm! { "wfe" }
    }
}

pub fn enable_interrupts_and_sleep() {}
