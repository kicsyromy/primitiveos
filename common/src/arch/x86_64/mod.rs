pub fn disable_interrupts() {
    unsafe {
        asm! { "cli" }
    }
}

pub fn enable_interrupts() {
    unsafe {
        asm! { "sti" }
    }
}

pub fn sleep() {
    unsafe {
        asm! { "hlt" }
    }
}

pub fn enable_interrupts_and_sleep() {
    unsafe {
        asm! { "sti\nhlt" }
    }
}
