use crate::arch::Address;

pub const PIXEL_SIZE_BYTES: usize = core::mem::size_of::<u32>();

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
#[repr(C)]
#[allow(dead_code)]
pub enum PixelFormat {
    RGB,
    BGR,
    Unsupported,
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct EFIFramebuffer {
    pub base_address: Address,
    pub buffer_size: usize,
    pub width: usize,
    pub height: usize,
    pub pixels_per_scanline: usize,
    pub pixel_format: PixelFormat,
}

impl EFIFramebuffer {
    pub const fn new(
        base_address: Address,
        buffer_size: usize,
        width: usize,
        height: usize,
        pixels_per_scanline: usize,
        pixel_format: PixelFormat,
    ) -> Self {
        Self {
            base_address,
            buffer_size,
            width,
            height,
            pixels_per_scanline,
            pixel_format,
        }
    }

    pub fn clear(&mut self) {
        for y in 0..self.height {
            for x in 0..self.pixels_per_scanline {
                self.put_pixel(x, y, 0x00000000);
            }
        }
    }

    #[inline]
    pub fn framebuffer_offset(&self, x: usize, y: usize) -> usize {
        x * PIXEL_SIZE_BYTES + (y * PIXEL_SIZE_BYTES * self.pixels_per_scanline)
    }

    #[inline]
    pub fn put_pixel(&mut self, x: usize, y: usize, color: u32) {
        let write_address = self.base_address + self.framebuffer_offset(x, y);
        assert!(write_address < self.base_address + self.buffer_size);

        unsafe {
            core::ptr::write_volatile(write_address.as_mut_ptr() as *mut u32, color);
        }
    }
}
