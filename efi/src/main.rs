#![no_std]
#![no_main]
#![feature(abi_efiapi)]
#![feature(asm)]
#![feature(global_asm)]

extern crate alloc;

use common::{
    acpi::{RSDPDescriptorV1, RSDPDescriptorV2},
    arch::{AddressVirtual, disable_interrupts},
    boot_information::BootInformation,
    efi_framebuffer::EFIFramebuffer,
    efi_memory_descriptor::{EFIMemoryDescriptor, EFIMemoryDescriptorList, EFIMemoryType},
    psf1_font::PSF1Font,
};
use log::*;
use uefi::{
    ResultExt,
    Status, table::{
        boot::{MemoryDescriptor, MemoryType},
        Boot, SystemTable,
    },
};

use crate::{kernel_image::KernelImage, psf1_font::load_font};

mod filesystem;
mod graphics_output;
mod kernel_image;
mod psf1_font;
mod arch;

const KERNEL_FILE_NAME: &str = "primitiveos.elf";
const FONT_FILE_NAME: &str = "zap-vga16.psf";

#[no_mangle]
fn uefi_start(image_handle: uefi::Handle, mut system_table: SystemTable<Boot>) -> Status {
    uefi_services::init(&mut system_table).expect_success("Failed to initialize utils");

    system_table
        .stdout()
        .reset(false)
        .expect_success("Failed to reset output buffer");

    let rev = system_table.uefi_revision();
    let (major, minor) = (rev.major(), rev.minor());

    info!("UEFI {}.{}", major, minor);

    let framebuffer = graphics_output::framebuffer(&system_table);
    if framebuffer.is_none() {
        panic!("Failed to initialize framebuffer");
    }
    let framebuffer = framebuffer.unwrap();
    info!("{:?}", framebuffer);

    let font = load_font(FONT_FILE_NAME, image_handle, &system_table);
    if font.is_none() {
        panic!("Failed to load font from disk");
    }
    let font = font.unwrap();
    info!("{:?}", font.header);

    let kernel_image = KernelImage::load(KERNEL_FILE_NAME, image_handle, &system_table);
    if kernel_image.is_none() {
        panic!("Failed to load kernel image");
    }
    let kernel_image = kernel_image.unwrap();

    let mut rsdp_descriptor_v1 = None;
    let mut rsdp_descriptor_v2 = None;
    for table in system_table.config_table() {
        use uefi::table::cfg::{ACPI2_GUID, ACPI_GUID};
        if table.guid == ACPI_GUID {
            unsafe {
                rsdp_descriptor_v1 =
                    RSDPDescriptorV1::copy_from(AddressVirtual::from(table.address as usize));
            }
        }

        if table.guid == ACPI2_GUID {
            unsafe {
                rsdp_descriptor_v2 =
                    RSDPDescriptorV2::copy_from(AddressVirtual::from(table.address as usize));
            }
        }
    }

    if rsdp_descriptor_v2.is_none() {
        panic!("Failed to find XSDT pointer table");
    }

    exit_boot_services_and_start_kernel(
        system_table,
        image_handle,
        kernel_image,
        framebuffer,
        font,
        rsdp_descriptor_v1,
        rsdp_descriptor_v2.unwrap(),
    );
}

fn exit_boot_services_and_start_kernel(
    system_table: SystemTable<Boot>,
    image_handle: uefi::Handle,
    kernel_image: KernelImage,
    framebuffer: EFIFramebuffer,
    font: PSF1Font,
    rsdp_descriptor_v1: Option<RSDPDescriptorV1>,
    rsdp_descriptor_v2: RSDPDescriptorV2,
) -> ! {
    use core::mem::{align_of, size_of};

    assert_eq!(
        size_of::<MemoryDescriptor>(),
        size_of::<EFIMemoryDescriptor>(),
        "Size mismatch between EFI and kernel memory descriptor sizes"
    );

    assert_eq!(
        align_of::<MemoryDescriptor>(),
        align_of::<EFIMemoryDescriptor>(),
        "Alignment mismatch between EFI and kernel memory descriptors"
    );

    let map_size = system_table.boot_services().memory_map_size() * 2_usize;
    let buffer = system_table
        .boot_services()
        .allocate_pool(
            MemoryType::custom(EFIMemoryType::CUSTOM_MEMORY_DESCRIPTORS as u32),
            map_size,
        )
        .expect_success("Failed to allocate memory for MemoryDescriptors")
        as *mut EFIMemoryDescriptor;
    let slice = unsafe { core::slice::from_raw_parts_mut(buffer as *mut u8, map_size) };

    info!(
        "Exiting boot services and calling kernel entry point at: 0x{:x}",
        kernel_image.start_address
    );

    // Disable HW Interrupts until the kernel is ready to deal with them
    disable_interrupts();

    let (_runtime_services, descriptors) = system_table
        .exit_boot_services(image_handle, slice)
        .expect_success("Failed to exit boot services");

    let descriptor_count = descriptors.len();
    let slice = unsafe { core::slice::from_raw_parts_mut(buffer, descriptor_count) };
    for (index, descriptor) in descriptors.enumerate() {
        let descriptor = descriptor as &MemoryDescriptor;
        let target_mem = &mut slice[index] as *mut EFIMemoryDescriptor as *mut u8;
        let source_mem = descriptor as *const MemoryDescriptor as *const u8;
        unsafe {
            common::libc_functions::memmove(
                target_mem,
                source_mem,
                core::mem::size_of::<MemoryDescriptor>(),
            )
        };
    }

    let memory_descriptors = EFIMemoryDescriptorList::new(buffer, descriptor_count);

    (kernel_image.entry_point)(BootInformation {
        kernel_start_address: kernel_image.start_address,
        kernel_size: kernel_image.size,
        framebuffer,
        font,
        memory_descriptors,
        rsdp_descriptor_v1,
        rsdp_descriptor_v2,
    });
}
