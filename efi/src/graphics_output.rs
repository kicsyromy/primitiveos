use log::*;
use uefi::table::{Boot, SystemTable};

use common::{arch::Address, efi_framebuffer::*};

pub fn framebuffer(system_table: &SystemTable<Boot>) -> Option<EFIFramebuffer> {
    let graphics_output = system_table
        .boot_services()
        .locate_protocol::<uefi::proto::console::gop::GraphicsOutput>();

    if graphics_output.is_err() {
        error!("Failed to find GRAPHICS_OUTPUT_PROTOCOL");
        return None;
    }

    let graphics_output = unsafe { graphics_output.unwrap().log().get().as_mut() }.unwrap();
    let mode_info = graphics_output.current_mode_info();
    let mut framebuffer = graphics_output.frame_buffer();

    let pixel_format = map_uefi_pixel_format(&mode_info.pixel_format());
    if pixel_format == PixelFormat::Unsupported {
        error!("Unsupported framebuffer pixel format");
        return None;
    }

    Some(EFIFramebuffer::new(
        Address::from(framebuffer.as_mut_ptr()),
        framebuffer.size(),
        mode_info.resolution().0,
        mode_info.resolution().1,
        mode_info.stride(),
        pixel_format,
    ))
}

fn map_uefi_pixel_format(uefi_format: &uefi::proto::console::gop::PixelFormat) -> PixelFormat {
    match uefi_format {
        uefi::proto::console::gop::PixelFormat::Rgb => PixelFormat::RGB,
        uefi::proto::console::gop::PixelFormat::Bgr => PixelFormat::BGR,
        uefi::proto::console::gop::PixelFormat::Bitmask => PixelFormat::Unsupported,
        uefi::proto::console::gop::PixelFormat::BltOnly => PixelFormat::Unsupported,
    }
}
