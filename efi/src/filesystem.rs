use alloc::vec::Vec;

use log::*;
use uefi::{
    proto::media::file::{File, FileHandle, FileInfo, RegularFile},
    table::{boot::MemoryType, Boot, SystemTable},
    ResultExt,
};

pub fn open_file(
    path: &str,
    image_handle: uefi::Handle,
    system_table: &SystemTable<Boot>,
) -> uefi::Result<uefi::proto::media::file::FileHandle> {
    use uefi::proto::media::file::{FileAttribute, FileMode};

    let loaded_image = system_table
        .boot_services()
        .handle_protocol::<uefi::proto::loaded_image::LoadedImage>(image_handle)?
        .log();
    let loaded_image =
        unsafe { loaded_image.get().as_ref() }.ok_or(uefi::Status::PROTOCOL_ERROR)?;

    let file_system = system_table
        .boot_services()
        .handle_protocol::<uefi::proto::media::fs::SimpleFileSystem>(loaded_image.device())?
        .log();
    let file_system = unsafe { file_system.get().as_mut() }.ok_or(uefi::Status::PROTOCOL_ERROR)?;

    let mut directory = file_system.open_volume()?.log();

    directory.open(path, FileMode::Read, FileAttribute::READ_ONLY)
}

pub fn load_file_content(
    mut file: FileHandle,
    system_table: &SystemTable<Boot>,
) -> Option<&'static mut [u8]> {
    let mut file_info_buffer: [u8; 0] = [];
    let file_info = file.get_info::<FileInfo>(&mut file_info_buffer);

    let file_info_size = file_info.err().unwrap().data().unwrap();
    let mut file_info_buffer = Vec::with_capacity(file_info_size);
    unsafe { file_info_buffer.set_len(file_info_size) };

    let file_info = file
        .get_info::<FileInfo>(file_info_buffer.as_mut())
        .unwrap()
        .log();

    let file_size = file_info.file_size() as usize;
    let file_content = system_table
        .boot_services()
        .allocate_pool(MemoryType::LOADER_DATA, file_size)
        .expect_success("Failed to allocate memory for file content");
    let file_content = unsafe { core::slice::from_raw_parts_mut(file_content, file_size) };

    let mut regular_file = unsafe { RegularFile::new(file) };
    regular_file
        .read(file_content)
        .map_err(|_| {
            error!("Failed to read file content");
        })
        .ok()?
        .log();

    Some(file_content)
}
