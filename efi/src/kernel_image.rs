use common::{boot_information::BootInformation, elf::Elf};
use log::*;
use uefi::{
    table::{
        boot::{AllocateType, MemoryType},
        Boot, SystemTable,
    },
    ResultExt,
};

#[cfg(target_arch = "x86_64")]
pub type KernelEntryPoint = extern "sysv64" fn(BootInformation) -> !;

#[cfg(not(target_arch = "x86_64"))]
pub type KernelEntryPoint = extern "C" fn(BootInformation) -> !;

pub struct KernelImage {
    pub entry_point: KernelEntryPoint,
    pub start_address: usize,
    pub size: usize,
}

impl core::fmt::Debug for KernelImage {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "it's good")
    }
}

impl KernelImage {
    pub fn load(
        path: &str,
        image_handle: uefi::Handle,
        system_table: &SystemTable<Boot>,
    ) -> Option<Self> {
        use crate::filesystem::{load_file_content, open_file};
        use common::arch::Address;

        let kernel_file = match open_file(path, image_handle, &system_table) {
            Ok(file) => {
                info!("Kernel image opened for reading");
                file.log()
            }
            Err(_) => {
                error!("Failed to open kernel image");
                return None;
            }
        };

        let data = load_file_content(kernel_file, &system_table)?;
        let elf_file = Elf::parse(data);
        if elf_file.is_err() {
            error!("Failed to parse ELF file: {:?}", elf_file.unwrap_err());
            return None;
        }

        let elf_file = elf_file.unwrap();
        info!("Kernel image read: {:?}", elf_file);

        let elf_loader = common::elf::Loader::new(&elf_file);

        let base_address = align_low(0x1000_usize);
        let total_load_segments_size: usize = elf_loader.bytes_memory_mapped();
        info!(
            "Reserving {} pages ({} bytes) at physical address 0x{:x} for kernel data segments",
            common::page_count!(total_load_segments_size),
            total_load_segments_size,
            base_address
        );

        // Allocate the required pages
        let mut segments_data = system_table.boot_services().allocate_pages(
            AllocateType::Address(base_address),
            MemoryType::LOADER_DATA,
            common::page_count!(total_load_segments_size),
        );

        if segments_data.is_err() {
            warn!(
                "Failed to allocate memory for kernel data segments at 0x{:x}, {:?}",
                base_address,
                segments_data.unwrap_err()
            );
            warn!("Retrying at first available address...");
            segments_data = system_table.boot_services().allocate_pages(
                AllocateType::AnyPages,
                MemoryType::LOADER_DATA,
                common::page_count!(total_load_segments_size),
            );
        }

        let segments_data =
            segments_data.expect_success("Failed to allocate memory for kernel data segments");
        let base_address = segments_data as usize;

        info!(
            "Loaded kernel image at base physical address 0x{:x}",
            base_address
        );

        let segments_data = unsafe {
            core::slice::from_raw_parts_mut(
                core::mem::transmute::<u64, *mut u8>(segments_data),
                total_load_segments_size,
            )
        };

        for program_header in elf_loader
            .loadable_program_headers()
            .iter()
            .map(|ph| ph.unwrap())
        {
            let mut elf_segment = elf_loader.segment(segments_data, program_header);
            elf_segment.load();
            elf_segment.zero_bss();
            elf_segment.apply_relocations(Address::from(base_address));
        }

        let kernel_start_address = elf_file.header.entry_point.as_usize() + base_address;
        let entry_point: KernelEntryPoint = unsafe { core::mem::transmute(kernel_start_address) };

        // Deallocate the kernel image since it's not needed any more
        system_table
            .boot_services()
            .free_pool(&mut data[0] as *mut _)
            .expect_success("Failed to deallocate unneeded kernel memory");

        Some(Self {
            entry_point,
            start_address: kernel_start_address,
            size: total_load_segments_size,
        })
    }
}

fn align_low(x: usize) -> usize {
    x & !0xFFF
}
