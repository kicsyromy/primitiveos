use log::*;
use uefi::table::{Boot, SystemTable};

use common::psf1_font::*;

use crate::filesystem::{load_file_content, open_file};
use uefi::ResultExt;

pub fn load_font(
    path: &str,
    image_handle: uefi::Handle,
    system_table: &SystemTable<Boot>,
) -> Option<PSF1Font> {
    let font_file = open_file(path, image_handle, system_table);
    if font_file.is_err() {
        error!("Failed to load font file");
        return None;
    }
    let font_content = load_file_content(font_file.unwrap().log(), &system_table)?;
    assert_eq!(font_content.len(), PSF1_FONT_FILE_SIZE_BYTES);

    let header_size_bytes = core::mem::size_of::<PSF1Header>();
    let font_header: PSF1Header =
        *unsafe { &*(font_content[0..header_size_bytes].as_ptr() as *const PSF1Header) };
    if font_header.magic[0] != PSF1_MAGIC0 || font_header.magic[1] != PSF1_MAGIC1 {
        error!("Invalid font file");
        return None;
    }

    let mut font_glyph_buffer = [0; PSF1_GLYPH_BUFFER_SIZE_BYTES];
    unsafe {
        common::libc_functions::memcpy(
            font_glyph_buffer.as_mut_ptr(),
            font_content[PSF1_HEADER_SIZE_BYTES..].as_ptr(),
            PSF1_GLYPH_BUFFER_SIZE_BYTES,
        );
    }

    info!("Loaded font from disk");

    system_table
        .boot_services()
        .free_pool(font_content.as_mut_ptr())
        .expect_success("Failed to deallocate temporary font buffer");

    Some(PSF1Font {
        header: font_header,
        glyph_buffer: font_glyph_buffer,
    })
}
