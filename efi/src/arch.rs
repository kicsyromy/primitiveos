// Disable __chkstk for aarch64
#[cfg(target_arch = "aarch64")]
global_asm!(
    r#"
    .global __chkstk
__chkstk:
    ret
"#
);
