OSNAME = primitiveos

MKFILEDIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

HOST_ARCH := $(shell sh -c "uname -m")
HOST_OS := $(shell sh -c "uname -o")

TARGET_ARCH = x86_64
TARGET_EFI = $(TARGET_ARCH)-none-efi
TARGET_KERNEL = $(TARGET_ARCH)-none-kernel
CONFIG = release
TESTING_BUILD = false

CARGO_CONFIG_PARAM := $(shell sh -c "if [ "$(CONFIG)" == "release" ]; then echo "--release"; else echo ""; fi")
CARGO_TEST_PARAM := $(shell sh -c "if [ "$(TESTING_BUILD)" == "true" ]; then echo --tests; else echo; fi")

STRIP := $(shell sh -c "if [ "$(TARGET_ARCH)" != "x86_64" ]; then echo "$(TARGET_ARCH)-none-elf-strip"; else echo "strip"; fi")

QEMU_BINARY = qemu-system-$(TARGET_ARCH)
QEMU_MACHINE := $(shell sh -c "                  \
    if [ "$(TARGET_ARCH)" == "x86_64" ]; then    \
        echo "q35";                              \
    elif [ "$(TARGET_ARCH)" == "aarch64" ]; then \
        echo "virt";                             \
    else                                         \
        echo "none";                             \
    fi"                                          \
)
QEMU_VIDEO_DEVICE := $(shell sh -c "             \
    if [ "$(TARGET_ARCH)" == "x86_64" ]; then    \
        echo "qxl-vga";                          \
    elif [ "$(TARGET_ARCH)" == "aarch64" ]; then \
        echo "ramfb";                            \
    else                                         \
        echo "ramfb";                            \
    fi"                                          \
)

BINDIR = $(MKFILEDIR)/bin
OVMFDIR = $(MKFILEDIR)/ovmf/$(TARGET_ARCH)
BINDIR_EFI = $(MKFILEDIR)/efi/target/$(TARGET_EFI)/$(CONFIG)
SRCDIR_EFI = $(MKFILEDIR)/efi
BINDIR_KERNEL = $(MKFILEDIR)/kernel/target/$(TARGET_KERNEL)/$(CONFIG)
SRCDIR_KERNEL = $(MKFILEDIR)/kernel

OVMF_EFI_CODE = $(OVMFDIR)/OVMF_CODE.fd
OVMF_EFI_VARS = $(BINDIR)/$(TARGET_ARCH)_efi_vars.fd

USER_ID := $(shell sh -c "echo $$UID")
GROUP_ID := $(shell sh -c "echo $$UID")

.PHONY: efi kernel

efi:
	cd $(SRCDIR_EFI) && cargo +nightly build $(CARGO_CONFIG_PARAM) --target $(TARGET_EFI).json
	cp $(BINDIR_EFI)/$(OSNAME).efi $(BINDIR)/$(OSNAME).efi.debug
	cp $(BINDIR_EFI)/$(OSNAME).efi $(BINDIR)/$(OSNAME).efi

kernel:
	if [ "$(TESTING_BUILD)" == "true" ]; then rm -rf $(BINDIR_KERNEL)/deps/*.elf; fi
	cd $(SRCDIR_KERNEL) && cargo +nightly build $(CARGO_CONFIG_PARAM) $(CARGO_TEST_PARAM) --target $(TARGET_KERNEL).json
	if [ "$(TESTING_BUILD)" == "true" ]; then cp -fv $(BINDIR_KERNEL)/deps/$(OSNAME)*.elf $(BINDIR_KERNEL)/$(OSNAME).elf; fi
	cp $(BINDIR_KERNEL)/$(OSNAME).elf $(BINDIR)/$(OSNAME).elf.debug
	cp $(BINDIR_KERNEL)/$(OSNAME).elf $(BINDIR)/$(OSNAME).elf
	$(STRIP) $(BINDIR)/$(OSNAME).elf

buildimg: efi kernel
	cd $(MKFILEDIR)
	mkdir -p $(BINDIR)
	dd if=/dev/zero of=$(BINDIR)/$(OSNAME).img bs=512 count=262144
	parted --script $(BINDIR)/$(OSNAME).img mktable gpt mkpart primary fat32 2048s 100% set 1 boot on
	mkdir -p $(BINDIR)/rootfs
	sudo kpartx -av $(BINDIR)/$(OSNAME).img
	LOOP_DEVICE_NAME=$(shell sh -c "sudo kpartx -l $(BINDIR)/$(OSNAME).img | cut -f5 -d' ' | xargs basename") && \
		LOOP_DEVICE_PARTITION=p1 && \
		LOOP_DEVICE_PATH=/dev/mapper/$$LOOP_DEVICE_NAME$$LOOP_DEVICE_PARTITION && \
		sudo mkfs.fat -F 32 -n "PRIMITIVEOS" $$LOOP_DEVICE_PATH && \
		sudo mount -o umask=0022,gid=$(GROUP_ID),uid=$(USER_ID) $$LOOP_DEVICE_PATH $(BINDIR)/rootfs
	mkdir -p $(BINDIR)/rootfs/EFI/BOOT
	mv $(BINDIR)/$(OSNAME).efi $(BINDIR)/rootfs/EFI/BOOT/bootx64.efi
	cp -av $(MKFILEDIR)/efi/startup.nsh $(BINDIR)/rootfs/
	mv $(BINDIR)/$(OSNAME).elf $(BINDIR)/rootfs/
	cp -av $(SRCDIR_KERNEL)/assets/fonts/zap-vga16.psf $(BINDIR)/rootfs/
	sync && sudo umount -r $(BINDIR)/rootfs
	sudo kpartx -d $(BINDIR)/$(OSNAME).img
	rm -rf $(BINDIR)/rootfs/*
	if [ ! -f "$(OVMF_EFI_VARS)" ]; then cp -avf "$(OVMFDIR)/OVMF_VARS.fd" "$(OVMF_EFI_VARS)"; fi

run: buildimg
	$(QEMU_BINARY) -drive format=raw,file=$(BINDIR)/$(OSNAME).img -machine $(QEMU_MACHINE) -cpu max -smp cores=2 -m 2G -device $(QEMU_VIDEO_DEVICE) -drive if=pflash,format=raw,unit=0,file="$(OVMF_EFI_CODE)",readonly=on -drive if=pflash,format=raw,unit=1,file="$(OVMF_EFI_VARS)" -net none -serial stdio -device usb-ehci,id=usb -device usb-mouse -device usb-kbd

debug: buildimg
	$(QEMU_BINARY) -s -S -drive format=raw,file=$(BINDIR)/$(OSNAME).img -machine $(QEMU_MACHINE) -cpu max -smp cores=2 -m 2G -device $(QEMU_VIDEO_DEVICE) -drive if=pflash,format=raw,unit=0,file="$(OVMF_EFI_CODE)",readonly=on -drive if=pflash,format=raw,unit=1,file="$(OVMF_EFI_VARS)" -net none -serial stdio -device usb-ehci,id=usb -device usb-mouse -device usb-kbd

test: buildimg
	$(QEMU_BINARY) -drive format=raw,file=$(BINDIR)/$(OSNAME).img -machine $(QEMU_MACHINE) -cpu max -smp cores=2 -m 2G -device $(QEMU_VIDEO_DEVICE) -drive if=pflash,format=raw,unit=0,file="$(OVMF_EFI_CODE)",readonly=on -drive if=pflash,format=raw,unit=1,file="$(OVMF_EFI_VARS)" -net none -serial stdio -device isa-debug-exit,iobase=0xf4,iosize=0x04 -display none || [[ $$? -eq 33 ]]

clippy:
	cd $(SRCDIR_EFI) && cargo +nightly clippy --target $(TARGET_EFI).json
	cd $(SRCDIR_KERNEL) && cargo +nightly clippy --target $(TARGET_KERNEL).json

default: efi kernel
